
@echo off

SET BASEPATH=%~dp0

SET PATH="%PATH%;%USERPROFILE%\AppData\Local\Julia-1.0.5\bin"
SET PATH=%PATH:"=%

SET XPRESS_JL_NO_AUTO_INIT=1
SET XPRESS_JL_SKIP_LIB_CHECK=1

julia.exe --project=%BASEPATH% -e "import Pkg; Pkg.instantiate()"