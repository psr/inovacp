
# get path
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

repopath="$DIR/bidbaseddispatch"

# variables 
juliapath="$repopath/deps/julia-distribution/linux/bin/julia"
JULIA_HOME="$repopath/deps/julia-distribution/linux/bin"
XPAUTH_PATH="$repopath/deps/libs/xpauth.xpr"
file="$repopath/src/cloud.jl"
echo $file

# exports
export JULIA_LOAD_PATH="$repopath/deps/packages:$JULIA_LOAD_PATH"
export JULIA_DEPOT_PATH="$repopath/deps/packages:$JULIA_DEPOT_PATH"
export LD_LIBRARY_PATH="$repopath/deps/psrclassesinterfacejulia:$LD_LIBRARY_PATH"

# clone
python environement.py
chmod -R +777 $repopath
find "$repopath/deps/packages" -type f -print0 | xargs -0 dos2unix

# run
$juliapath -H $JULIA_HOME $file $DIR
