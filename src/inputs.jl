abstract type inputs end
abstract type AbstractInput end
abstract type AbstractPSRClassesPtr <: AbstractInput end
mutable struct ExternalFilePtr <: AbstractInput
    Ptr::IOStream
end

"""
    Classes for reading whole files
"""
abstract type FileType end

mutable struct FileScenario{T}<:FileType
header::Vector{String}
value::T
FileScenario() = new{Float64}(String[], 0.0)
FileScenario(header::Vector{String}, value) = new{typeof(value)}(header, value)
end
mutable struct FileString{T}<:FileType
    header::Vector{String}
    value::T
    FileString() = new{String}(String[], "")
    FileString(header::Vector{String}, value) = new{typeof(value)}(header, value)
end

mutable struct InputFile{T}
    keyword::String
    filename::String
    inputType::InputType
    column_keys::Int
    filetype::T

    InputFile(keyword::String, filename::String, inputType::InputType) = new{FileScenario}(keyword, filename, inputType, 3, FileScenario() )
    InputFile(keyword::String, filename::String, inputType::InputType, column_keys::Int) = new{FileScenario}(keyword, filename, inputType, column_keys, FileScenario())
    InputFile(keyword::String, filename::String, inputType::InputType, column_keys::Int, filetype::FileScenario) = new{FileScenario}(keyword, filename, inputType, column_keys, filetype)
    InputFile(keyword::String, filename::String, inputType::InputType, column_keys::Int, filetype::FileString) = new{FileString}(keyword, filename, inputType, column_keys, filetype)
end


function open_inputs(options::BidDispatchOptions, PATH::String, maxhours::Int)
    runmode = options.runmode
    input_dict = Dict()
    quantity_dict = Dict() #Dict{String, Dict{String, Vector{String}}}()
    header_dict = Dict()

    # setup inputs
    files = InputFile[]
    # push!(files, InputFile("Fcf", "FCF23", Dictionary, 1))
    push!(files, InputFile("Fcf", "fcf", Dictionary, 1))
    if isfile(joinpath(options.INPUTPATH, "scenarios_assignments.csv")) ||
        (isfile(joinpath(options.INPUTPATH, "scenarios_assignments.bin")) &&
         isfile(joinpath(options.INPUTPATH, "scenarios_assignments.hdr")))
        push!(files, InputFile("Clusters", "scenarios_assignments", Dictionary, 3))
    else
        # println("File scenarios_assignments is not in folder, setting Clusters = 1")
    end

    if runmode == TRUEUP_MODE::RunMode
        files = append!(files, [
            InputFile("TUP_ter", "thermal_generation", Dictionary),
            InputFile("TUP_hyd", "hydro_generation", Dictionary),
            InputFile("TUP_cmit", "thermal_commitment_status", Dictionary)]
            )
            push!(files, InputFile("thermal_res_dn", "thermal_res_dn", Dictionary, 3))
            push!(files, InputFile("thermal_res_up", "thermal_res_up", Dictionary, 3))
            push!(files, InputFile("hydro_res_dn", "hydro_res_dn", Dictionary, 3))
            push!(files, InputFile("hydro_res_up", "hydro_res_up", Dictionary, 3))
    else
        if options.reserve != NoReserve::ReserveType
            push!(files, InputFile("thermal_res_dn", "thermal_res_dn", Dictionary, 3))
            push!(files, InputFile("thermal_res_up", "thermal_res_up", Dictionary, 3))
            push!(files, InputFile("hydro_res_dn", "hydro_res_dn", Dictionary, 3))
            push!(files, InputFile("hydro_res_up", "hydro_res_up", Dictionary, 3))
        end
    end

    if options.robust
        push!(files, InputFile("UC_ng", "uncertainty_set_ng", Dictionary, 1))
        push!(files, InputFile("UC_si", "uncertainty_set_si", Dictionary, 1))
    end

    if options.ExternalRenewables
        push!(files, InputFile("RenScen", "cenarios_eolica", Dictionary, 2))
    end

    # READ or OPEN files
    for inputfile in files
        key = inputfile.keyword
        name = inputfile.filename
        type = inputfile.inputType
        column_keys = inputfile.column_keys

        println("Reading file $(name)")
        binpath = joinpath(PATH, name*".bin")
        hdrpath = joinpath(PATH, name*".hdr")
        csvpath = joinpath(PATH, name*".csv")
        # check file
        if (!isfile(binpath) || !isfile(hdrpath)) && !isfile(csvpath)
            println("ERROR: file $(name) not found")
        end

        if type == Graf
            if isfile(binpath) && isfile(hdrpath)
                input_dict[key] = PSRClassesBINPtr(open_file(name*".hdr" , name*".bin", PATH))
                quantity_dict[key] = zeros(PSRIOGrafResultBase_maxAgent(input_dict[key].Ptr))
            elseif isfile(csvpath)
                input_dict[key] = PSRClassesCSVPtr(open_file(name*".csv", PATH))
                quantity_dict[key] = zeros(PSRIOGrafResultBase_maxAgent(input_dict[key].Ptr))
            end
        elseif type == External
            if isfile(csvpath)
                input_dict[key] = ExternalFilePtr(open(joinpath(PATH, name*".csv")))
                header = split(readline(input_dict[key].Ptr),",")[3:end]
                nagents = length(header)
                quantity_dict[key] = zeros(nagents)
                quantity_dict[key*"_ac"] = zeros(nagents, maxhours)
                # header_dict[key] = header
            end
        elseif type == Dictionary
            if isfile(csvpath)
                quantity_dict[key] = read_whole_file(joinpath(PATH, name*".csv"), column_keys, inputfile.filetype)
                # header_dict[key] = quantity_dict[key]["header"]
            end
        elseif type == Fcf
            if isfile(csvpath)
                quantity_dict[key] = read_Fcf(joinpath(PATH, name*".csv"))
                # header_dict[key] = quantity_dict[key]["header"]
            end
        else
            if isfile(csvpath)
                input_dict[key] = ExternalFilePtr(open(joinpath(PATH, name*".csv")))
                header = readline(input_dict[key].Ptr)
                header = split(header,",")
                nagents = length(header)
                d = Dict()
                d["header"] = strip.(header)
                for i in 1:nagents
                    d[strip(header[i])] = 0.0
                end
                quantity_dict[key] = d
            end
        end
    end
    # quantity_dict["header"] = header_dict

    return input_dict, quantity_dict
end

function read_whole_file(fpath, column_keys, filetype::FileScenario)
    if isfile(fpath)
        s = get_matrix_size(fpath, column_keys)
        # preallocate memory
        data = FileScenario(String[], zeros(Float64, s))

        open(fpath, "r") do f
            line = readline(f)

            # split and remove index columns
            splitted_line = split(line, ",")
            # d["header"] = splitted_line[column_keys+1:end]
            data.header = splitted_line[column_keys+1:end]

            for line in eachline(f)
                splitted_line = strip.(split(line, ","))

                # store values
                for idx in (column_keys+1):length(splitted_line)
                    v = splitted_line[idx]

                    # get key
                    s = get_row_index(splitted_line[1:column_keys], idx-column_keys)

                    data.value[CartesianIndex(s)] = parse(Float64, v)
                end
            end
        end
        return data
    end

    return filetype
end

function read_whole_file(fpath, column_keys, filetype::FileString)
    d = Dict()
    if isfile(fpath)
        open(fpath, "r") do f
            line = readline(f)

            # split and remove index columns
            splitted_line = split(line, ",")
            d["header"] = splitted_line[column_keys+1:end]

            for line in readlines(f)
                splitted_line = strip.(split(line, ","))
                key = join(splitted_line[1:column_keys],",")

                # store values
                d[key] = splitted_line[column_keys+1:end]
            end
        end
    end
    return d
end
function get_last_row(fpath)
    row=""
    open(fpath, "r") do f
        for line in eachline(f)
            if length(line)>1
                row = line
            end
        end
    end
    return row
end
function get_matrix_size(fpath, column_keys)
    # get last row of file
    lastrow = get_last_row(fpath)
    columns = String.(split(lastrow, ","))
    nagent = Int(length(columns) - column_keys)

    # parse first columns
    indexes = parse.(Int, columns[1:column_keys] )

    # fill size
    s = Int[]
    for (i,v) in enumerate(indexes)
        push!(s,v)
    end
    push!(s,nagent)

    t = array2tuple(s)

    return t
end
function get_row_index(indexes, col)
     # fill size
     s = Int[]
     for (i,v) in enumerate(indexes)
         push!(s, parse(Int,v))
     end
     push!(s, col)
     t = array2tuple(s)
     return t
end
function array2tuple(s)
    if length(s) == 2
        return (s[1], s[2])
    elseif  length(s) == 3
        return (s[1], s[2], s[3])
    else
        return (s[1], s[2], s[3], s[4])
    end
    return ()
end

function read_Fcf(fpath)
    d = Dict{Any,Vector{Float64}}()
    open(fpath, "r") do f
        line = readline(f)
        splitted_line = split(line, ",")
        d["header"] = splitted_line[3:end]
        nagents = length(splitted_line) - 2
        matrix = zeros(nagents)
        for line in eachline(f)
            splitted_line = split(line, ",")
            d[( parse(Int, splitted_line[1]), parse(Int, splitted_line[2]), parse(Int, splitted_line[3]), parse(Int, splitted_line[4]) ) ] = [parse(Float64, String(i)) for i in splitted_line[3:end]]
        end
    end
    return d
end

function update_quantities!(inpts, quants)
    for (key, ptr) in inpts
        if key in keys(quants)
            update_vector!(ptr, quants[key])
        end
        nextRegistry!(ptr)
    end
end

function set_scenario!(inpts, quants)
    files=[
    ("Demand", "Demand", External)]
    # ("Inflow", "Inflow", Dictionary),]
    for (key, _, __) in files
        if key in keys(quants)
            ptr = inpts[key]
            update_vector!(ptr, quants[key])
            nextRegistry!(ptr)
        end
    end
end

function nextRegistry!(graf::ExternalFilePtr)
    nothing
end

function update_vector!(graf::ExternalFilePtr, agent_values::Vector{Float64})
    stream = graf.Ptr
    line = readline(stream)
    values = [i != "" ? i : "0.0" for i in split(line,",") ]
    values = parse.(Float64, values)[3:end]
    if size(agent_values ) != size(values)
        throw("Error in $(graf) number of agents doesnt match: $(size(agent_values)) <> $(size(values))")
    end
    for (i,v) in enumerate(values)
        agent_values[i] = max(v,0)
    end
    nothing
end

function update_vector!(graf::ExternalFilePtr, agent_values::Dict{Any,Any})
    stream = graf.Ptr
    line = readline(stream)
    values = [i != "" ? i : "0" for i in split(line,",") ]
    values = parse.(Float64, values)
    for (i,v) in enumerate(values)
        agent_values[agent_values["header"][i]] = max(v,0)
    end
    nothing
end

function store_inputs!(hour::Int32, quantities::Dict{Any,Any})
    storevalues = ["PresPup", "PresSup", "PresT", "PresSdn", "PresPdn", "Ptherm", "Phydro", "Prenew", "GenMinCstr", "Qtherm", "Qhydro", "Qrenew", "Demand"]
    for f in storevalues
        if f in keys(quantities)
            quantities[f*"_ac"][:, hour] = quantities[f]
        end
    end
    nothing
end

function read_map(path)
    names = ["TresMap.csv", "SresMap.csv", "PresMap.csv"]
    mapping = Dict()
    for (i,n) in enumerate(names)
        p = joinpath(path, n)
        if isfile(p)
            mapping[n] = map(p)
        end
    end
    return mapping
end

function map(fpath)
    df = CSV.read(fpath)
    reserves = unique(df[!,Symbol("Reserve")])
    mapping = Dict()
    for r in reserves
        mapping[String(r)] = strip.(df[df[:,1] .== r,:][:,2])
    end
    return mapping
end

function close_inputs!(prb::BidDispatchProblem)
    # files
    for (key, value) in prb.data.inpts
        close_file!(value)
    end
    empty!(prb.data.inpts)

    # psrclasses pointers
    if prb.psrc.inflow != C_NULL
        PSRIOSDDPHydroForwardBackward_close(prb.psrc.inflow)
        prb.psrc.inflow = C_NULL
    end
    if !prb.options.ExternalRenewables
        if prb.psrc.gnd_ptr != C_NULL
            PSRIOElementHourlyScenarios_close(prb.psrc.gnd_ptr)
            prb.psrc.gnd_ptr = C_NULL
        end
    end
    nothing
end

function close_file!(input::ExternalFilePtr)
    close(input.Ptr)
    nothing
end

# Dispatch Config
@enum ConsiderNetwork no=0 dclink=1