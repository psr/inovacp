"""
    Economic Dispatch methods
"""

"""
    Set variable upper bounds

"""
function set_bounds!(m::JuMP.Model, prb::BidDispatchProblem, hour, scenario)
    d = prb.data
    options = prb.options

    gnd_bound!(m, prb, hour, scenario)
    hydro_outflow!(m, prb, hour, scenario)

    # bound maximum/minimum generation considering reserve
    thermal_generation_capacity!(m, prb, hour, scenario)
    if has_reserve(prb)
        hydro_generation_capacity!(m, prb, hour, scenario)
    end
    nothing
end