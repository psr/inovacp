"""
    Base methods for dispatch problem
"""
function init_jump_types!(prb::BidDispatchProblem, m)
    d = prb.data
    n = prb.n
    # initialzie expressions
    d.HTurb = @expression(m, [h=1:n.Hydros, hour=1:n.MaxHours, scen=1:n.ScenariosExtended], 0)
    d.spillage = @expression(m, [h=1:n.Hydros, hour=1:n.MaxHours, scen=1:n.ScenariosExtended], 0)
    d.TGen = @expression(m, [t=1:length(d.tExisting), hour=1:n.MaxHours, scen=1:n.ScenariosExtended], 0)

    # objective function
    d.fobj.hydroCosts = @expression(m, [1:n.ScenariosExtended], 0)
    d.fobj.FutureCost = @expression(m, [1:n.ScenariosExtended], 0)
    d.fobj.thermalCosts = @expression(m, [1:n.ScenariosExtended], 0)
    d.fobj.CommitmentCost = @expression(m, 0)
    d.fobj.RenewableCosts = @expression(m, [1:n.ScenariosExtended], 0)
    d.fobj.ReserveCosts = @expression(m, [1:n.ScenariosExtended], 0)
    d.fobj.DeficitCost = @expression(m, [1:n.ScenariosExtended], 0)
    d.fobj.RedispatchCost = @expression(m, [1:n.ScenariosExtended], 0)

    nothing
end
# create variables
function build_archetype(prb::BidDispatchProblem, mapping, solver_id)
    n = prb.n
    d = prb.data
    options = prb.options
    
    # chooses MILP solver
    if prb.options.solver == CPLEX_SOLVER::SolverId
        # opt = CPLEX.Optimizer
        error("CPLEX not supported")
    elseif prb.options.solver == GLPK_SOLVER::SolverId
        println("Using GLPK solver")
        opt = GLPK.Optimizer
    elseif prb.options.solver == XPRESS_SOLVER::SolverId
        println("Using Xpress solver")
        Xpress.initialize()
        opt = Xpress.Optimizer
    elseif prb.options.solver == CBC_SOLVER::SolverId
        println("Using Cbc solver")
        opt = Cbc.Optimizer
        # opt = optimizer_with_attributes(Cbc.Optimizer, "logLevel" => 1)
    else
        error("No solver selected")
    end

    m = Model( opt )

    if !options.solver_silent
        unset_silent(m)
    else
        set_silent(m)
    end
    if options.solver_time_limit != 0
        set_time_limit_sec(m, options.solver_time_limit)
    end

    # initialzie expressions
    init_jump_types!(prb, m)

    # add variables
    # -------------
    # hydro
    var_storage!(m::JuMP.Model, prb)
    var_fcf!(m::JuMP.Model, prb)

    # thermal
    if options.commitment
        var_COMT(m::JuMP.Model, prb)
        var_startup(m::JuMP.Model, prb)
        var_shutdown(m::JuMP.Model, prb)
    end

    # system
    var_deficit!(m::JuMP.Model, prb)

    var_interc!(m::JuMP.Model, prb)

    var_dclinks(m::JuMP.Model, prb)

    var_thetas(m::JuMP.Model, prb)

    # reserve
    var_reserve(m::JuMP.Model, prb, mapping)

    # renewables
    var_gergnd!(m::JuMP.Model, prb)

    # outflow slack
    var_outflow!(m, prb)

    # Especial variables
    # change with the runmode
    if options.fuelConsumption

        var_fuelconsumptions!(m, prb)

    else
        var_thermalgen!(m, prb)
    end

    if options.runmode == DETERMINISTIC_MODE::RunMode

        var_turbining!(m::JuMP.Model, prb)

    elseif options.runmode == AFFINE_MODE::RunMode

        var_affine!(m::JuMP.Model, prb)

    elseif options.runmode == TRUEUP_MODE::RunMode

        var_turbining!(m::JuMP.Model, prb)
        var_trueup!(m::JuMP.Model, prb)

    end

    var_robust!(m, prb)

    # build cuts
    for scen=1:n.ScenariosExtended # hour=1:n.MaxHours,
        hset = hydroBlockSet(prb)
        fcf!(m, prb, scen)
    end

    return m
end

"""
    Getters

- depend on the runMode
"""

function get_hturb(m, prb::BidDispatchProblem)
    d = prb.data
    if prb.options.runmode == DETERMINISTIC_MODE::RunMode
        return m[:HTurb]
    elseif prb.options.runmode == TRUEUP_MODE::RunMode
        return m[:TPhyd]
    else
        return d.HTurb
    end
end

function get_hturb(m, prb::BidDispatchProblem, h::Int, hour::Int32,s::Int32)
    d = prb.data
    if prb.options.runmode == DETERMINISTIC_MODE::RunMode
        return m[:HTurb][h,hour,s]
    elseif prb.options.runmode == TRUEUP_MODE::RunMode
        return m[:TPhyd][h,hour,s]
    else
        return d.HTurb[h,hour,s]
    end
end

function get_spillage(m,prb::BidDispatchProblem)
    d = prb.data
    if prb.options.runmode == DETERMINISTIC_MODE::RunMode
        return m[:spillage]
    elseif prb.options.runmode == TRUEUP_MODE::RunMode
        return m[:TPspil]
    else
        return m[:spillage] # d.spillage
    end
end

function get_tgen(m, prb::BidDispatchProblem)
    d = prb.data
    options = prb.options

    if options.runmode == TRUEUP_MODE::RunMode
        return m[:TPter]
    elseif !prb.options.fuelConsumption
        return m[:TGen]
    else
        return d.TGen
    end
end

function get_demand(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    return get_demand(prb, d.hour)
end
function get_demand(prb::BidDispatchProblem, hour)
    n = prb.n
    d = prb.data
    options = prb.options

    if options.ExplicitRen
        return d.demandHour
    else
        if options.runmode == DETERMINISTIC_MODE::RunMode
            return d.netdemand[:, hour, prb.n.ScenariosExtended]
        else
            return d.netdemand[:, hour, d.scen]
        end
    end
end

function get_renewable(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    if options.runmode == DETERMINISTIC_MODE::RunMode
        return d.rengen[:, prb.data.hour, prb.n.ScenariosExtended]
    else
        return d.rengen[:, prb.data.hour, prb.data.scen]
    end
end

function get_renewable!(prb::BidDispatchProblem, vec::Vector{Float64})
    n = prb.n
    d = prb.data
    options = prb.options

    vec = d.rengen[:, prb.data.hour, prb.data.scen]

    nothing
end

function get_inflow(prb::BidDispatchProblem, hour)
    n = prb.n
    d = prb.data
    options = prb.options

    @assert hour == 1
    # for now only one single value for the entire stage

    return d.inflow_forecast
end

"""
    Variable's Limit
"""
function gnd_bound!(m::JuMP.Model, prb::BidDispatchProblem, hour, scenario)
    n = prb.n
    d = prb.data
    options = prb.options
    if n.Gnd>0
        if options.ExplicitRen
            RGen = m[:RGen]
            ren_gen = get_renewable(prb)

            for r in 1:n.Gnd
                if scenario <= n.Scenarios
                    set_upper_bound.(RGen[r, hour, scenario], ren_gen[r])
                else
                    set_upper_bound.(RGen[r, hour, scenario], d.forecast[r, hour])
                end
            end
        elseif options.SpillRen # bound renewable spillage
            Rspill = m[:Rspill]

            demand = d.demandHour
            net_demand = get_demand(prb)

            # renewable generation is the diff
            for dem in 1:n.Demand
                set_upper_bound(Rspill[dem, hour, scenario], demand[dem] - net_demand[dem])
            end
        end
    end
end

function trueup_bound!(m::JuMP.Model, prb::BidDispatchProblem, hour, scenario)
    n = prb.n
    d = prb.data
    options = prb.options
    quantities = prb.data.quantities

    # regular unlimited
    # trueup_bound_redispatch_unlimited!(m::JuMP.Model, prb::BidDispatchProblem, hour, scenario)

    # limit redispatch inside reserves
    trueup_bound_reserves!(m::JuMP.Model, prb::BidDispatchProblem, hour, scenario)
end

function trueup_bound_redispatch_unlimited!(m::JuMP.Model, prb::BidDispatchProblem, hour, scenario)
    n = prb.n
    d = prb.data
    options = prb.options
    quantities = prb.data.quantities

    if n.Hydros>0
        HTurb = get_hturb(m, prb)
        # Turbined water (in hm3)
        TPHs1= m[:TPHs1]
        TPHs2 = m[:TPHs2]
        for h in 1:n.Hydros
            if d.hExist[h] <= 0 && d.fprodt[h] > 0
                r = d.hyd2tup[h]

                if quantities["TUP_hyd"].value[prb.options.stage, end, hour, h] <= 0.1

                    e_cstr = @constraint(m, HTurb[h, hour, scenario] * d.fprodt[h] ==  quantities["TUP_hyd"].value[prb.options.stage, end, hour, h] )
                    set_name(e_cstr, "HydTU($h,$hour,$scenario)")
                else
                    e_cstr = @constraint(m, HTurb[h, hour, scenario] * d.fprodt[h] + TPHs1[h, hour, scenario] - TPHs2[h, hour, scenario] ==  quantities["TUP_hyd"].value[prb.options.stage, end, hour, h] )
                    set_name(e_cstr, "HydTU($h,$hour,$scenario)")
                end
            end
        end
    end

    if n.Thermals>0
        TGen = get_tgen(m, prb)
        TPTs1= m[:TPTs1]
        TPTs2 = m[:TPTs2]
        for (j,t) in enumerate(d.tExisting)
            r = d.ter2tup[t]
            if r != 0
                if quantities["TUP_ter"].value[prb.options.stage, end,hour, j] <= 0.1
                    e_cstr = @constraint(m,TGen[j, hour, scenario] == quantities["TUP_ter"].value[prb.options.stage, end,hour, j] )
                    set_name(e_cstr, "TerTU($j,$hour,$scenario)")
                else
                    e_cstr = @constraint(m, TGen[j, hour, scenario] + TPTs1[j, hour, scenario] - TPTs2[j, hour, scenario] == quantities["TUP_ter"].value[prb.options.stage, end,hour, j] )
                    set_name(e_cstr, "TerTU($j,$hour,$scenario)")
                end
            end
        end
    end
end

function trueup_bound_reserves!(m::JuMP.Model, prb::BidDispatchProblem, hour, scenario)
    n = prb.n
    d = prb.data
    options = prb.options
    quantities = prb.data.quantities

    if n.Hydros>0
        HTurb = get_hturb(m, prb)
        # Turbined water (in hm3)
        TPHs1= m[:TPHs1]
        TPHs2 = m[:TPHs2]
        TPHs1slk = m[:TPHs1slk]
        TPHs2slk = m[:TPHs2slk]
        for h in 1:n.Hydros
            if d.hExist[h] <= 0 && d.fprodt[h] > 0
                r = d.hyd2tup[h]
                e_cstr = @constraint(m, HTurb[h, hour, scenario] * d.fprodt[h] + TPHs1[h, hour, scenario] - TPHs2[h, hour, scenario] ==  quantities["TUP_hyd"].value[prb.options.stage, end, hour, h] )
                set_name(e_cstr, "HydTU($h,$hour,$scenario)")

                e_cstr = @constraint(m, TPHs1[h, hour, scenario] - TPHs1slk[h, hour, scenario] <=  d.quantities["hydro_res_up"].value[1,1,hour,h])
                set_name(e_cstr, "ResupHTU($h,$hour,$scenario)")
                e_cstr = @constraint(m, TPHs2[h, hour, scenario] - TPHs2slk[h, hour, scenario] <=  d.quantities["hydro_res_dn"].value[1,1,hour,h])
                set_name(e_cstr, "ResdnHTU($h,$hour,$scenario)")

                add_to_expression!(d.fobj.ReserveCosts[scenario], ( (TPHs1slk[h,hour,scenario] + TPHs2slk[h,hour,scenario]) * maximum(d.fcsCost)  )
                )
            end
        end
    end

    if n.Thermals>0
        TGen = get_tgen(m, prb)
        TPTs1= m[:TPTs1]
        TPTs2 = m[:TPTs2]
        TPTs1slk = m[:TPTs1slk]
        TPTs2slk = m[:TPTs2slk]
        for (j,t) in enumerate(d.tExisting)
            r = d.ter2tup[t]
            if r != 0
                e_cstr = @constraint(m, TGen[j, hour, scenario] + TPTs1[j, hour, scenario] - TPTs2[j, hour, scenario] == quantities["TUP_ter"].value[prb.options.stage, end,hour, j] )
                set_name(e_cstr, "TerTU($j,$hour,$scenario)")
                e_cstr = @constraint(m, TPTs1[j, hour, scenario] - TPTs1slk[j, hour, scenario] <=  d.quantities["thermal_res_up"].value[1,1,hour,j])
                set_name(e_cstr, "ResupTTU($j,$hour,$scenario)")
                e_cstr = @constraint(m, TPTs2[j, hour, scenario] - TPTs2slk[j, hour, scenario] <=  d.quantities["thermal_res_dn"].value[1,1,hour,j])
                set_name(e_cstr, "ResdnTTU($j,$hour,$scenario)")

                add_to_expression!(d.fobj.ReserveCosts[scenario], ( (TPTs1slk[j,hour,scenario] + TPTs2slk[j,hour,scenario]) * maximum(d.fcsCost))
                )
            end
        end
    end
end

# Fuel consumption
function fuel_consumption!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    MAX_EFF_SEGS = 3

    if n.FuelCons > 0
        TGen = get_tgen(m, prb)
        FC = m[:FC]

        hr = prb.data.hour
        s = prb.data.scen

        if options.fuelConsumption

            FuelConsAFF = @constraint(m, [t = 1:length(d.tExisting)], TGen[t, hr, s] <= sum(FC[ l , j , hr, s] for j in 1:n.FuelCons, l in 1:d.fcs_nsegments[j] if d.tExisting[t] == d.fcs2ter[j]) )

            for t=1:length(d.tExisting)
                set_name(FuelConsAFF[t], "FuelConsAFF($t, $hr, $s)")
            end
        end
    end
end


"""
    Physical Modeling
"""
function hydro_outflow!(m::JuMP.Model, prb::BidDispatchProblem, hr::Int32, s::Int32)
    n = prb.n
    d = prb.data
    options = prb.options
    max_hour = n.MaxHours
    if n.Hydros>0
        HTurb = get_hturb(m, prb)
        def_slack = m[:def_slack]

        for i=1:n.Hydros
            if d.defmin[i] > 0
                defmin = @constraint(m, HTurb[i,hr,s] + def_slack[i,s] >= d.defmin[i])
                set_name(defmin, "defmin($i, $(hr), $s)")
                # defmax = @constraint(m, [i=1:n.Hydros], HTurb[i,hr,s] <= d.defmax[i])
                # set_name(defmax, "defmax($i, $(hr), $s)")
            end
        end
    end
    nothing
end

# Thermal unit COMT
function thermal_unit_COMT(m::JuMP.Model, prb::BidDispatchProblem, hour)
    options = prb.options
    quants = prb.data.quantities

    if options.minupdntime
        minimum_up_time2!(m, prb, hour)
        minimum_down_time2!(m, prb, hour)
    end

    # using tight formulation
    if options.commitment
        tight_startup_shutdown3!(m, prb, hour)
    end

    # thermal_unit_COMT
    if options.ramp
        ramp_up!(m, prb, hour, n.ScenariosExtended)
        ramp_down!(m, prb, hour, n.ScenariosExtended)
    end

    # startup costs
    if options.startupcost
        startupcost!(m, prb, hour)
    end

    nothing
end

function maximum_up_time!(m::JuMP.Model, prb::BidDispatchProblem, hour, max_hour,s)
    x   = m[:COMT]
    tau = prb.data.tMinUptime

    for (j,t) in enumerate(prb.data.tExisting)
        MaximumUpTime  =  @constraint(m, sum(x[j, hour + k] for k in 0:min(tau[t] + 1, max_hour - hour)) <= tau[t])
        set_name(MaximumUpTime, "MaxUT($j, $(hour), $s)")
    end
    nothing
end

function minimum_up_time2!(m::JuMP.Model, prb::BidDispatchProblem, hour)
    x   = m[:COMT]
    y   = m[:STup]
    tau = Int.(prb.data.tMinUptime)

    # operative hours
    for (j,t) in enumerate(prb.data.tExisting)
        if tau[t] > 0 && prb.data.is_commit[t] > 0
            mint = max(1, (hour - tau[t] + 1) )
            MinimumUpTime =  @constraint(m, sum(y[j, k] for k in mint:hour ) <= x[j,hour])

            set_name(MinimumUpTime, "MinUT($j, $(hour))")
        end
    end
    nothing
end

function minimum_down_time2!(m::JuMP.Model, prb::BidDispatchProblem, hour)
    w   = m[:STdn]
    x   = m[:COMT]
    tau = Int.(prb.data.tMinDowntime)

    # operative hours
    for (j,t) in enumerate(prb.data.tExisting)
        if tau[t] > 0 && prb.data.is_commit[t] > 0
            mint = max(1, (hour - tau[t] + 1) )
            MinimumDownTime = @constraint(m, sum(w[j, k] for k in mint:hour ) <= 1 - x[j,hour])
            set_name(MinimumDownTime, "MinDT($j, $(hour ))")
        end
    end
    nothing
end

function ramp_up!(m::JuMP.Model, prb::BidDispatchProblem, hour, s)
    options = prb.options
    quants = prb.data.quantities
    max_hour = prb.n.MaxHours
    TGen = get_tgen(m, prb)

    x=1.0
    y=1.0
    if options.commitment
        y= m[:STup]
        x= m[:COMT]
    end

    for (j,t) in enumerate(prb.data.tExisting)
        if hour <= max_hour && prb.data.tRampUpCold[t] > 0
            if hour > 1 && (prb.data.tRampUpCold[t] + prb.data.tRampUpWarm[t]) > 1
                # RampUp = @constraint(m, TGen[j,hour,s] - TGen[j,hour-1,s] <= (prb.data.tRampUpCold[t])* y[j, hour] + prb.data.tRampUpWarm[t] * x[j,hour-1])
                RampUp = @constraint(m, TGen[j,hour,s] - TGen[j,hour-1,s] <= prb.data.tRampUpWarm[t] * x[j,hour-1])
                set_name(RampUp, "RampUp($j, $(hour), $s)")
            else
                # RampUp[j, hour] = @constraint(m, TGen[j, hour, s] - prb.data.initial_power[j] <= (prb.data.tRampUpCold[j])* (1-prb.data.initial_state_commit[j]) + prb.data.tRampUpWarm[j] * prb.data.initial_state_commit[j])
                # set_name(RampUp[j, hour], "RampUp($j, $(hour), $s)")
            end
        end
    end
    nothing
end

function ramp_down!(m::JuMP.Model, prb::BidDispatchProblem, hour, s)
    quants = prb.data.quantities
    options = prb.options
    max_hour = prb.n.MaxHours

    x=1.0
    w=1.0
    if options.commitment
        w= m[:STdn]
        x= m[:COMT]
    end

    TGen = get_tgen(m, prb)

    for (j,t) in enumerate(prb.data.tExisting)
        if hour <= max_hour && prb.data.tRampDown[t] > 0
            if hour > 1 && (prb.data.tRampUpCold[t] + prb.data.tRampUpWarm[t]) > 1

                RampDown = @constraint(m, TGen[j,hour,s] - TGen[j,hour-1,s] >= -prb.data.tRampDown[t]*x[j,hour])
                set_name(RampDown, "RampDown($j, $(hour), $s)")
            else
                # RampDown[j, hour] = @constraint(m, TGen[j, hour, s] - prb.data.initial_power[j] >= -prb.data.tRampDown[j]*x[j,hour, n.ScenariosExtended] - prb.data.tGerMin[j]*w[j,hour, n.ScenariosExtended])
                # set_name(RampDown[j, hour], "RampDown($j, $(hour), $s)")
            end
        end
    end
    nothing
end

function startupcost!(m::JuMP.Model, prb::BidDispatchProblem, hour)
    if !prb.data.startup_old && prb.options.startupcost
        x   = m[:COMT]
        y   = m[:STup]
        c   = m[:STupCost]
        w   = m[:STdn]
        u = m[:State]
        throw(1)
        for j in prb.data.tExisting
            if prb.data.tExist[j] <= 0 && prb.data.is_commit[j] > 0
                # startup cost varying with time
                # time after generator gets cold
                Tmc = prb.data.timeIsCold[j]
                # downtime
                DT = prb.data.tMinDowntime[j]
                StartupCost1 = @constraint(m, sum( u[j, h, hour]  for h in collect(Int, max(Int(hour - Tmc + 1),1):(hour -     DT)) if h <= prb.n.MaxHours) <= y[j, hour])
                set_name(StartupCost1, "StartupCost1($j, $(hour))")
                StartupCost2 = @constraint(m, sum( u[j, hour, h] for h in collect(Int, (hour + DT):min(hour + Tmc - 1,     prb.n.MaxHours)) if h > 0 && h <= prb.n.MaxHours) <= w[j, hour])
                set_name(StartupCost2, "StartupCost2($j, $(hour))")
                # build cost
                Cc = prb.data.startupCold[j]
                Cw = prb.data.startupWarm[j]
                Tmw =  prb.data.timeIsWarm[j]
                Ch =prb.data.startupHot[j]
                Tmh = DT
                # constraints
                StartupCost3 = @constraint(m, c[j, hour] == Cc*y[j, hour]
                # hot state
                + (Ch - Cc) * sum(u[j,h,hour] for h in collect(Int, max(Int(hour - Tmw + 1), 1):(hour - Tmh)) if h <= prb.n.MaxHours)
                # warm state
                + (Cw - Cc) * sum(u[j,h,hour] for h in collect(Int, max(Int(hour - Tmc + 1), 1):(hour - Tmw)) if h <= prb.n.MaxHours)
                )
                set_name(StartupCost3, "StartupCost3($j, $(hour))")
            end
        end
    end
end

function tight_startup_shutdown3!(m, prb::BidDispatchProblem, hour)
      #! 46) Maximum Number of Thermal Start-Ups
      x   = m[:COMT]
      y   = m[:STup]
      w   = m[:STdn]

    for (j,t) in enumerate(prb.data.tExisting)
        if prb.data.tExist[t] <= 0 && prb.data.is_commit[t] > 0
            # count startups
            if hour == 1
                # convention : initial state = 0
                # StartUpCstr[j, hour] =  @constraint(m, y[j, hour,n.ScenariosExtended] - w[j, hour,n.ScenariosExtended] == x[j, hour,n.ScenariosExtended] - prb.data.initial_state_commit[j])
                # StateIsOff[j, hour] =  @constraint(m, y[j, hour,n.ScenariosExtended] + w[j, hour,n.ScenariosExtended] <= x[j, hour,n.ScenariosExtended] + prb.data.initial_state_commit[j])
                # StateIsOn[j, hour] =  @constraint(m, y[j, hour,n.ScenariosExtended] + w[j, hour,n.ScenariosExtended] + x[j, hour,n.ScenariosExtended] + prb.data.initial_state_commit[j] <= 2)
            else
                StartUpCstr =  @constraint(m, y[j, hour] - w[j, hour] == x[j, hour] - x[j, hour-1])
                StateIsOff =  @constraint(m, y[j, hour] + w[j, hour] <= x[j, hour] + x[j, hour-1])
                StateIsOn =  @constraint(m, y[j, hour] + w[j, hour] + x[j, hour] + x[j, hour-1] <= 2)
                set_name(StartUpCstr, "CmtUpDn($j, $(hour))")
                set_name(StateIsOff, "StOff($j, $(hour))")
                set_name(StateIsOn, "StOn($j, $(hour))")
            end
        end
    end
      nothing
end

# System constraints
function load_balance!(m::JuMP.Model, prb::BidDispatchProblem, hour, s)
    n = prb.n
    d = prb.data
    options = prb.options
    quantities = prb.data.quantities

    # load variables into scope
    # -------------------------
    def = m[:def]

    demand = get_demand(prb)

    if n.Thermals>0
        g_ter = get_tgen(m, prb)
    end

    if n.Gnd>0
        if options.ExplicitRen
            g_gnd = m[:RGen]
        elseif options.SpillRen
            Rspill = m[:Rspill]
        end
    end
    if n.Hydros>0
        g_hid = get_hturb(m, prb)
    end

    if options.network == 0
        hyd_gen = AffExpr(0.0)

        for i in 1:n.Hydros
            if d.hExist[i] <= 0
                add_to_expression!(hyd_gen, prb.data.fprodt[i] * g_hid[i,hour,s])
            end
        end

        ter_gen = AffExpr(0.0)

        for (j,t) in enumerate(d.tExisting)
            if d.tExist[t] <= 0
                add_to_expression!(ter_gen, g_ter[j,hour,s] )
            end
        end

       loadbal = @constraint(m,
            # hydro
            hyd_gen

            # thermal
            + ter_gen

            # renewable
            + sum(g_gnd[r, hour, s] for r in 1:n.Gnd if d.gExist[r] <= 0 && options.ExplicitRen)

            # deficit
            + sum( def[1, hour, s])

            # demand
            - sum(demand)

            # renewable spillage for implicit representation
            - sum(Rspill[dem, hour, s] for dem in 1:n.Demand if !options.ExplicitRen && options.SpillRen)
            == 0.0
        )

        set_name(loadbal, "Lbal(1,$hour,$s)")
    elseif options.network == 1 # INTERCONNECTION
        IntercTo = m[:IntercTo]
        IntercFrom = m[:IntercFrom]

        for b in 1:n.Sys

            hyd_gen = AffExpr(0.0)
            for i in 1:n.Hydros
                if d.hyd2sys[i] == b && d.hExist[i] <= 0
                    add_to_expression!(hyd_gen, prb.data.fprodt[i] * g_hid[i,hour,s])
                end
            end

            ter_gen = AffExpr(0.0)
            for (j,t) in enumerate(d.tExisting)
                if d.ter2sys[t] == b && d.tExist[t] <= 0
                    add_to_expression!(ter_gen, g_ter[j,hour,s] )
                end
            end

            loadbal = @constraint(m,
                # hydro
                hyd_gen

                # thermal
                + ter_gen

                # # renewable
                + sum(g_gnd[r, hour, s] for r in 1:n.Gnd if d.gnd2sys[r] == b && d.gExist[r] <= 0 && options.ExplicitRen)

                # network
                # entering node
                - sum( (1-d.iLossTo[l]) * IntercTo[l, hour, s] for l in 1:n.Intercs if d.int2sys[l] == b)
                + sum(  IntercFrom[l, hour, s] for l in 1:n.Intercs if d.int2sys[l] == b)

                # leaving node
                - sum((1-d.iLossFrom[i]) * IntercFrom[i, hour, s] for i in 1:n.Intercs if d.intFsys[i] == b )
                + sum(IntercTo[i, hour, s] for i in 1:n.Intercs if d.intFsys[i] == b)

                # deficit
                + def[b, hour, s]

                # demand
                - sum(demand[ d.sys2dem[b] ] )

                # renewable spillage for implicit representation
                - sum(Rspill[dem, hour, s] for dem in 1:n.Demand if !options.ExplicitRen && options.SpillRen)

                == 0.0
            )
            set_name(loadbal, "Lbal($b,$hour,$s)")
        end
    elseif options.network == 2 # NETWORK
        DCTo = m[:DCTo]
        DCF = m[:DCF]
        for b in 1:n.Buses
            dem = d.bus2dem[b] > 0 ? demand[d.bus2dem[b]] : 0
            loadbal = @constraint(m,
                # hydro
                sum( prb.data.fprodt[i] * g_hid[i,hour,s] for i in 1:n.Hydros if d.hyd2Bus[i] == b && d.hExist[i] <= 0)

                # thermal
                + sum(g_ter[j,hour,s] for (j,t) in enumerate(d.tExisting) if d.therm2Bus[t] == b && d.tExist[t] <= 0)

                # renewable
                + sum(g_gnd[r, hour, s] for r in 1:n.Gnd if d.ren2Bus[r] == b && d.gExist[r] <= 0 && options.ExplicitRen)

                # network
                # entering node
                - sum( (1-d.dLossTo[l]) * DCTo[l, hour, s] for l in 1:n.DCLinks if d.dcl2bus[l] == b)
                + sum(  DCF[l, hour, s] for l in 1:n.DCLinks if d.dcl2bus[l] == b)

                # leaving node
                - sum((1-d.dLossFrom[i]) * DCF[i, hour, s] for i in 1:n.DCLinks if d.dclFbus[i] == b )
                + sum(DCTo[i, hour, s] for i in 1:n.DCLinks if d.dclFbus[i] == b)

                # deficit
                + sum(def[b, hour, s])

                # demand
                - dem

                == 0.0
            )
            set_name(loadbal, "Lbal($b,$hour,$s)")
        end
    end

    nothing
end

function network!(m::JuMP.Model, prb::BidDispatchProblem, hour, s)
    n = prb.n
    d = prb.data
    options = prb.options
    quantities = prb.data.quantities

    if options.network == 2
        DCTo = m[:DCTo]
        DCF = m[:DCF]
        theta = m[:theta]

        if options.kirchoff2ndlaw != 0
            for l in 1:n.DCLinks
                loadFlowTo = @constraint(m, DCTo[l, hour, s] - DCF[l, hour, s] == (theta[d.dcl2bus[l], hour, s] - theta[d.dclFbus[l], hour, s])/ d.dReactanceTo[l] )
                set_name(loadFlowTo, "loadFlowTo($l, $hour, $s)")
            end
        end

        # sum of circuits constraint
        for cstr in d.aCstr
            afrom = cstr[1]
            ato = cstr[2]
            limit = cstr[3]
            areaCstr = @constraint(m, sum(DCTo[l, hour, s] for l in 1:n.DCLinks if d.bus2area[d.dcl2bus[l]] == afrom && d.bus2area[d.dclFbus[l]] == ato) + sum(DCF[l, hour, s] for l in 1:n.DCLinks if  d.bus2area[d.dclFbus[l]] == afrom && d.bus2area[d.dcl2bus[l]]== ato) <= limit )

            set_name(areaCstr, "SumCircuitsCstr($afrom, $ato)")
        end

        # Minimum area generation constraint
        if quantities["header"]["AreaGenMinCstr"] != []
            if options.runmode == DETERMINISTIC_MODE::RunMode
                HTurb =m[:HTurb]
            else
                HTurb = get_hturb(m, prb)
            end
            if options.runmode == DETERMINISTIC_MODE::RunMode
                TGen =m[:TGen]
            else
                TGen = get_tgen(m, prb)
            end
            RGen = m[:RGen]
            areaGenMinCstr = Dict()
            for (idx, aname) in enumerate(quantities["header"]["AreaGenMinCstr"])

                area_idx = d.areaCstr2area[idx]
                areaGenMinCstr[idx, hour] = @constraint(m, sum(prb.data.fprodt[l] * HTurb[l, hour, s] for l in 1:n.Hydros if  d.bus2area[d.hyd2Bus[l]] == area_idx )
                + sum(TGen[l, hour, s] for l in d.tExisting if  d.bus2area[d.therm2Bus[l]] == area_idx )
                + sum(RGen[l, hour, s] for l in 1:n.Gnd if  d.bus2area[d.ren2Bus[l]] == area_idx )
                >= quantities["AreaGenMinCstr"][idx])

                set_name(areaGenMinCstr[idx, hour], "areaGenMinCstr($idx, $hour)")
            end
        end

        # area import / export constraint
        areaExpCstr = Dict()
        areaImpCstr = Dict()
        for cstr in d.aExpCstr
            aidx = cstr[1]
            importlimit = cstr[2]
            exportlimit = cstr[3]
            #  export from area
            # positive sense + negative sense <= limit
            areaExpCstr[aidx, hour, s] = @constraint(m, sum(DCTo[l, hour, s] for l in 1:n.DCLinks if  d.bus2area[d.dclFbus[l]] == aidx && d.bus2area[d.dcl2bus[l]] != aidx) + sum(DCF[l, hour, s] for l in 1:n.DCLinks if d.bus2area[d.dcl2bus[l]] == aidx && d.bus2area[d.dclFbus[l]] != aidx) <= exportlimit
            )
            # import from area
            areaImpCstr[aidx, hour, s] = @constraint(m, sum(DCTo[l, hour, s] for l in 1:n.DCLinks if  d.bus2area[d.dcl2bus[l]] == aidx && d.bus2area[d.dclFbus[l]] != aidx) + sum(DCF[l, hour, s] for l in 1:n.DCLinks if d.bus2area[d.dclFbus[l]] == aidx && d.bus2area[d.dcl2bus[l]] != aidx) <= importlimit
            )

            set_name(areaExpCstr[aidx, hour, s], "areaExpCstr($aidx, $hour, $s)")
            set_name(areaImpCstr[aidx, hour, s], "areaImpCstr($aidx, $hour, $s)")
        end
    end
    nothing
end

function hydroBlockSet(prb::BidDispatchProblem)
    options = prb.options
    n = prb.n

    blockset_length = n.MaxHours / options.hydroBlocks

    if blockset_length < 1
        throw("Block lenght error: hydroblocks$(options.hydroBlocks) and max hours$(n.MaxHours)")
    end
    blockset = []
    # build blocks of hours
    for i in 1:options.hydroBlocks
        hours_in_block = collect(Int32, (1 + blockset_length*(i - 1)):(blockset_length*(i) ) )
        push!(blockset, hours_in_block)
    end

    return blockset
end

function water_balance!(m::JuMP.Model, prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    options = prb.options
    quantities = prb.data.quantities
    # load variables in to scope
    # --------------------------
    if n.Hydros>0
        HVolF = m[:HVolF]
        spillage = get_spillage(m, prb)
        turbining = get_hturb(m, prb)

        # m3/s -> hm3/h
        # 1m3 = 1.0E-6 hm3
        # 1 /s = 3600 /h
        conv = 1.0*10^(-6) * 60 * 60

        # set of hours
        HSet = hydroBlockSet(prb)
        nHset = length(HSet)

        # upstream spillage
        up_spill = @expression(m, [h=1:n.Hydros, iset=1:nHset, s=1:n.ScenariosExtended], length(d.upstream_spill[h]) >= 1 ? sum(spillage[j,hour,s] for j in d.upstream_spill[h], hour in HSet[iset] ) : 0.0)
        # upstream turbining
        up_turbining = @expression(m, [h=1:n.Hydros, iset=1:nHset, s=1:n.ScenariosExtended], length(d.upstream_turb[h]) >= 1 ? sum(turbining[j,hour,s] for j in d.upstream_turb[h], hour in HSet[iset] ) : 0.0 )

        for (hblock, hset) in enumerate(HSet)

            for s in collect(Int32, 1:n.ScenariosExtended)
                pull_inflow_data!(prb, s)
                inflow = get_inflow(prb, 1)

                for (idxh,h) in enumerate(d.reservoirs)
                    stored_water = d.volini[h]
                    if hblock > 1
                        stored_water = HVolF[idxh, hblock-1, s]
                    end

                    HydroBalance =  @constraint(m, HVolF[idxh, hblock, s] ==
                    + stored_water
                    + conv * inflow[d.hyd2station[h]] * length(hset)
                    - conv * sum(spillage[h,hour,s] for hour in hset)
                    - conv * sum(turbining[h,hour,s] for hour in hset)
                    # upstream plants
                    + conv * up_turbining[h, hblock, s]
                    + conv * up_spill[h, hblock, s]
                    )
                    set_name(HydroBalance, "HBal($h,$hblock,$s)")
                end

                for h in d.runoffriver
                    HydroBalance =  @constraint(m,0 ==
                        + conv * inflow[d.hyd2station[h]] * length(hset)
                        - conv * sum(spillage[h,hour,s] for hour in hset)
                        - conv * sum(turbining[h,hour,s] for hour in hset)
                        # upstream plants
                        + conv * up_turbining[h, hblock, s]
                        + conv * up_spill[h, hblock, s]
                    )
                    set_name(HydroBalance, "HBal($h,$hblock,$s)")
                end
            end
        end
    end
end

# Reserve constraints

# Reserve constraints
function reserve!(m::JuMP.Model, prb::BidDispatchProblem, hour, mapping)
    options = prb.options
    d = prb.data
    n = prb.n
    if has_reserve(prb)

        # 1.1 - 5% da carga DESSEM CAG
        if options.reserve == CAGLoad::ReserveType
            reserve_CAG_DESSEM!(m::JuMP.Model, prb::BidDispatchProblem, hour)

        # 2.1
        elseif options.reserve == CAGPercent::ReserveType
            reserve_CAG_percent_igual!(m::JuMP.Model, prb::BidDispatchProblem, hour)

        # 2.2
        elseif options.reserve == CAGCoopt::ReserveType
            reserve_CAG_coopt!(m::JuMP.Model, prb::BidDispatchProblem, hour)

        # regular
        elseif options.reserve == Individual::ReserveType
            reserve_AFF!(m::JuMP.Model, prb::BidDispatchProblem, hour)

        elseif options.reserve == Coopt::ReserveType
            reserve_AFF_coopt!(m::JuMP.Model, prb::BidDispatchProblem, hour)
        elseif  options.reserve == CooptByCategory::ReserveType
            reserve_AFF_coopt_cat!(m::JuMP.Model, prb::BidDispatchProblem, hour)
        end
    end
end

# Reserve constraints
function reserve_CAG_percent_igual!(m::JuMP.Model, prb::BidDispatchProblem, hour)
    options = prb.options
    d = prb.data
    n = prb.n
    if has_reserve(prb)
        hidrosCAG=[18, 61, 24, 6, 34, 8, 17, 25]

        # DOWN reserve
        RdnHyd = m[:RdnHyd]
        RdnTer = m[:RdnTer]
        hyd_dn_res = @expression(m, sum( RdnHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0))
        ter_dn_res =  @expression(m, sum( RdnTer[t,hour] for t=1:length(d.tExisting) ))

        reserve_requirement_down =  sum(d.quantities["thermal_res_dn"].value[1,1,hour,:]) + sum(d.quantities["hydro_res_dn"].value[1,1,hour,:])

        RdnTCstr = @constraint(m, hyd_dn_res + ter_dn_res == reserve_requirement_down )
        set_name(RdnTCstr, "RdnTCstr($hour)")

        # UP reserve
        RupHyd = m[:RupHyd]
        RupTer = m[:RupTer]
        hyd_up_res = @expression(m, sum( RupHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0))
        ter_up_res =  @expression(m, sum( RupTer[t,hour] for t=1:length(d.tExisting) ))
        reserve_requirement_up =  sum(d.quantities["thermal_res_up"].value[1,1,hour,:]) + sum(d.quantities["hydro_res_up"].value[1,1,hour,:])

        RupHCstr = @constraint(m, hyd_up_res + ter_up_res == reserve_requirement_up )
        set_name(RupHCstr, "RupHCstr($hour)")

        # SIMULACAO 1- UHE do CAG com % igual
        RdnHydSlk = m[:RdnHydSlk]
        RupHydSlk = m[:RupHydSlk]
        nh = length(hidrosCAG)
        for h in d.reservoirs
            if d.hCode[h] in hidrosCAG
                @constraint(m, RdnHyd[h,hour] + RdnHydSlk[h,hour] ==  reserve_requirement_down / nh )
                @constraint(m, RupHyd[h,hour] + RupHydSlk[h,hour] ==  reserve_requirement_up / nh )
            end
        end

        add_to_expression!(d.fobj.ReserveCosts[1], 3000* sum(RdnHydSlk[:,hour]))
        add_to_expression!(d.fobj.ReserveCosts[1], 3000* sum(RupHydSlk[:,hour]))
    end
end

function reserve_CAG_coopt!(m::JuMP.Model, prb::BidDispatchProblem, hour)
    options = prb.options
    d = prb.data
    n = prb.n
    if has_reserve(prb)
        hidrosCAG=[18, 61, 24, 6, 34, 8, 17, 25]

        # slack
        RdnHydSlk = m[:RdnHydSlk]
        RupHydSlk = m[:RupHydSlk]

        # DOWN reserve
        RdnHyd = m[:RdnHyd]
        RdnTer = m[:RdnTer]
        hyd_dn_res = @expression(m, sum( RdnHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0 ))
        hyd_dn_res_CAG = @expression(m, sum(RdnHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0 && d.hCode[h] in hidrosCAG))
        ter_dn_res =  @expression(m, sum( RdnTer[t,hour] for t=1:length(d.tExisting) ))

        reserve_requirement_down = sum(d.quantities["thermal_res_dn"].value[1,1,hour,:]) + sum(d.quantities["hydro_res_dn"].value[1,1,hour,:])

        RdnTCstr = @constraint(m, hyd_dn_res + ter_dn_res ==  reserve_requirement_down)
        RdnTCstrCAG = @constraint(m, hyd_dn_res_CAG + sum(RdnHydSlk[:,hour]) == reserve_requirement_down)
        set_name(RdnTCstr, "RdnTCstr($hour)")

        # UP reserve
        RupHyd = m[:RupHyd]
        RupTer = m[:RupTer]
        hyd_up_res = @expression(m, sum( RupHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0))
        hyd_up_res_CAG = @expression(m, sum( RupHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0 && d.hCode[h] in hidrosCAG))
        ter_up_res =  @expression(m, sum( RupTer[t,hour] for t=1:length(d.tExisting) ))

        reserve_requirement_up =  sum(d.quantities["hydro_res_up"].value[1,1,hour,:]) + sum(d.quantities["thermal_res_up"].value[1,1,hour,:])

        RupHCstr = @constraint(m, hyd_up_res + ter_up_res == reserve_requirement_up)
        RupHCstrCAG = @constraint(m, hyd_up_res_CAG + sum(RupHydSlk[:,hour]) == reserve_requirement_up)
        set_name(RupHCstr, "RupHCstr($hour)")

        add_to_expression!(d.fobj.ReserveCosts[1], 3000* sum(RdnHydSlk[:,hour]))
        add_to_expression!(d.fobj.ReserveCosts[1], 3000* sum(RupHydSlk[:,hour]))
    end
end

function reserve_CAG_DESSEM!(m::JuMP.Model, prb::BidDispatchProblem, hour)
    options = prb.options
    d = prb.data
    n = prb.n
    if has_reserve(prb)
        hidrosCAG=[18, 61, 24, 6, 34, 8, 17, 25]

        # slack
        RdnHydSlk = m[:RdnHydSlk]
        RupHydSlk = m[:RupHydSlk]

        dem = sum(d.demandHour)

        # DOWN reserve
        RdnHyd = m[:RdnHyd]
        RdnTer = m[:RdnTer]
        hyd_dn_res = @expression(m, sum( RdnHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0 ))
        hyd_dn_res_CAG = @expression(m, sum(RdnHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0 && d.hCode[h] in hidrosCAG))
        ter_dn_res =  @expression(m, sum( RdnTer[t,hour] for t=1:length(d.tExisting) ))

        RdnTCstr = @constraint(m, hyd_dn_res + ter_dn_res == 0.05*dem)
        RdnTCstrCAG = @constraint(m, hyd_dn_res_CAG + sum(RdnHydSlk[:,hour]) == 0.05*dem)
        set_name(RdnTCstr, "RdnTCstr($hour)")

        # UP reserve
        RupHyd = m[:RupHyd]
        RupTer = m[:RupTer]
        hyd_up_res = @expression(m, sum( RupHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0))
        hyd_up_res_CAG = @expression(m, sum( RupHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0 && d.hCode[h] in hidrosCAG))
        ter_up_res =  @expression(m, sum( RupTer[t,hour] for t=1:length(d.tExisting) ))

        RupHCstr = @constraint(m, hyd_up_res + ter_up_res == 0.05 * dem )
        RupHCstrCAG = @constraint(m, hyd_up_res_CAG + sum(RupHydSlk[:,hour]) == 0.05 * dem )
        set_name(RupHCstr, "RupHCstr($hour)")

        add_to_expression!(d.fobj.ReserveCosts[1], 3000* sum(RdnHydSlk[:,hour]))
        add_to_expression!(d.fobj.ReserveCosts[1], 3000* sum(RupHydSlk[:,hour]))
    end
end

#
function reserve_AFF_coopt!(m::JuMP.Model, prb::BidDispatchProblem, hour)
    options = prb.options
    d = prb.data
    n = prb.n
    if has_reserve(prb)

        # DOWN reserve
        RdnHyd = m[:RdnHyd]
        RdnTer = m[:RdnTer]
        hyd_dn_res = @expression(m, sum( RdnHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0 ))
        ter_dn_res =  @expression(m, sum( RdnTer[t,hour] for t=1:length(d.tExisting) ))
        reserve_requirement_down = sum(d.quantities["thermal_res_dn"].value[1,1,hour,:]) + sum(d.quantities["hydro_res_dn"].value[1,1,hour,:])

        RdnTCstr = @constraint(m, hyd_dn_res + ter_dn_res == reserve_requirement_down )
        set_name(RdnTCstr, "RdnTCstr($hour)")

        # UP reserve
        RupHyd = m[:RupHyd]
        RupTer = m[:RupTer]
        hyd_up_res = @expression(m, sum( RupHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0))
        ter_up_res =  @expression(m, sum( RupTer[t,hour] for t=1:length(d.tExisting) ))

        reserve_requirement_up = sum(d.quantities["hydro_res_up"].value[1,1,hour,:]) + sum(d.quantities["thermal_res_up"].value[1,1,hour,:])

        RupHCstr = @constraint(m, hyd_up_res + ter_up_res == reserve_requirement_up)
        set_name(RupHCstr, "RupHCstr($hour)")

        # add_to_expression!(d.fobj.ReserveCosts, 3000* sum(RdnHydSlk[:,hour]))
        # add_to_expression!(d.fobj.ReserveCosts, 3000* sum(RupHydSlk[:,hour]))
    end
end

function reserve_AFF_coopt_cat!(m::JuMP.Model, prb::BidDispatchProblem, hour)
    options = prb.options
    d = prb.data
    n = prb.n
    if has_reserve(prb)

        # DOWN reserve
        RdnHyd = m[:RdnHyd]
        RdnTer = m[:RdnTer]
        hyd_dn_res = @expression(m, sum( RdnHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0 ))
        ter_dn_res =  @expression(m, sum( RdnTer[t,hour] for t=1:length(d.tExisting) ))

        # hydro down
        @constraint(m, hyd_dn_res == sum(d.quantities["hydro_res_dn"].value[1,1,hour,:]) )
        # thermal down
        @constraint(m, ter_dn_res == sum(d.quantities["thermal_res_dn"].value[1,1,hour,:]) )

        # UP reserve
        RupHyd = m[:RupHyd]
        RupTer = m[:RupTer]
        hyd_up_res = @expression(m, sum( RupHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0))
        ter_up_res =  @expression(m, sum( RupTer[t,hour] for t=1:length(d.tExisting) ))

        # hydro up
        @constraint(m, hyd_up_res == sum(d.quantities["hydro_res_up"].value[1,1,hour,:]))
        # thermal up
        @constraint(m, ter_up_res == sum(d.quantities["thermal_res_up"].value[1,1,hour,:]))

    end
end

function reserve_AFF!(m::JuMP.Model, prb::BidDispatchProblem, hour)
    options = prb.options
    d = prb.data
    n = prb.n
    if has_reserve(prb)
        # DOWN reserve
        RdnHyd = m[:RdnHyd]
        RdnTer = m[:RdnTer]
        hyd_dn_res = @expression(m, sum( RdnHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0))
        ter_dn_res =  @expression(m, sum( RdnTer[t,hour] for t=1:length(d.tExisting) ))

        RdnTCstr = @constraint(m, hyd_dn_res + ter_dn_res == sum(d.quantities["thermal_res_dn"].value[1,1,hour,:]) + sum(d.quantities["hydro_res_dn"].value[1,1,hour,:]) )
        set_name(RdnTCstr, "RdnTCstr($hour)")

        # UP reserve
        RupHyd = m[:RupHyd]
        RupTer = m[:RupTer]
        hyd_up_res = @expression(m, sum( RupHyd[h,hour] for h=1:n.Hydros if d.hExist[h] <= 0))
        ter_up_res =  @expression(m, sum( RupTer[t,hour] for t=1:length(d.tExisting) ))

        RupHCstr = @constraint(m, hyd_up_res + ter_up_res == sum(d.quantities["hydro_res_up"].value[1,1,hour,:]) + sum(d.quantities["thermal_res_up"].value[1,1,hour,:]) )
        set_name(RupHCstr, "RupHCstr($hour)")

        RdnHydSlk  = m[:RdnHydSlk]
        RupHydSlk  = m[:RupHydSlk]
        RupTerSlk  = m[:RupTerSlk]
        RdnTerSlk  = m[:RdnTerSlk]

        for h in d.reservoirs
            if d.quantities["hydro_res_dn"].value[1,1,hour,h] <= 0.1
                @constraint(m, RdnHyd[h,hour] == 0 )
                @constraint(m, RupHyd[h,hour] == 0 )
            else
                @constraint(m, RdnHyd[h,hour] + RdnHydSlk[h,hour] >= d.quantities["hydro_res_dn"].value[1,1,hour,h] )
                @constraint(m, RupHyd[h,hour] + RupHydSlk[h,hour] >= d.quantities["hydro_res_up"].value[1,1,hour,h] )
            end
        end
        for t in 1:length(d.tExisting)
            if d.quantities["thermal_res_dn"].value[1,1,hour,t] <= 0.1
                @constraint(m, RdnTer[t,hour] == 0)
                @constraint(m, RupTer[t,hour] == 0 )
            else
                @constraint(m, RdnTer[t,hour] + RdnTerSlk[t,hour] >= d.quantities["thermal_res_dn"].value[1,1,hour,t] )
                @constraint(m, RupTer[t,hour] + RupTerSlk[t,hour] >= d.quantities["thermal_res_up"].value[1,1,hour,t] )
            end
        end

        add_to_expression!(d.fobj.ReserveCosts[1], 3000* sum(RdnHydSlk[:,hour]))
        add_to_expression!(d.fobj.ReserveCosts[1], 3000* sum(RupHydSlk[:,hour]))
        add_to_expression!(d.fobj.ReserveCosts[1], 3000* sum(RdnTerSlk[:,hour]))
        add_to_expression!(d.fobj.ReserveCosts[1], 3000* sum(RupTerSlk[:,hour]))
    end
end

function thermal_generation_capacity!(m::JuMP.Model, prb::BidDispatchProblem, hour, s)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Thermals > 0
        # RdnTer = zeros(Float64, length(d.tExisting), prb.n.MaxHours)
        # RupTer = zeros(Float64, length(d.tExisting), prb.n.MaxHours)
        if has_reserve(prb)
            RdnTer = m[:RdnTer]
            RupTer = m[:RupTer]
        end

        if options.commitment
            x = m[:COMT]
        end

        TGen = get_tgen(m, prb)

        for (j, t) in enumerate(d.tExisting)
            if d.is_commit[t] > 0
                if has_reserve(prb)
                    MaximumGenerationThermal=  @constraint(m, TGen[j,hour,s] + RupTer[j,hour]  <= d.tPotInst[t] * x[j,hour])

                    set_name(MaximumGenerationThermal, "MaxGT($t,$(hour),$s)")

                    # Minimum capacity
                    gen_min = max(d.tGerMin[t], 0.0)
                    # gen_min = gen_min == 0.0 ? 0.05 * d.tGerMax[t] : gen_min
                    MinGenTer = @constraint(m, TGen[j,hour,s] - RdnTer[j,hour] >= gen_min * x[j,hour])

                    set_name(MinGenTer, "MinGT($t,$(hour),$s)")
                else
                    MaximumGenerationThermal=  @constraint(m, TGen[j,hour,s]  <= d.tPotInst[t] * x[j,hour])

                    set_name(MaximumGenerationThermal, "MaxGT($t,$(hour),$s)")

                    # Minimum capacity
                    gen_min = max(d.tGerMin[t], 0.0)
                    # gen_min = gen_min == 0.0 ? 0.05 * d.tGerMax[t] : gen_min
                    MinGenTer = @constraint(m, TGen[j,hour,s] >= gen_min * x[j,hour])

                    set_name(MinGenTer, "MinGT($t,$(hour),$s)")
                end
            end
        end
    end
    nothing
end

function hydro_generation_capacity!(m::JuMP.Model, prb::BidDispatchProblem, hour, s)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Hydros > 0
        RHyd = zeros(Float64, n.Hydros, n.MaxHours)
        if has_reserve(prb)
            RdnHyd = m[:RdnHyd]
            RupHyd = m[:RupHyd]
        end
        HTurb = get_hturb(m, prb)

        for j in 1:n.Hydros
            if prb.data.fprodt[j] > 0
                # maximum genenration
                MaximumGenerationHydro =  @constraint(m, prb.data.fprodt[j] * HTurb[j,hour,s] + RupHyd[j,hour] <=
                prb.data.fprodt[j] * d.turb_max[j])
                set_name(MaximumGenerationHydro, "MaxGHyd($j, $(hour), $s)")

                # hydro power capacity
                # MaximumGenerationHydro =  @constraint(m, prb.data.fprodt[j] * HTurb[j,hour,s] + RHyd[j,hour] <=
                # prb.data.fprodt[j] * d.turb_max[j])
                # set_name(MaximumGenerationHydro, "RPotHyd($j, $(hour), $s)")
                # Minimum generation
                gen_min = prb.data.fprodt[j] > 0 ? max(0.0, d.defmin[j] / prb.data.fprodt[j]) : 0.0

                MinimumGenerationHydro =  @constraint(m, prb.data.fprodt[j] * HTurb[j,hour,s] - RdnHyd[j,hour] >= 0.0)
                set_name(MinimumGenerationHydro, "MinGHyd($j, $(hour), $s)")
            else
                set_upper_bound(RupHyd[j,hour], 0.0)
                set_upper_bound(RdnHyd[j,hour], 0.0)
            end
        end
    end
    nothing
end


"""
    Set objective function for hour
"""
function setobjective_exp!(m::JuMP.Model, prb::BidDispatchProblem, hour)

    # f = prb.rtflag
    d = prb.data
    n = prb.n
    options = prb.options
    quants = prb.data.quantities

    # load variables in to scope
    # --------------------------
    if n.Thermals>0
        g_ter = get_tgen(m, prb)
        if options.commitment
            ShutDown = m[:STdn]
        end
        if options.commitment
            StartUp = m[:STup]
        end
        if d.has_resPup
            PRTup=m[:PRTup]
        end
        if d.has_resPdn
            PRTdn=m[:PRTdn]
        end
        if d.has_resSup
            SRTup=m[:SRTup]
        end
        if d.has_resSdn
            SRTdn=m[:SRTdn]
        end
        if d.has_resT
            TertiaryReserveThermal=m[:TertiaryReserveThermal]
        end
    end

    def = m[:def]
    if n.Hydros>0
        def_slack = m[:def_slack]
        if options.runmode == DETERMINISTIC_MODE::RunMode
            g_hid = m[:HTurb]
        else
            g_hid = get_hturb(m, prb)
        end
        fcf = m[:fcf]

    end
    if n.Gnd>0 && options.ExplicitRen
        g_gnd = m[:RGen]
    end

    # regularization
    reg = 1

    # Deficit cost
    for sys in 1:size(def)[1]
        # deviation scenarios
        for scen in 1:n.Scenarios
            add_to_expression!(d.fobj.DeficitCost[scen], d.dcost * def[sys, hour, scen])
        end
        if options.runmode == AFFINE_MODE::RunMode
            # forecast scenario
            add_to_expression!(d.fobj.DeficitCost[n.ScenariosExtended], d.dcost * def[sys, hour, n.ScenariosExtended])
        end
    end

    # Thermal Cost
    # ------------

    if n.Thermals > 0
        # thermal cost
        if options.runmode != TRUEUP_MODE::RunMode
            # sum( if fcs_operating(prb, j) || ev))
            FC = m[:FC]
            for j in 1:n.FuelCons
                for l in 1:d.fcs_nsegments[j], s in 1:n.ScenariosExtended
                    if d.tExist[d.fcs2ter[j]] <= 0
                        add_to_expression!(d.fobj.thermalCosts[s], reg * d.fcsCost[l, j] * FC[l, j, hour, s]  )
                    end
                end
            end
        end
        # startup cost
        # simple startup cost
        if options.commitment #!options.startupcost
            for j in 1:length(d.tExisting)
                add_to_expression!(d.fobj.CommitmentCost, reg * d.tShutDownCost[j] * ShutDown[j, hour])
            end
        elseif options.startupcost && any( ( d.startupHot .+ d.startupWarm .+ d.startupCold) .> 0)
            if d.startup_old
                for j in d.tExisting
                    if d.tExist[j] <= 0
                        add_to_expression!(d.fobj.CommitmentCost, reg *d.startupCold[j] * StartUp[j,hour] )
                    end
                end
            else
                c   = m[:STupCost]
                for j in d.tExisting, s in 1:n.Scenarios
                    if d.tExist[j] <= 0
                        add_to_expression!(d.fobj.CommitmentCost,reg * sum( c[j,hour,s] ))
                    end
                end
            end
        end
        # shutdown cost
        if options.startupcost && any(d.tShutDownCost .> 0)
            for j in 1:length(d.tExisting)
                add_to_expression!(d.fobj.CommitmentCost, reg *sum( d.tShutDownCost[j] * ShutDown[j,hour] ))
            end
        end
    end

    # Hydro Cost
    # ----------
    if n.Hydros > 0
        # outflow slack
        for i in 1:n.Hydros, s in 1:n.Scenarios
            add_to_expression!(d.fobj.hydroCosts[s], reg * def_slack[i,s]  )
        end
    end

    if options.runmode == TRUEUP_MODE::RunMode
        TPTs1= m[:TPTs1]
        TPTs2 = m[:TPTs2]
        TPHs1= m[:TPHs1]
        TPHs2 = m[:TPHs2]
        # REDISPATCH COST
        redispatch_cost = [158,	158, 67.79,	67.79,	67.79]

        for t in 1:length(d.tExisting), s in 1:n.Scenarios
            add_to_expression!(d.fobj.RedispatchCost[s], ( (TPTs1[t,hour,s] + TPTs2[t,hour,s]) * redispatch_cost[d.ter2sys[t]]   )
            )
        end
        for h in 1:n.Hydros, s in 1:n.Scenarios
            add_to_expression!(d.fobj.RedispatchCost[s], ( ( TPHs1[h,hour,s] + TPHs2[h,hour,s] ) * redispatch_cost[d.hyd2sys[h]]  )
            )
        end

        # THERMAL COST
        for t in 1:length(d.tExisting), s in 1:n.Scenarios
            add_to_expression!(d.fobj.thermalCosts[s],
                sum( prb.data.quantities["TUP_ter"].value[prb.options.stage, end , hour, t] * redispatch_cost[d.ter2sys[t]] )
            )
        end

        # HYDRO COST
        for h in 1:n.Hydros, s in 1:n.Scenarios
            add_to_expression!(d.fobj.hydroCosts[s],
                sum( prb.data.quantities["TUP_hyd"].value[prb.options.stage, end , hour, h] * redispatch_cost[d.hyd2sys[h]] )
            )
        end
    end

    nothing
end

"""
    Add objective function to Model
"""
function setobjective!(m::JuMP.Model, prb::BidDispatchProblem)
    fobj = prb.data.fobj

    totalcost = sum(fobj.hydroCosts) + sum(fobj.thermalCosts) + sum(fobj.CommitmentCost) + sum(fobj.RenewableCosts) + sum(fobj.ReserveCosts) + sum(fobj.DeficitCost) + sum(fobj.FutureCost) + sum(fobj.RedispatchCost)

    @objective(m , Min , totalcost)
    nothing
end

function print_fobj(obj)
    @show obj
    @show typeof(obj)
    @show obj.constant
    @show obj.terms
    for i in keys(obj.terms)
        @show i, obj.terms[i]*value(i)
    end
end

