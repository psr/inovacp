function cut_in2_days(original_file, skplines, serie)
    cut_in2_hours(original_file, skplines, 24, serie)
end

function cut_in2_48h(original_file, skplines, serie)
    cut_in2_hours(original_file, skplines, 48, serie)
end

function cut_in2_hours(original_file, skplines, hourssequence, serie = "all")
    input_path, original_filename = splitdir(original_file)
    output_path = joinpath(input_path, "days")
    if !isdir(output_path)
        mkdir(output_path)
    end

    orig_df = readtable(original_file, skipstart=skplines)
    s = size(orig_df)

    nhours = maximum(orig_df[:,3])
    nseries = s[1] / nhours

    if serie != "all" && serie isa Number
        orig_df = orig_df[orig_df.Seq_ .== serie, :]
    end

    sequences = nhours / hourssequence
    for seq in 1:sequences
        init_hour = (seq - 1) * hourssequence + 1
        end_hour = init_hour + hourssequence - 1

        # slice dataframe in the chosen hours
        values = orig_df[(orig_df.Blck .<= end_hour) .& (orig_df.Blck .>= init_hour),:]

        # empty buffer to disk
        output_name = "$(serie)_$(Int(init_hour))-$(Int(end_hour))_" * original_filename
        output_file = joinpath(output_path, output_name)
        CSV.write(output_file, values)
    end
end

mutable struct TimeCut
    RT_initHour::Int
    SDDP_initHour::Int
    serie::Int
    horizon::Int
end
function cut_file(session, original_file, skplines, sections)
    input_path, original_filename = splitdir(original_file)
    output_path = joinpath(input_path, "days")
    if !isdir(output_path)
        mkdir(output_path)
    end

    # orig_df = readtable(original_file, skipstart=skplines, encoding=:latin1)
    orig_df = CSV.read(original_file, skipto=skplines+2,header=skplines+1)
    s = size(orig_df)

    nhours = maximum(orig_df[:,3])
    nseries = s[1] / nhours

    # slice dataframe in the chosen hours
    total_hours = sum(i.horizon for i in sections)
    values = orig_df[1:total_hours,:]
    for sec in sections
        if nseries > 1
            vec_sddp = collect(sec.SDDP_initHour:(sec.SDDP_initHour + sec.horizon-1) #(sec.SDDP_initHour + sec.horizon-1)
            vec_rt = collect(sec.RT_initHour:(sec.RT_initHour + sec.horizon-1)) # sec.RT_initHour + sec.horizon-1
            for i in 1:length(vec_sddp)
                values[vec_rt[i], :] = orig_df[(orig_df.Blck .== vec_sddp[i]) .& (orig_df[!, Symbol("Seq.")] .== sec.serie), :]
            end
        else
            vec_sddp = collect(sec.SDDP_initHour:(sec.SDDP_initHour + sec.horizon-1))
            vec_rt = collect(sec.RT_initHour:(sec.RT_initHour + sec.horizon-1))
            for i in 1:length(vec_sddp)
                values[vec_rt[i], :] = orig_df[(orig_df.Blck .== vec_sddp[i]), :]
            end
        end
    end

    # check if its in GWh
    f = open(original_file)
    l = readline(f)
    if "GWh" in strip.(split(l, ","))
        values[:, 4:end] = 1000 .* values[:, 4:end]
    end
    close(f)

    # empty buffer to disk
    output_path_session = joinpath(output_path, "session $session")
    if !isdir(output_path_session)
        mkdir(output_path_session)
    end
    namesdict = Dict("potter.csv" => "Qtherm.csv", "gerhid.csv" => "Qhydro.csv", "gergnd.csv" => "Qrenew.csv",
                    "oppchg.csv" => "Phydro.csv", "rghsec.csv" => "QresSup.csv", "demxba.csv" => "Demand.csv", "cmgrge.csv" => "PresSup.csv")
    output_name =  namesdict[original_filename]
    output_file = joinpath(output_path_session, output_name)
    header = names(values)
    header[2] = Symbol("Serie")
    header[3] = Symbol("Hour")
    names!(values, header)
    cutted_df = values[:, names(values)[2:end]]
    CSV.write(output_file, cutted_df)
end

function build_RT_sections(initial_hour, total_hours, session_length, serie)
    number_of_sessions = total_hours / session_length
    sections = []
    for session in 1:number_of_sessions
        # builds uncertainty shifts:
        shift_day = (session-1) * total_hours + 2*total_hours # day-ahead and I1 causes 2 shifts
        shift_intraday = (session - 1 ) * session_length

        begins_at = initial_hour + shift_day + shift_intraday
        begins_at_RT = session_length * (session-1) +1
        new_sec = TimeCut(begins_at_RT, begins_at, serie, session_length)
        push!(sections, new_sec)
    end
    return sections
end

function build_session_inputs(serie, total_hours, initial_week, number_of_intraday, original_file, skplines)
    initial_hour = (initial_week - 1) * 168 + 1
    number_of_sessions = number_of_intraday
    session_length = total_hours / number_of_intraday

    # day ahead
    DA_sec = TimeCut(1, initial_hour, serie, total_hours)
    cut_file(" Day-Ahead", original_file, skplines, [DA_sec])
    sessions_dfs = []
    for session in 1:number_of_sessions
        # builds uncertainty shifts:
        shift_day = (session) * total_hours # first intraday shifts due to day-ahead
        shift_intraday = (session - 1 ) * session_length

        begins_at = initial_hour + shift_intraday + shift_day
        ends_at = begins_at + 24 #initial_hour + total_hours + shift_day - 1
        tamanho = (number_of_sessions - session + 1) * session_length
        new_sec = TimeCut(1, begins_at, serie, 24) # tamanho
        cut_file(session, original_file, skplines, [new_sec])
    end

    sections = build_RT_sections(initial_hour, total_hours, session_length, serie)
    # build real time with 
    cut_file("RealTime", original_file, skplines, sections)
end

include("thermal_failure.jl")
function build_thermal_availability(serie, total_hours, initial_week, number_of_intraday, skplines, path)
    initial_hour = (initial_week - 1) * 168 + 1
    number_of_sessions = number_of_intraday
    session_length = Int(total_hours / number_of_intraday)
    output_path = joinpath(path, "days")
    if !isdir(output_path)
        mkdir(output_path)
    end
    # considers TEIF:
    thermal_config = joinpath(path, "ThermalPlants.csv")
    teif = read_teif(total_hours, thermal_config)
    nagents = length(keys(teif))
    status = ones(nagents)
    
    fpath = joinpath(path, "potter.csv")
    f = open(fpath)
    lines = readlines(f)
    agents = strip.(split(strip(lines[4]), ","))[4:end]
    close(f)
    potter = readtable(fpath, skipstart=skplines)

    header = [Symbol(i) for i in strip.(split(strip(lines[4]), ",")[2:end])]
    header[1] = Symbol("Serie")
    header[2] = Symbol("Hour")
    lmat = nagents+2
    realtime = zeros(total_hours, lmat)
    for session in 1:number_of_sessions
        # builds uncertainty shifts:
        shift_day = (session) * total_hours # first intraday shifts due to day-ahead
        shift_intraday = (session - 1 ) * session_length

        begins_at = initial_hour + shift_intraday + shift_day
        ends_at = initial_hour + total_hours + shift_day - 1

        # apply failure
        result = apply_failure(teif, agents)
        # update status with failures
        oldstat = deepcopy(status)
        status .-= result
        # caps negative values
        status = [max(i, 0) for i in status]
        time_of_failure = failure_hour(status, session_length)
        mat = zeros(total_hours, lmat)
        for (i,blk) in enumerate(Int(begins_at):Int(ends_at))
            mat[i,1]=serie
            mat[i,2]=blk
            realtime[i,1]=serie
            realtime[i,2]=blk
            iday = (session - 1 ) * session_length
            for a in 3:(nagents+2)
                # intraday
                # @show potter
                # @show size(potter)
                mat[i, a] = potter[serie, a+1] * status[a-2]

                # real time
                if oldstat[a-2] == 1 && status[a-2] == 0 && time_of_failure[a-2] <= i ||  oldstat[a-2] == 0
                    realtime[iday+i, a] = 0 #potter[serie, a] * status[a-2]
                else
                    realtime[iday+i, a] = potter[serie, a+1]
                end
            end
        end
        new_df = DataFrame(mat)
        names!(new_df, header)

        # empty buffer to disk
        output_path_session = joinpath(output_path, "session $session")
        if !isdir(output_path_session)
            mkdir(output_path_session)
        end
        output_name = "Qtherm.csv"
        output_file = joinpath(output_path_session, output_name)
        CSV.write(output_file, new_df)
    end

    # real time
    output_path_session = joinpath(output_path, "session realtime")
    if !isdir(output_path_session)
        mkdir(output_path_session)
    end
    new_df2 = DataFrame(realtime)
    names!(new_df2, header)
    output_name2 = "Qtherm.csv"
    output_file2 = joinpath(output_path_session, output_name2)
    CSV.write(output_file2, new_df2)
end

function build_outputs(path)
    serie = 92
    skplines = 3
    total_hours = 24
    initial_week = 3
    number_of_intraday = 3
    files = ["rghsec.csv", "oppchg.csv", "gerhid.csv", "gergnd.csv", "demxba.csv", "cmgrge.csv"]
    # files = ["gergnd.csv", "demxba.csv", "cmgrge.csv", "cmgdem.csv"]
    # files = ["demand.csv"]

    # creates QTherm
    build_thermal_availability(serie, total_hours, initial_week, number_of_intraday, skplines, path)

    for f in files 
        @show f
        original_file = joinpath(path, f)
        if isfile(original_file)
            build_session_inputs(serie, total_hours, initial_week, number_of_intraday, original_file, skplines)
        else
            throw("File not found : $(original_file)")
        end
    end
end

using DataFrames, CSV, Distributions
include("thermal_failure.jl")
# path = "D:\\Dropbox (PSR)\\CO_CREG_StressTest\\12. Bases\\Base Setembro 2019\\BD_LP_SEP_19 - DEMHR - REN - SIM 05-23 - SLCE 0 - Commit\\"

path = "D:\\Dropbox (PSR)\\CO_CREG_StressTest\\12. Bases\\Base Setembro 2019\\BD_LP_SEP_19 - DEMHR - REN - SIM 05-23 - SLCE 0 - Commit\\"
build_outputs(path)