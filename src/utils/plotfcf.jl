using Plots
import Libdl
PATH_PSRC = joinpath("D:\\Repositories\\Outros\\bidbaseddispatch\\deps", "psrclassesinterfacejulia")
include(joinpath(PATH_PSRC, "PSRClasses_julia1.jl"))
ret = PSRClasses_init(PATH_PSRC)


function plot_fcf()
    INPUTPATH ="D:\\Repositories\\Outros\\bidbaseddispatch\\test\\EDP\\Chile\\Chile - Lucas\\DMCM light\\"
    fcf_name = "costmexx.psr"
    PATH_FCF = joinpath(INPUTPATH, fcf_name)

    #     Cria handle para funcao de custo futuro
    #     -----------------------------------------
    fcf = C_NULL
    fcf = PSRIOSDDPFutureCost_create(0)

    #     Carrega funcao de custo futuro
    #     ------------------------------
    ret = PSRIOSDDPFutureCost_load( fcf, PATH_FCF)

    totalCuts = PSRIOSDDPFutureCost_numberCuts(fcf)
    maxvol = PSRIOSDDPFutureCost_maxVolume(fcf)
    maxinflow =  PSRIOSDDPFutureCost_maxInflow(fcf)
    maxord = PSRIOSDDPFutureCost_maxOrd(fcf)

    PSRIOSDDPFutureCost_gotoStage(fcf, 1)
    hydro_coefs = zeros(Float64, maxvol, totalCuts)
    RHS = zeros(Float64, totalCuts)
    for cut in 1:totalCuts
        #         Le proxima restricao
        #         --------------------
        PSRIOSDDPFutureCost_getCut(fcf)

        #        Stag	Iter	Ser.	Clus
        ser = PSRIOSDDPFutureCost_getHeaderInformationForSimulation(fcf)
        it =  PSRIOSDDPFutureCost_getHeaderInformationForIteration(fcf)
        clust = PSRIOSDDPFutureCost_getHeaderInformationForCluster(fcf)
        RHS[cut] = PSRIOSDDPFutureCost_getRHS(fcf)

        #         Obtem os coeficientes relacionados aos volumes
        #         ----------------------------------------------
        hydro_code = []
        for idvol = 1:maxvol
            coefsVol = PSRIOSDDPFutureCost_getVolume(fcf, idvol-1)
            hydro_coefs[idvol, cut] = coefsVol
            push!(hydro_code, PSRIOSDDPFutureCost_idVolume(fcf, idvol-1))
        end
    end
    xSet = collect(0:1:2000)

    p = plot()
    for cut in 1:totalCuts
        line = zeros(Float64, length(xSet))
        for (i,x) in enumerate(xSet)
            line[i] = RHS[cut] + hydro_coefs[1, cut] * x
        end
        plot!(xSet, line)
    end
    display(p)
end