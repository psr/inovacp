# using Distributions
# using Random
# Random.seed!(123)

function apply_failure(teif_dict::Dict, name::String)
    return rand(1, teif_dict[strip(name)])
end
function apply_failure(teif_dict::Dict, header::Array)
    failures = zeros(Int64, length(header))
    for (i,a) in enumerate(header)
        if ! (a in keys(teif_dict))
            @show keys(teif_dict)
            @show a 
        end
        failures[i] = rand( teif_dict[a])
    end
    return failures
end

function failure_hour(status::Array, horizon)
    time_of_fail = zeros(Int64, length(status))
    for i in 1:length(status)
        if status[i] <= 0
            a = [i for i in 1:horizon]
            time_of_fail[i] = rand(a)
        end
    end
    return time_of_fail
end

function read_teif(horizon, file_path)
    teif_dict = Dict()

    orig_df = readtable(file_path)

    for i in 1:size(orig_df)[1]
        name = orig_df[i,1]
        teif = orig_df[i, 13]

        ## SUCCESS IS TO FAIL HERE !
        teif_dict[strip(name)] = Bernoulli(teif/100)
    end
    return teif_dict
end