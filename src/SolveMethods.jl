"""
    Solve routine
"""
function solve_problem!(m::JuMP.Model, prb::BidDispatchProblem)
    if prb.options.solver == XPRESS_SOLVER::SolverId
        set_solver_params!(m, prb)
    end

    if prb.options.solvelp
        MIP_LP_routine(m, prb)
    else
        MIP_routine(m, prb)
    end
    nothing
end

"""
    Organize MIP CALL
"""
function MIP_LP_routine(m::JuMP.Model, prb::BidDispatchProblem)
    solveMIP!(m, prb)

    if termination_status( m ) == MOI.OPTIMAL
        print_msg(prb, "Optimal solution found!!")

        finish_execution!(m, prb)

        if prb.options.logprint
            writeLP(m, prb.options.INPUTPATH)
        end
    else
        print_msg(prb, "ERROR: Couldn't solve problem: $(termination_status( m ))")

        writeLP(m, prb.options.INPUTPATH)
    end
    nothing
end

"""
    Organize LP CALL
"""
function MIP_routine(m::JuMP.Model, prb::BidDispatchProblem)
    # chose solver
    optimize!(m)

    if termination_status( m ) == MOI.OPTIMAL
        print_msg(prb, "Optimal solution found!")

        finish_execution!(m, prb)

        if prb.options.logprint
            writeLP(m, prb.options.INPUTPATH)
        end
    else
        print_msg(prb, "ERROR: Couldn't solve problem: $(termination_status( m ))")

        writeLP(m, prb.options.INPUTPATH)
    end
    nothing
end

"""
    Solve problem as MIP
"""
function solveMIP!(m::JuMP.Model, prb::BidDispatchProblem)

    # solve MILP
    optimize!(m)

    if termination_status( m ) == MOI.OPTIMAL
        # solve LP to get duals

        solveLP(prb, m)

        writeLP(m, prb.options.INPUTPATH)
        # write_fobj(prb)
        # print_fobj(obj)
    else
        writeLP(m, prb.options.INPUTPATH)

        throw("ERROR: Couldn't solve the MIP problem: $(termination_status( m ))")
    end

    return nothing
end

"""
    Fix integer variables and solve problem as LP
"""
function solveLP(prb::BidDispatchProblem, m::JuMP.Model)
    options = prb.options
    d = prb.data
    # re-solve as an LP to get duals
    if options.startupcost
        @constraint(m, m[:State] .== value.(m[:State]))
        unset_binary.(m[:State])
    end
    if options.commitment
        y   =  value.(m[:STup])
        w   =  value.(m[:STdn])
        x   =  value.(m[:COMT])
        @constraint(m, m[:STup] .== y)
        @constraint(m, m[:STdn] .== w)
        @constraint(m, m[:COMT] .== x)
        unset_binary.(m[:COMT])
    end

    optimize!(m)
    nothing
end

"""
    Write LP File
"""
function writeLP(m::JuMP.Model, PATH::String)

    name = termination_status( m ) == MOI.OPTIMAL ? "prb.lp" : "inf.lp"

    JuMP.write_to_file(m, name)

    nothing
end

"""
    Close inputs and write solution
"""
function finish_execution!(m::JuMP.Model, prb::BidDispatchProblem)
    # write outputs
    IO_writing_results(prb)
    outputs(m, prb)

    # close inputs
    close_inputs!(prb)

    elapsed = trunc(time() - prb.starttime)
    # print_global_results!(prb, m, elapsed)
    # write_fobj(prb)
    write_fobj_scenario(prb)

    nothing
end