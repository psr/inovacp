function PSRCinit(options::BidDispatchOptions)#; PATH_INFL="", PATH_FORW="", PATH_BACK="")

    PATH_CASE = joinpath(options.INPUTPATH,"")
    PATH_PSRCLASSES = options.PSRCPATH
    PATH_IHM = options.IHMPATH

    PATH_INFL = PATH_CASE
 
    # ----------------------------------
    # Parte 1 - Inicializacao PSRCLASSES
    # ----------------------------------

    # Inclui as definicoes do modulo PSRClasses
    # -----------------------------------------
    include( joinpath(PATH_PSRCLASSES,"PSRClasses.jl") )

    # Inicializa a DLL do PSRClasses
    # ------------------------------
    ret = PSRClasses_init(PATH_PSRCLASSES)

    # Configura Gerenciador de Log
    # ----------------------------
    ilog = PSRManagerLog_getInstance(0)

    # Inicializa idioma portugues para log
    # ------------------------------------
    PSRManagerLog_initPortuguese(ilog)

    # create simple on screen log
    # ilogcons = PSRLogSimpleConsole_create(0)
    # PSRManagerLog_addLog(ilog, ilogcons)

    # create file log
    ilogcons = PSRLogTextFile_create(0, joinpath(PATH_CASE, "psrclasses_model.log"))
    PSRManagerLog_addLog(ilog, ilogcons)

    PSRLog_setUsingUnitInformation(ilogcons, true)

    # Configura Gerenciador de Mascaras e carrega Mascaras
    # ----------------------------------------------------
    igmsk  = PSRManagerIOMask_getInstance(0)
    checkFile(joinpath(PATH_PSRCLASSES,"Masks_SDDP_V10.2.pmk"))
    iret = PSRManagerIOMask_importFile(igmsk, joinpath(PATH_PSRCLASSES,"Masks_SDDP_V10.2.pmk"))

    checkFile(joinpath(PATH_PSRCLASSES,"Masks_SDDP_V10.3.pmk"))
    iret = PSRManagerIOMask_importFile(igmsk, joinpath(PATH_PSRCLASSES,"Masks_SDDP_V10.3.pmk"))

    checkFile(joinpath(PATH_PSRCLASSES,"Masks_SDDP_Blocks.pmk"))
    iret = PSRManagerIOMask_importFile(igmsk, joinpath(PATH_PSRCLASSES,"Masks_SDDP_Blocks.pmk"))

    checkFile(joinpath(PATH_IHM,"Affine.pmk"))
    iret = PSRManagerIOMask_importFile(igmsk, joinpath(PATH_IHM,"Affine.pmk"))

    # Configura Gerenciador de Modelos e carrega Modelos
    # --------------------------------------------------
    igmdl = PSRManagerModels_getInstance(0)

    checkFile(joinpath(PATH_PSRCLASSES,"Models_SDDP_V10.2.pmd"))
    iret = PSRManagerModels_importFile(igmdl, joinpath(PATH_PSRCLASSES,"Models_SDDP_V10.2.pmd"))

    checkFile(joinpath(PATH_PSRCLASSES,"Models_SDDP_V10.3.pmd"))
    iret = PSRManagerModels_importFile(igmdl, joinpath(PATH_PSRCLASSES,"Models_SDDP_V10.3.pmd"))

    checkFile(joinpath(PATH_PSRCLASSES,"Models_SDDP_PSRCore.pmd"))
    iret = PSRManagerModels_importFile(igmdl, joinpath(PATH_PSRCLASSES,"Models_SDDP_PSRCore.pmd"))

    checkFile(joinpath(PATH_PSRCLASSES,"Models_SDDP_Keywords.pmd"))
    iret = PSRManagerModels_importFile(igmdl, joinpath(PATH_PSRCLASSES,"Models_SDDP_Keywords.pmd"))

    checkFile(joinpath(PATH_IHM,"Affine.pmd"))
    iret = PSRManagerModels_importFile(igmdl, joinpath(PATH_IHM,"Affine.pmd"))


    # Cria um controlador de tempo
    # ----------------------------------------------------------

    # -------------------------------------
    # Parte 2 - Carrega estudo da base SDDP
    # -------------------------------------
    ictrlgnd = C_NULL

    PATH_DAT = joinpath(PATH_CASE,"sddp.dat")
    PATH_BIN = joinpath(PATH_CASE,"PSRClasses.bin")

    checkFile(PATH_DAT)

    # Cria Objeto Leitor de Estudo SDDP e carrega dados
    # ----------------------------------------------------
    # DAT file
    if isfile(PATH_DAT)

        # Cria Estudo
        # -----------
        istdy = PSRStudy_create(0)

        iosddp = PSRIOSDDP_create(0)

        if options.network == 1
            PSRIOSDDP_useNetwork( iosddp, true )
        else
            PSRIOSDDP_useNetwork( iosddp, false )
        end
        PSRIOSDDP_useIndexedHydroParameters(iosddp, true)
        PSRIOSDDP_useOnlySelectedSystems( iosddp, true )

        iret = PSRIOSDDP_load(iosddp, istdy, PATH_CASE, PATH_INFL, PSR_SDDP_VERSION_14)

        iupdater = PSRUpdaterSDDP_create(0)
        PSRUpdaterSDDP_applyExpansionDecisions(iupdater, istdy)
        PSRUpdaterSDDP_applyRulesForDefaultBlocks(iupdater, istdy)

        PSRUpdaterSDDP_toFuelConsumptionRepresentation(iupdater, istdy)

        #   Carrega dados do modelo Affine
        #   ------------------------------
        PSRManagerIOMask_loadAutomaticData(igmsk, istdy, "Affine", PATH_CASE)

    elseif isfile(PATH_BIN)

        istdy = PSRStudy_create(0)
        iosddp = PSRIOImage_create(0)
        PSRIOImage_load(iosddp, istdy, PATH_BIN)
        iosddp = C_NULL

        updater = PSRUpdaterSDDP_create(0)
        PSRUpdaterSDDP_applyExpansionDecisions(updater, istdy)
        if existMaintenance(istdy)
            PSRUpdaterSDDP_applyMaintenanceData(updater, istdy)
        end
        iosddp = C_NULL

    else
        throw("Database not found!")
    end

    println("Data path: "*PATH_CASE)

    return istdy, iosddp
end

function psr_relation(lst_from, lst_to, TYPE)

    n_from = PSRCollectionElement_maxElements(lst_from)

    map = zeros(Int32, n_from)

    PSRCollectionElement_mapRelationShip(lst_from, lst_to, map, TYPE, false )

    return map
end

function psr_complex_relation(lst_from, lst_to, TYPE)

    n_from = PSRCollectionElement_maxElements(lst_from)
    n_to = PSRCollectionElement_maxElements(lst_to)

    # elemnts of list TO which are connectes to list FROM
    # for instance:
    # 1) the (sub)set of thermals(TO) that belong to some contraint(FROM)
    # or
    # the (sub)set of constraints(TO) that contain some thermal(FROM)
    @static if VERSION >= v"0.7"
        first   = Array{Int32}(undef, n_from)
        next    = Array{Int32}(undef, n_to*n_from)
        pointer = Array{Int32}(undef, n_to*n_from)
    else
        first   = Array{Int32}(n_from)
        next    = Array{Int32}(n_to*n_from)
        pointer = Array{Int32}(n_to*n_from)
    end
    PSRCollectionElement_mapComplexRelationShip(lst_from, lst_to, first, next, pointer, TYPE, false )

    OUT = [zeros(Int32,0) for i in 1:n_from] # 1) array (of size constraints) of sets of thermals
    for r in 1:n_from
        current_index = first[r]
        while ( current_index > 0 )
            push!(OUT[r], pointer[current_index] )
            current_index = next[current_index]
        end
    end

    return OUT
end

function psr_get_int(ptr, attribute::String, default::Integer = -999)
    temp_ptr = PSRModel_parm2(ptr, attribute)
    if temp_ptr != C_NULL
        if PSRParm_getDataType(temp_ptr) != PSR_PARM_INTEGER
            error("ponteiro do parm int ($attribute) com problema")
        end
        if PSRParm_noParm( temp_ptr )
            return default
        else
            return PSRParm_getInteger(temp_ptr)
        end
    elseif default == -999
        error("ponteiro do parm int ($attribute) com problema")
    else
        return Int32(default)
    end
end

function psr_get_int_vec(ptr, attribute::String)
    ptr = PSRModel_vector2(ptr, attribute)
    if ptr != C_NULL
        len = PSRVector_size(ptr)
        if len > 0
            vec = zeros(Int32, len)
            for i in 1:len
                vec[i] = PSRVector_getInteger(ptr, i-1)
            end
            return vec
        else
            return Int32[]
        end
    else
        return Int32[]
    end
end

function psr_get_real_vec(ptr, attribute::String)
    ptr = PSRModel_vector2(ptr, attribute)
    if ptr != C_NULL
        len = PSRVector_size(ptr)
        if len > 0
            vec = zeros(Float64, len)
            for i in 1:len
                vec[i] = PSRVector_getReal(ptr, i-1)
            end
            return vec
        else
            return Float64[]
        end
    else
        return Float64[]
    end
end

function psr_get_real(ptr, attribute::String, default::Float64 = NaN)

    temp_ptr = PSRModel_parm2(ptr, attribute)

    if temp_ptr != C_NULL
        if PSRParm_getDataType(temp_ptr) != PSR_PARM_REAL
            error("ponteiro do parm real ($attribute) com problema: tipo errado")
        end
        if PSRParm_noParm( temp_ptr )
            return default
        else
            return PSRParm_getReal(temp_ptr)
        end
    elseif isnan(default)
        error("ponteiro do parm real ($attribute) com problema")
    else
        return default
    end
end

function psr_map_parm!(ptr, name::String, pos::Array, NUM::Integer = 0)

    ret = PSRMapData_mapParm(ptr, name, pos, NUM)
    ret == length(pos) || error("psrclasses wrong dim")

    nothing
end

function psr_map_parm(ptr::PSRClassesPtr, name::String, lst::PSRClassesPtr, tp::DataType, NUM::Integer = 0)::Vector{tp}

    n = PSRCollectionElement_maxElements(lst)
    @static if VERSION >= v"0.7"
        out = Array{tp}(undef, n)
    else
        out = Array{tp}(n)
    end
    ret = PSRMapData_mapParm(ptr, name, out, NUM)
    ret == length(out) || error("psrclasses wrong dim")

    return out::Vector{tp}
end

# improve string version
function psr_map_parm!(ptr, name::String, pos::String, NUM::Integer = 0)

    ret = PSRMapData_mapParm(ptr, name, pos, NUM)
    ret*NUM == length(pos) || error("psrclasses wrong dim")

    nothing
end

function psr_map_vector!(ptr, name::String, pos::Array)

    ret = PSRMapData_mapVector(ptr, name, pos, 0)
    ret == length(pos) || error("psrclasses wrong dim")

    nothing
end

function psr_map_vector(ptr::PSRClassesPtr, name::String, lst::PSRClassesPtr, tp::DataType)

    n = PSRCollectionElement_maxElements(lst)
    @static if VERSION >= v"0.7"
        out = Array{tp}(undef, n)
    else
        out = Array{tp}(n)
    end
    ret = PSRMapData_mapVector(ptr, name, out, 0)
    ret == length(out) || error("psrclasses wrong dim")

    return out
end

psr_map_dimvector!(ptr, name::String, pos::Array, dim1::String) = psr_map_dimvector!(ptr, name, pos, dim1, "")
function psr_map_dimvector!(ptr, name::String, pos::Array, dim1::String, dim2::String)

    ret = PSRMapData_mapDimensionedVector(ptr, name, pos, 0, dim1, dim2)
    ret == length(pos) || error("psrclasses wrong dim")

    nothing
end

psr_map_vector(ptr::PSRClassesPtr, name::String, lst::PSRClassesPtr, tp::DataType, dim1::String) = psr_map_vector(ptr, name, lst, tp, dim1, "")
function psr_map_vector(ptr::PSRClassesPtr, name::String, lst::PSRClassesPtr, tp::DataType, dim1::String, dim2::String)

    n = PSRCollectionElement_maxElements(lst)
    out = Array{tp}(undef, n)
    ret = PSRMapData_mapDimensionedVector(ptr, name, out, 0, dim1, dim2)
    ret == length(out) || error("psrclasses wrong dim")

    return out
end

function psr_vector_len(collection, attribute)
    ptr = PSRCollectionElement_element(collection, 0)
    imodel = PSRElement_model(ptr)
    i_gen = PSRModel_vector2(imodel, attribute)
    i_size = PSRVector_size(i_gen)
    return i_size
end

function parm_data(istdy, collection, attribute::String, element::Integer)
    ptr = PSRCollectionElement_element(collection, element-1)
    imodel = PSRElement_model(ptr)
    i_gen = PSRModel_parm2(imodel, attribute)
    data_etapa = PSRParm_getDate(i_gen)
    return PSRStudy_getStageFromDate(istdy, data_etapa)
end

function parm_data(istdy, collection, attribute::String)
    n = PSRCollectionElement_maxElements(collection)
    out = zeros(Int32, n)
    for i in 1:n
         out[i] = parm_data(istdy, collection, attribute, i)
    end
    return out
end

function checkFile(PATH_FILE::String; exception = true)

    if !( isfile(PATH_FILE) )
        if exception
            #errormsg("File $PATH_FILE does not exist")
        end
        return false
        #error("File $PATH_FILE does not exist")
    end

    return true
end

function existMaintenance(study_ptr::PSRClassesPtr)
    # get collection list
    maint_lst = PSRStudy_getCollectionMaintenanceData(study_ptr)

    # check if exist data
    exist_maintenance = loadExistElement(maint_lst, "Data")
    return any(exist_maintenance)
end

function loadExistElement(list::PSRClassesPtr, field::String)

    N = PSRCollectionElement_maxElements(list)
    @static if VERSION >= v"0.7"
        data = Array{Bool}(undef, N)
    else
        data = Array{Bool}(N)
    end

    for i in 1:N
        data[i] = existElement(list,i,field)
    end

    return data
end

function loadExistElement!(data::Vector{Bool}, list::PSRClassesPtr, field::String)

    N = PSRCollectionElement_maxElements(list)

    if length(data) != N
        error("SDDP: wrong dimension of vector")
    end

    for i in 1:N

        data[i] = existElement(list,i,field)
    end

    return nothing
end

function existElement(list::PSRClassesPtr, element::Integer, field::String)
    # ATTENTION: collection must be non-empty
    # ---------------------------------------
    out = false
    #info("inside exist Element")
    ptr1 = PSRCollectionElement_element(list, element-1)
    imodel = PSRElement_model(ptr1)


    vetor = PSRModel_vector2(imodel, field)
    if vetor == C_NULL
        vetor = PSRModel_vector2(imodel, field*"(1)")
        if vetor == C_NULL
            vetor = PSRModel_vector2(imodel, field*"(1,1)")
            if vetor == C_NULL
                #errormsg(:warn_once,"element $field not mapped in PSRClasses, it can be a PSRCore issue")
                return false
            end
        end
    end
    size = PSRVector_size(vetor)

    if size > 0
        out = true
    end

    return out
end

function PSRStudy_getCollectionByString(istudy::PSRClassesPtr, entity::String)
    # "PSRGasNode"
    # "PSRGasPipeline"
    # PSRCollectionString *ptrClassNameFilters = new PSRCollectionString();
    istring = PSRCollectionString_create(istudy)

    # ptrClassNameFilters->addString("PSRMaintenanceData");
    PSRCollectionString_addString(istring, entity)

    # PSRCollectionElement *ptrColElement = getCollectionElements(NULL, ptrClassNameFilters);
    lst = PSRStudy_getCollectionElements(istudy, C_NULL, istring)
	# delete ptrClassNameFilters;

    # ptrColElement->removeRedundant();
    PSRCollectionElement_removeRedundant( lst )

	return lst;
end

function updatevalid(current, newval)
    if current > 0
        return min(current, newval)
    else
        return newval
    end
end
