"""
Affine methods
"""
function var_affine!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    if n.Hydros>0

        @variable(m, lambda_hyd[1:length(d.reservoirs), 1:n.MaxHours, 1:n.Clusters])
        @variable(m, 0.0 <= beta_hyd[i=1:length(d.reservoirs), 1:n.MaxHours, 1:n.Clusters] <= d.turb_max[d.reservoirs[i]])

        # @variable(m, beta_spil[1:length(d.reservoirs), 1:prb.n.MaxHours, 1:n.ScenariosExtended]>=0)
        @variable(m, spillage[h=1:n.Hydros, 1:prb.n.MaxHours, 1:n.ScenariosExtended] >= 0)
        for h=1:n.Hydros, hour=1:(prb.n.MaxHours), s=1:n.ScenariosExtended
            defmax = d.defmax[h] > 0 ? 100*d.defmax[h] : 1e3
            set_upper_bound(spillage[h, hour, s], defmax)
            set_name(spillage[h, hour, s], "Hspil($h,$hour,$s)")
        end

        @variable(m, lambda_ro[1:length(d.runoffriver), 1:n.MaxHours, 1:n.Clusters])
        @variable(m, 0 <= beta_ro[i=1:length(d.runoffriver), 1:n.MaxHours, 1:n.Clusters]<= d.turb_max[d.runoffriver[i]])

        # @variable(m, runoffspill[1:length(d.runoffriver), 1:n.MaxHours, 1:n.ScenariosExtended]>=0)
    end

    if n.Thermals>0 && prb.options.thermalAffine
        @variable(m, lambda_ter[1:length(d.tExisting), 1:n.MaxHours, 1:n.Clusters])
        @variable(m, beta_ter[1:length(d.tExisting), 1:n.MaxHours, 1:n.Clusters])
    end
end

function affine_load_balance!(m::JuMP.Model, prb::BidDispatchProblem, hour::Int32)
    d = prb.data
    n = prb.n
    lambda_hyd=m[:lambda_hyd]
    lambda_ro=m[:lambda_ro]
    if n.Thermals>0
        lambda_ter=m[:lambda_ter]
    end
    if prb.options.network !=0
        for sys in 1:n.Sys, clus in 1:n.Clusters
            has_hydro = any(d.hyd2sys .== sys)
            has_thermal = any(d.ter2sys .== sys)
            if has_hydro || (has_thermal && prb.options.thermalAffine)
                if n.Thermals>0 && prb.options.thermalAffine
                    cstr = @constraint(m,
                        sum(prb.data.fprodt[hyd] * lambda_hyd[i, hour, clus] for (i, hyd) in enumerate(d.reservoirs) if d.hExist[hyd] <= 0 && d.hyd2sys[hyd] == sys) +
                        sum(prb.data.fprodt[hyd] * lambda_ro[i, hour, clus] for (i, hyd) in enumerate(d.runoffriver) if d.hExist[hyd] <= 0 && d.hyd2sys[hyd] == sys) +
                        sum(lambda_ter[i, hour, clus] for (i,t) in enumerate(d.tExisting) if d.ter2sys[t] == sys ) ==
                        1
                    )
                    set_name(cstr, "AffLBal($sys,$hour,$clus)")
                else
                    cstr = @constraint(m,
                        sum(prb.data.fprodt[hyd] * lambda_hyd[i, hour, clus] for (i, hyd) in enumerate(d.reservoirs) if d.hExist[hyd] <= 0 && d.hyd2sys[hyd] == sys) +
                        sum(prb.data.fprodt[hyd] * lambda_ro[i, hour, clus] for (i, hyd) in enumerate(d.runoffriver) if d.hExist[hyd] <= 0 && d.hyd2sys[hyd] == sys) ==
                        1
                    )
                    set_name(cstr, "AffLBal($sys,$hour,$clus)")
                end
            end
        end
    else
        for clus in 1:n.Clusters
            if n.Thermals>0 && prb.options.thermalAffine
                cstr = @constraint(m,
                    sum(prb.data.fprodt[hyd] * lambda_hyd[i, hour, clus] for (i, hyd) in enumerate(d.reservoirs) if d.hExist[hyd] <= 0 ) +
                    sum(prb.data.fprodt[hyd] * lambda_ro[i, hour, clus] for (i, hyd) in enumerate(d.runoffriver) if d.hExist[hyd] <= 0 )
                    +
                    sum(lambda_ter[i, hour, clus] for (i,t) in enumerate(d.tExisting) ) ==
                    1
                )
                set_name(cstr, "AffLBal(1,$hour, $clus)")
            else
                cstr = @constraint(m,
                    sum(prb.data.fprodt[hyd] * lambda_hyd[i, hour, clus] for (i, hyd) in enumerate(d.reservoirs) if d.hExist[hyd] <= 0) +
                    sum(prb.data.fprodt[hyd] * lambda_ro[i, hour, clus] for (i, hyd) in enumerate(d.runoffriver) if d.hExist[hyd] <= 0 ) ==
                    1
                )
                set_name(cstr, "AffLBal(1,$hour, $clus)")
            end
        end
    end
    nothing
end

# Add bounds to affine function
function affine_bounds!(m::JuMP.Model, prb::BidDispatchProblem, hr::Int32, s::Int32)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Hydros>0
        HTurb = get_hturb(m, prb)
        spillage = get_spillage(m, prb)

        # add turb bounds
        TurbLim=@constraint(m, [i=1:n.Hydros], 0 <= HTurb[i,hr,s] <= d.turb_max[i])
        for i in 1:n.Hydros
            defmax = d.defmax[i] > 0 ? 100*d.defmax[i] : 1e3
            Spillage=@constraint(m, 0.0 <= spillage[i,hr,s] <= defmax)
            set_name(TurbLim[i], "TurbLim($i, $(hr), $s)")
            set_name(Spillage, "SpilLim($i, $(hr), $s)")
        end
    end
    if n.Thermals>0
        TGen = get_tgen(m, prb)
        if options.commitment
            x = m[:COMT]
        end

        for (j, t) in enumerate(d.tExisting)
            if d.is_commit[t] > 0
                MaximumGenerationThermal=  @constraint(m, TGen[j,hr,s]  <= d.tPotInst[t] * x[j,hr])

                set_name(MaximumGenerationThermal, "MaxGT($t,$(hr),$s)")

                # Minimum capacity
                gen_min = max(d.tGerMin[t], 0.0)
                # gen_min = gen_min == 0.0 ? 0.05 * d.tGerMax[t] : gen_min
                MinGenTer = @constraint(m, TGen[j,hr,s] >= gen_min * x[j,hr])

                set_name(MinGenTer, "MinGT($t,$(hr),$s)")
            else
                # gen bounds
                TGenLim = @constraint(m, 0.0 <= TGen[j, hr, s] <= d.tPotInst[d.tExisting[t]])
                set_name(TGenLim, "TGen0($t, $(hr), $s)")
            end
        end
    end
end

# Add bounds to affine function
function affine_feasible!(m::JuMP.Model, prb::BidDispatchProblem, hr::Int32)
    n = prb.n
    d = prb.data
    options = prb.options

    smin,smax = get_feasibility_idx(prb, hr)
    for s in smin
        affine_bounds!(m, prb, hr, s)
    end
    for s in smax
        affine_bounds!(m, prb, hr, s)
    end

    if options.ramp && hr < prb.n.MaxHours
        smin, smax = get_ramp_idx( prb, hr)
        for s in smin
            ramp_up!(m, prb, hr, s)
            ramp_down!(m, prb, hr, s)
        end
        for s in smax
            ramp_up!(m, prb, hr, s)
            ramp_down!(m, prb, hr, s)
        end
    end
    nothing
end

function get_feasibility_idx(prb::BidDispatchProblem, hour::Int32)
    n = prb.n
    d = prb.data
    options = prb.options
    smin = zeros(Int32, n.Sys)
    smax = zeros(Int32, n.Sys)

    # find min and max delta
    for sys in 1:n.Sys, clus in 1:n.Clusters
        smin[sys] = findmin(d.delta[hour, sys, 1:n.Scenarios])[2]
        smax[sys] = findmax(d.delta[hour, sys, 1:n.Scenarios])[2]
    end

    if options.network == 0
        imin = minimum(smin)
        imax = maximum(smax)
        for sys in 1:n.Sys
            smin[sys] = imin
            smax[sys] = imax
        end
    end

    return smin, smax
end

function get_ramp_idx(prb::BidDispatchProblem, hour::Int32)
    n = prb.n
    d = prb.data
    options = prb.options
    smin = zeros(Int32, n.Sys)
    smax = zeros(Int32, n.Sys)

    rampup = max.(d.delta[hour+1, :, 1:n.Scenarios] .- d.delta[hour, :, 1:n.Scenarios])
    rampdn = max.(d.delta[hour, :, 1:n.Scenarios] .- d.delta[hour+1, :, 1:n.Scenarios], 0)

    # find min and max delta
    for sys in 1:n.Sys
        smin[sys] = findmax( rampup[sys,:] )[2]
        smax[sys] = findmax( rampdn[sys,:] )[2]
    end

    if options.network == 0
        imin = findmax(smin)[2]
        imax = findmax(smax)[2]
        for sys in 1:n.Sys
            smin[sys] = imin
            smax[sys] = imax
        end
    end

    return smin, smax
end

function regularThermCstr!(m::JuMP.Model, prb::BidDispatchProblem, hr::Int32, s::Int32)

    n = prb.n
    d = prb.data
    options = prb.options

    if n.Thermals > 0 && !options.thermalAffine
        TGen = get_tgen(m, prb)

        if options.network == 0
            cstr = @constraint(m, sum(TGen[j, hr, s] for (j,t) in enumerate(d.tExisting)) == sum(TGen[j, hr, n.ScenariosExtended] for (j,t) in enumerate(d.tExisting) ) )
                set_name(cstr, "TotalTGen($hr, $s)")
        elseif options.network == 1
            for sys in 1:n.Sys
                # fix total thermal generation
                cstr = @constraint(m, sum(TGen[j, hr, s] for (j,t) in enumerate(d.tExisting) if d.ter2sys[t] == sys) == sum(TGen[j, hr, n.ScenariosExtended] for (j,t) in enumerate(d.tExisting) if d.ter2sys[t] == sys) )
                set_name(cstr, "TotalTGen($sys, $hr, $s)")
            end
        end
    end
end

# Fuel consumption
function fuel_consumption!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    MAX_EFF_SEGS = 3

    if n.FuelCons > 0
        TGen = get_tgen(m, prb)
        FC = m[:FC]

        hr = prb.data.hour
        s = prb.data.scen

        if options.fuelConsumption

            FuelConsAFF = @constraint(m, [t = 1:length(d.tExisting)], TGen[t, hr, s] <= sum(FC[ l , j , hr, s] for j in 1:n.FuelCons, l in 1:d.fcs_nsegments[j] if d.tExisting[t] == d.fcs2ter[j]) )

            for t=1:length(d.tExisting)
                set_name(FuelConsAFF[t], "FuelConsAFF($t, $hr, $s)")
            end
        end
    end
end


# Build affine expressions
function build_hturb!(m::JuMP.Model, prb::BidDispatchProblem)
    n=prb.n
    prb.n.MaxHours = n.MaxHours
    d = prb.data

    # FOR Deterministic case
    if prb.options.runmode != AFFINE_MODE::RunMode
        return nothing
    end

    # load variables
    lambda_hyd = m[:lambda_hyd]
    beta_hyd = m[:beta_hyd]
    # runoffturb = m[:runoffturb]
    lambda_ro = m[:lambda_ro]
    beta_ro = m[:beta_ro]
    delta = 0.0
    # build expression
    for (i,h) in enumerate(d.reservoirs)
        if d.hExist[h] <= 0
            for hour=1:n.MaxHours
                for scen=1:n.Scenarios
                    if prb.options.network == 0
                        delta = sum( d.delta[hour, :, scen] )
                    else
                        delta = d.delta[hour, prb.data.hyd2sys[h], scen]
                    end

                    prb.data.HTurb[h, hour, scen] = lambda_hyd[i, hour, d.scen2clus[scen]] * delta + beta_hyd[i, hour, d.scen2clus[scen]]
                end
                prb.data.HTurb[h, hour, n.ScenariosExtended] = sum(beta_hyd[i, hour,:]) / n.Clusters
            end
        end
    end
    for (i,h) in enumerate(d.runoffriver)
        if d.hExist[h] <= 0
            for hour=1:n.MaxHours
                for scen=1:n.Scenarios
                    if prb.options.network == 0
                        delta = sum( d.delta[hour, :, scen] )
                    else
                        delta = d.delta[hour, prb.data.hyd2sys[h], scen]
                    end
                    # prb.data.HTurb[h, hour, scen] = runoffturb[i, hour, scen]

                    prb.data.HTurb[h, hour, scen] = lambda_ro[i, hour, d.scen2clus[scen]] * delta + beta_ro[i, hour, d.scen2clus[scen]]
                end
                prb.data.HTurb[h, hour,  n.ScenariosExtended] = sum(beta_ro[i, hour, :]) / n.Clusters
            end
        end
    end
    nothing
end

function build_spillage!(m::JuMP.Model, prb::BidDispatchProblem)
    n=prb.n
    prb.n.MaxHours = n.MaxHours
    d = prb.data

    # FOR Deterministic case
    if prb.options.runmode != AFFINE_MODE::RunMode
        return nothing
    end

    # load variables
    lambda_hyd = m[:lambda_hyd]
    beta_spil = m[:beta_spil]
    runoffspill = m[:runoffspill]

    # build expression
    for (i,h) in enumerate(d.reservoirs), hour=1:n.MaxHours
        for hour=1:n.MaxHours
            for scen=1:n.Scenarios
                if prb.options.network == 0
                    delta = sum( d.delta[hour, :, scen] )
                else
                    delta = d.delta[hour, prb.data.hyd2sys[h], scen]
                end

                # spillage[h, hour, scen] = lambda_hyd[i, hour] * prb.data.delta[hour, prb.data.hyd2sys[h], scen] + beta_spil[i, hour]
                prb.data.spillage[h, hour, scen] = beta_spil[i, hour, scen]
            end
            # spillage[h, hour, n.ScenariosExtended] = beta_spil[i, hour]
            prb.data.spillage[h, hour, n.ScenariosExtended] = beta_spil[i, hour, n.ScenariosExtended]
        end
    end
    for (i,h) in enumerate(d.runoffriver), hour=1:n.MaxHours, scen=1:n.ScenariosExtended
        prb.data.spillage[h, hour, scen] = runoffspill[i, hour, scen]
    end

    nothing
end

function build_tgen!(m::JuMP.Model, prb::BidDispatchProblem)
    n=prb.n
    d = prb.data

    if prb.options.runmode != AFFINE_MODE::RunMode || !prb.options.thermalAffine
        return nothing
    end

    # load variables
    lambda_ter = m[:lambda_ter]
    beta_ter = m[:beta_ter]
    delta = 0.0

    # build expression
    for (i,t) in enumerate(d.tExisting)
        for hour=1:n.MaxHours
            for scen=1:n.Scenarios
                if prb.options.network == 0
                    delta = sum( d.delta[hour, :, scen] )
                else
                    delta = d.delta[hour, prb.data.ter2sys[t], scen]
                end
                prb.data.TGen[i,hour,scen] = lambda_ter[i, hour, d.scen2clus[scen]] * delta + beta_ter[i, hour, d.scen2clus[scen]]
            end
            prb.data.TGen[i,hour,n.ScenariosExtended] = sum(beta_ter[i, hour,:]) / n.Clusters
        end
    end

    nothing
end

function get_forecast!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options
    stage = options.stage
    hour = Int32(0)

    existingvec = zeros(size(d.gGerHourly))
    for i in 1:n.Gnd
        if prb.data.gExist[i] <= 0
            existingvec[i] = 1
        end
    end

    # RENEWABLE FORECAST
    for hour in collect(Int32, 1:n.MaxHours)
        # PULL CENARIO DATA
        for scen in collect(Int32, 1:n.Scenarios)
            # pull Renewable
            pull_renewable_data!(prb, hour, scen)
            renew_generation = d.gGerHourly .* d.gFatOp .* d.rPotInst .* existingvec

            # store renewable generation
            if scen <= n.Scenarios
                if n.Gnd > 0
                    for i in 1:n.Gnd
                        d.rengen[i, hour, scen] = renew_generation[i]
                    end
                end
            end

            # build forecast
            for i in 1:n.Gnd
                d.forecast[i, hour] += renew_generation[i] / n.Scenarios
            end
        end

        # forecast = average generation
        if n.Gnd > 0 && options.runmode == AFFINE_MODE::RunMode
            d.rengen[:, hour, n.ScenariosExtended] = d.forecast[:, hour]
        end
    end

    # NET LOAD
    for hour::Int32 in 1:n.MaxHours
        # PULL LOAD
        pull_demand_data!(prb, hour)

        for sys in 1:n.Sys
            # demand uncertainty
            demand_scenarios = get_dem_scenarions(d.demandHour[sys], options.coef_variation_dem,  n.Scenarios)

            # Calculate Net Demand
            for scen in 1:n.Scenarios
                ren_in_sys = 0.0

                # get rengen in sys
                for i in 1:n.Gnd
                    if d.gnd2sys[i] == sys && prb.data.gExist[i] <= 0
                        ren_in_sys += d.rengen[i, hour, scen]
                    end
                end

                # split rengen between loads
                dem_in_sys = d.sys2dem[ sys ]
                ndem_in_sys = length( dem_in_sys )
                for j in dem_in_sys
                    d.netdemand[j, hour, scen] = d.demandHour[j] - ren_in_sys / ndem_in_sys + demand_scenarios[scen]

                end
            end
        end

        # forecast
        if options.runmode == AFFINE_MODE::RunMode
            for i in 1:n.Demand
                d.netdemand[i , hour, n.ScenariosExtended] = sum(d.netdemand[i , hour, s] for s in 1:n.Scenarios) / n.Scenarios
            end
        elseif options.runmode == DETERMINISTIC_MODE::RunMode
            for i in 1:n.Demand
                d.netdemand[i , hour, :] .= sum(d.netdemand[i , hour, 1:n.Scenarios]) / n.Scenarios
            end
        end

        for scen::Int32 in 1:n.Scenarios
            # calculate delta
            for sys in 1:n.Sys
                prb.data.delta[hour, sys, scen] = d.netdemand[sys, hour, scen] - d.netdemand[sys , hour, n.ScenariosExtended]
            end
        end
    end

    # INFLOW FORECAST
    d.inflow_forecast = zeros(Float64, length(d.hyd2station))
    for scen in collect(Int32, 1:n.Scenarios)
        pull_inflow_data!(prb, scen)
        d.inflow_forecast .+= d.inflowStage ./ n.Scenarios
    end

    nothing
end
