
#
# Import
# ------
import Libdl

# Const
# -----

using SpecialFunctions
using Cbc
using GLPK
# using CPLEX

# Using
# -----
using JuMP
using LinearAlgebra
using DataFrames
using CSV
using TOML
using Profile

#
# Include
# -------
# core code
include("types.jl")
include("mapping.jl")
include("build_vars.jl")
include("build_problem.jl")
include("SolverParams.jl")
include("SolveMethods.jl")

# RunMode-specific
include("DispatchModel.jl")
include("FCFModel.jl")
include("AffineModel.jl")
include("Robust.jl")
include("DemandUncertainty.jl")

# IO/file
include("inputs.jl")
include("outputs.jl")
include("IO.jl")

# PSRClasses
include("PSRClassesUtils.jl")
include("psrc_mapping.jl")

PATH_PSRC = joinpath(@__DIR__, "..", "deps", "psrclassesinterfacejulia")
include(joinpath(PATH_PSRC, "PSRClasses.jl"))
ret = PSRClasses_init(PATH_PSRC)
