function main(PATH_CASE)
    start = time()

    # load
    prb = load_general(PATH_CASE)
    psrclasses_mapping!(prb)
    prb.starttime = start

    # commitment model
    options = prb.options
    options.ramp = false
    options.startupcost = false
    options.minupdntime = true
    options.maxupdntime = true
    options.commitment = true
    options.thermalAffine = true
    options.fuelConsumption = true
    options.ExplicitRen = false
    options.AffineFeas = true
    options.stage = Int32(1)


    if options.runmode == DETERMINISTIC_MODE::RunMode
        options.robust = false

    elseif options.runmode == TRUEUP_MODE::RunMode
        options.solvelp = false
        options.reserve = NoReserve::ReserveType
    end

    # read inputs
    IO_reading_inputs(prb)
    prb.data.inpts, prb.data.quantities = open_inputs(options, prb.options.INPUTPATH, prb.n.MaxHours)

    IO_executionmode(prb)
    # prb.data.mapping
    # -------
    IO_mapping(prb)
    map_problem(prb)

    d = prb.data
    n = prb.n

    # problem loop
    hour = Int32(0)
    start2 = time()
    total_time_pull = 0.0
    total_time_build = 0.0
    t0 = 0.0
    t1 = 0.0
    t2 = 0.0
    t3 = 0.0
    t4 = 0.0
    t5 = 0.0
    t6 = 0.0
    t7 = 0.0
    t8 = 0.0
    ts = zeros(Float64, n.ScenariosExtended)

    start3 = time()

    # build renewable arrays
    get_forecast!(prb)
    GC.gc()

    if options.robust
        build_tau!(prb)
    end
    if options.runmode == DETERMINISTIC_MODE::RunMode
        prb.n.ScenariosExtended = 1
        prb.n.Scenarios = 1
    end

    # Model
    # -----
    IO_building_model(prb)
    m = build_archetype(prb, prb.data.mapping, prb.options.solver)
    # build affine functions
    if options.runmode == AFFINE_MODE::RunMode
        build_hturb!(m, prb)
        build_tgen!(m, prb)
    end

    start31 = time()
    t0 += (start31 - start3)

    startfor = time()
    for i in collect(Int32, prb.options.initial_hour:(prb.options.horizon_with_additional + prb.options.initial_hour-1))
        hour+= Int32(1)
        prb.data.hour = hour
        print_log(prb, hour)

        start32 = time()

        for s in collect(Int32, 1:n.ScenariosExtended)
            prb.data.scen = s

            start4 = time()
            # pull data
            start2 = time()
            pull_scenarios_data!(prb, Int32(hour), Int32(s))
            
            elapsed = trunc(time() - start2)
            total_time_pull += elapsed
            start5 = time()

            # Variable limits depends on runMode
            if options.runmode == TRUEUP_MODE::RunMode
                # network
                network!(m, prb, hour, s)
                set_bounds!(m, prb, hour, s)
                thermal_generation_capacity!(m, prb, hour, s)
                trueup_bound!(m, prb, hour, s)
                load_balance!(m, prb, hour, s)

            elseif options.runmode == AFFINE_MODE::RunMode
                # network
                network!(m, prb, hour, n.ScenariosExtended)
                gnd_bound!(m, prb, hour, s)

                if !options.AffineFeas || options.AffineClus
                    affine_bounds!(m, prb, hour, s)
                end
                # affine_bounds!(m, prb, hour, s)

                if !options.thermalAffine
                    regularThermCstr!(m, prb, hour, s)
                end

                # if renewable spillage is allowed, then must write load balance
                # load_balance!(m, prb, hour, s)
                if options.SpillRen
                    load_balance!(m, prb, hour, s)
                elseif s == n.ScenariosExtended  # load balance only in reference scenario
                    load_balance!(m, prb, hour, s)
                end

            else
                # network
                network!(m, prb, hour, s)
                set_bounds!(m, prb, hour, s)
                load_balance!(m, prb, hour, s)
            end

            # fuel consumption
            fuel_consumption!(m, prb)

            if options.robust
                robust_cstr!(m, prb, hour, s)
                if s == 1
                    robust_feas!(m, prb, hour)
                end
            end

            # Independent of RunMode
            start6 = time()

            # Time update
            start10 = time()
            print_log(prb, "tempo por serie: $(start10 - start4)")
            ts[s] += (start10 - start4)
        end

        start11 = time()
        if options.runmode == AFFINE_MODE::RunMode
            affine_load_balance!(m, prb, hour)
            if options.AffineFeas
                affine_feasible!(m, prb, hour)
            end
            # bound maximum/minimum generation considering reserve
            if has_reserve(prb)
                hydro_generation_capacity!(m, prb, hour, n.ScenariosExtended)
                thermal_generation_capacity!(m, prb, hour, n.ScenariosExtended)
            elseif options.commitment
                thermal_generation_capacity!(m, prb, hour, n.ScenariosExtended)
            end
        end

        if options.commitment && !(options.runmode == TRUEUP_MODE::RunMode)
            thermal_unit_COMT(m, prb, hour)
        end

        #
        reserve!(m, prb, hour, prb.data.mapping)
        start12 = time()

        # objective function
        setobjective_exp!(m, prb, hour)

        start13 = time()

        # store prices
        store_inputs!(hour, prb.data.quantities)
        print_log(prb, "tempo por hora: $(start13 - start32)")
        t7 += start13 - start12
        t8 += start12 - start11
    end

    # hydro balance
    water_balance!(m, prb)

    elapsed = trunc(time() - startfor)
    print_log(prb, "tempo ate comecar for $(startfor - start)")
    print_log(prb, "$t0")
    print_log(prb, "$t1")
    print_log(prb, "$t2")
    print_log(prb, "$t3")
    print_log(prb, "$t4")
    print_log(prb, "$t7")
    print_log(prb, "$t8")
    print_log(prb, "$ts")

    # fobj
    setobjective!(m, prb)

    # optimize
    IO_solving_problem(prb)
    solve_problem!(m, prb)

    # close inputs
    println("Finalizando...")
    print_log(prb, "Finalizando...")
    close_inputs!(prb)
    
    print_log(prb, "Sucesso.")
    println("Sucesso.")
    close(prb.data.logstream)
end