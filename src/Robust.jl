# build tau
function build_tau!(prb::BidDispatchProblem)
    options = prb.options
    d = prb.data
    n = prb.n
    qts = prb.data.quantities

    d.tau = zeros(Float64, n.Sys, n.MaxHours, 2)
    for h in 1:n.MaxHours, sys in 1:n.Sys, i in 1:2
        if sys == 1
            d.tau[sys, h, i] = qts["UC_ng"].value[h,i]
        else
            d.tau[sys, h, i] = qts["UC_si"].value[h,i]
        end
    end

    nothing
end

function var_robust!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    if options.robust
        if n.Hydros>0
            @variable(m, Rhslk[1:n.Hydros, 1:n.MaxHours] >=0)
            add_to_expression!(d.fobj.DeficitCost, d.dcost * sum(Rhslk) )
        end

        if n.Thermals>0
            @variable(m, Rtslk[1:length(d.tExisting), 1:n.MaxHours] >=0)
            add_to_expression!(d.fobj.DeficitCost, d.dcost * sum(Rtslk) )
        end
    end
end

"""
    Robust
"""
function robust_cstr!(m::JuMP.Model, prb::BidDispatchProblem, hour, scen)
    n = prb.n
    d = prb.data
    options = prb.options
    quantities = prb.data.quantities

    if n.Hydros>0
        # Turbined water (in hm3)
        lambda = m[:lambda_hyd]
        beta = m[:beta_hyd]
        TGen = get_tgen(m,prb)
        IntercTo = m[:IntercTo]
        IntercFrom = m[:IntercFrom]
        lambda_ro = m[:lambda_ro]
        beta_ro = m[:beta_ro]

        qcons = @expression(m, [sys=1:n.Sys], sum(TGen[j, hour, n.ScenariosExtended] for (j,t) in enumerate(d.tExisting) if d.ter2sys[t] == sys) +
            sum(beta[h, hour, 1] - lambda[h,hour,1] * d.forecast[sys, hour] for h in 1:length(d.reservoirs) if d.hyd2sys[h] == sys) +
            sum( beta_ro[h, hour, 1] - lambda_ro[h,hour,1] * d.forecast[sys, hour] for h in 1:length(d.runoffriver) if d.hyd2sys[h] == sys) +
            # network
            # entering node
            + sum( (1-d.iLossTo[l]) * IntercTo[l, hour, 1] for l in 1:n.Intercs if d.int2sys[l] == sys)
            - sum(  IntercFrom[l, hour, 1] for l in 1:n.Intercs if d.int2sys[l] == sys)
            # leaving node
            + sum((1-d.iLossFrom[i]) * IntercFrom[i, hour, 1] for i in 1:n.Intercs if d.intFsys[i] == sys )
            - sum(IntercTo[i, hour, 1] for i in 1:n.Intercs if d.intFsys[i] == sys)
        )

        # cstr
        Robust_cstr = @constraint(m, [h=1:n.Hydros, sys=1:n.Sys], d.tau[sys,hour, 1] * (2) - qcons[sys] <= 0 )

        for h in 1:n.Hydros, sys=1:n.Sys
            set_name(Robust_cstr[h, sys], "RobustCstr($h, $sys, $hour)")
        end
    end

end

function robust_feas!(m::JuMP.Model, prb::BidDispatchProblem, hr)
    n=prb.n
    prb.n.MaxHours = n.MaxHours
    d = prb.data
    qts = prb.data.quantities
    options = prb.options

    Rhslk = m[:Rhslk]
    Rtslk = m[:Rtslk]
    # FEAS
    for rs in 1:2
        netdemand = d.tau[:, hr, rs]
        TGen = build_robust_tgen!(m, prb, hr, netdemand)
        HTurb = build_robust_hturb!(m, prb, hr, netdemand)

        # Bounds
        if n.Hydros>0
            # add turb bounds
            TurbLiml=@constraint(m, [i=1:n.Hydros],  HTurb[i] + Rhslk[i,hr] >= 0.0)
            TurbLimu=@constraint(m, [i=1:n.Hydros],  HTurb[i] - Rhslk[i,hr] <= d.turb_max[i])
            for i in 1:n.Hydros
                set_name(TurbLimu[i], "TurbLimRu($i, $(hr))")
                set_name(TurbLiml[i], "TurbLimRl($i, $(hr))")
            end
        end
        if n.Thermals>0
            if options.commitment
                y= m[:STup]
                x= m[:COMT]
            end

            for (j, t) in enumerate(d.tExisting)
                if d.is_commit[t] > 0
                    MaximumGenerationThermal =  @constraint(m, TGen[j] - Rtslk[j,hr] <= d.tPotInst[t] * x[j,hr])

                    set_name(MaximumGenerationThermal, "MaxGTR($t,$(hr))")

                    # Minimum capacity
                    gen_min = max(d.tGerMin[t], 0.0)
                    # gen_min = gen_min == 0.0 ? 0.05 * d.tGerMax[t] : gen_min
                    MinGenTer = @constraint(m, TGen[j] + Rtslk[j,hr] >= gen_min * x[j,hr])

                    set_name(MinGenTer, "MinGT($t,$(hr))")
                else
                    # gen bounds
                    TGenLim = @constraint(m, TGen[j] - Rtslk[j,hr] <= d.tPotInst[d.tExisting[t]])
                    set_name(TGenLim, "TGenRl($t, $(hr))")
                    TGenLim = @constraint(m, TGen[j] + Rtslk[j,hr] >= 0.0)
                    set_name(TGenLim, "TGenRu($t, $(hr))")
                end
            end

            # RAMP
            if options.ramp && hr < prb.n.MaxHours
                TGenO = build_robust_tgen!(m, prb, hr-1, d.tau[:, hr-1, rs])
                # RAMP UP
                if hour <= max_hour && prb.data.tRampUpCold[t] > 0
                    if hour > 1 && (prb.data.tRampUpCold[t] + prb.data.tRampUpWarm[t]) > 1
                        RampUp = @constraint(m, TGen[j] - TGenO[j] <= prb.data.tRampUpWarm[t] * x[j,hour-1])
                        set_name(RampUp, "RampUpR($j, $(hour))")
                    else
                        # RampUp[j, hour] = @constraint(m, TGen[j, hour, s] - prb.data.initial_power[j] <= (prb.data.tRampUpCold[j])* (1-prb.data.initial_state_commit[j]) + prb.data.tRampUpWarm[j] * prb.data.initial_state_commit[j])
                        # set_name(RampUp[j, hour], "RampUp($j, $(hour), $s)")
                    end
                end

                # RAMP DOWN
                if hour <= max_hour && prb.data.tRampDown[t] > 0
                    if hour > 1 && (prb.data.tRampUpCold[t] + prb.data.tRampUpWarm[t]) > 1

                        RampDown = @constraint(m, TGen[j] - TGenO[j] >= -prb.data.tRampDown[t]*x[j,hour])
                        set_name(RampDown, "RampDownR($j, $(hour))")
                    else
                        # RampDown[j, hour] = @constraint(m, TGen[j, hour, s] - prb.data.initial_power[j] >= -prb.data.tRampDown[j]*x[j,hour, n.ScenariosExtended] - prb.data.tGerMin[j]*w[j,hour, n.ScenariosExtended])
                        # set_name(RampDown[j, hour], "RampDown($j, $(hour), $s)")
                    end
                end
            end
        end
    end


    nothing
end

function build_robust_hturb!(m::JuMP.Model, prb::BidDispatchProblem, hour, netdemand)
    n=prb.n
    prb.n.MaxHours = n.MaxHours
    d = prb.data

    # load variables
    lambda_hyd = m[:lambda_hyd]
    beta_hyd = m[:beta_hyd]
    # runoffturb = m[:runoffturb]
    lambda_ro = m[:lambda_ro]
    beta_ro = m[:beta_ro]

    delta = netdemand .- d.netdemand[: , hour, n.ScenariosExtended]
    if prb.options.network == 0
        delta = sum( delta )
    end
    HTurb = @expression(m, [h=1:n.Hydros], 0.0)
    # build expression
    for (i,h) in enumerate(d.reservoirs)
        if d.hExist[h] <= 0

            HTurb[h] = lambda_hyd[i, hour, 1] * delta[d.hyd2sys[h]] + beta_hyd[i, hour, 1]
        end
    end
    for (i,h) in enumerate(d.runoffriver)
        if d.hExist[h] <= 0
            HTurb[h] = lambda_ro[i, hour, 1] * delta[d.hyd2sys[h]] + beta_ro[i, hour, 1]
        end
    end
    return HTurb
end


function build_robust_tgen!(m::JuMP.Model, prb::BidDispatchProblem, hour, netdemand)
    n=prb.n
    d = prb.data

    # load variables
    lambda_ter = m[:lambda_ter]
    beta_ter = m[:beta_ter]

    delta = netdemand .- d.netdemand[: , hour, n.ScenariosExtended]
    if prb.options.network == 0
        delta = sum( delta )
    end
    TGen = @expression(m, [t=1:length(d.tExisting)], 0.0)

    # build expression
    for (i,t) in enumerate(d.tExisting)
        TGen[i] = lambda_ter[i, hour, 1] * delta[d.ter2sys[t]] + beta_ter[i, hour, 1]
    end

    return TGen
end