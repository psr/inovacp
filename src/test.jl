println("\nInitializing...\n")

PATH_CASE = pwd()*"\\"

# using ProfileView
cd(@__DIR__)
include("biddispatch.jl")
cd(PATH_CASE)
BidBasedDispatch.main(PATH_CASE)