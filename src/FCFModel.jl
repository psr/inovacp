"""
FCF methods
"""
function var_fcf!(m, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Hydros>0
        @variable(m, fcf[s=1:n.ScenariosExtended])
        for s=1:n.ScenariosExtended
            set_name(fcf[s], "alpha($s)")
        end
    end
end

function fcf!(m::JuMP.Model, prb::BidDispatchProblem, s)
    n = prb.n
    d = prb.data
    options = prb.options
    quants = prb.data.quantities

    if n.Hydros>0
        fcf_file!(m, prb, s)
    end
end

function fcf_file!(m::JuMP.Model, prb::BidDispatchProblem, s)
    n = prb.n
    d = prb.data
    options = prb.options
    quants = prb.data.quantities

    volfin = m[:HVolF]
    fcf = m[:fcf]

    totalCuts = size(quants["Fcf"].value)[1]
    RHS_vec = quants["Fcf"].value[:,1]
    min_RHS = minimum(RHS_vec)

    # FCF lower bound
    fcf_lb = @constraint(m, fcf[s] >= - max(0.0, min_RHS) * d.vpl_coef )
    set_name(fcf_lb, "FCFlb($s)")

    RHS = 0.0
    hydro_coefs = zeros(length(d.reservoirs))
    for i in 1:totalCuts
        # read cut parameters
        RHS = quants["Fcf"].value[i,1]
        hydro_coefs = quants["Fcf"].value[i,2:end]

        # FCF cut
        #forall(j in SFCF,k in SCUT|FCFX(k) = j) Future_Cost_Function(j,k):= ALFS(j)*DESR - sum(i in SHID|FIND(i) > 0) FVOL(FIND(i),k) * VOLM(i,S)  >= FRHS(k)

        fcf_cuts = @constraint(m, fcf[s] / d.vpl_coef >= RHS - min_RHS + sum(hydro_coefs[i] * volfin[i, end, s] for (i,h) in enumerate(d.reservoirs) if d.hExist[h] <= 0 ) )

        set_name(fcf_cuts, "FCFcut($i,$s)")
    end

    # add to objective function
    if options.runmode != TRUEUP_MODE::RunMode
        add_to_expression!(d.fobj.FutureCost[s], 1.5*1e3 * fcf[s])
    end
end

function fcf_psrclasses!(m::JuMP.Model, prb::BidDispatchProblem, hour, s)
    n = prb.n
    d = prb.data
    options = prb.options
    psrc = prb.psrc

    # load variables
    volfin = m[:HVolF]
    fcf = m[:fcf]

    # get sizes
    totalCuts = PSRIOSDDPFutureCost_numberCuts(psrc.fcf)
    maxvol = PSRIOSDDPFutureCost_maxVolume(psrc.fcf)
    maxinflow =  PSRIOSDDPFutureCost_maxInflow(psrc.fcf)
    maxord = PSRIOSDDPFutureCost_maxOrd(psrc.fcf)

    # set first stage
    PSRIOSDDPFutureCost_gotoStage(psrc.fcf, options.stage)

    # get id code of hydros
    hydro_codes = Vector{Int32}(undef, maxvol)
    for idvol = 1:maxvol
        hydro_codes[idvol] = PSRIOSDDPFutureCost_idVolume(psrc.fcf, idvol-1)
    end

    # map hydro code to index of reservoirs
    fcf_mapping = map_vector(hydro_codes, prb.data.hCode[d.reservoirs])

    hydro_coefs = zeros(Float64, length(d.reservoirs), totalCuts)
    RHS = zeros(Float64, totalCuts)
    for cut in 1:totalCuts
        #         Le proxima restricao
        #         --------------------
        PSRIOSDDPFutureCost_getCut(psrc.fcf)

        #        Stag	Iter	Ser.	Clus
        ser = PSRIOSDDPFutureCost_getHeaderInformationForSimulation(psrc.fcf)
        it =  PSRIOSDDPFutureCost_getHeaderInformationForIteration(psrc.fcf)
        clust = PSRIOSDDPFutureCost_getHeaderInformationForCluster(psrc.fcf)
        RHS[cut] = PSRIOSDDPFutureCost_getRHS(psrc.fcf)

        #         Obtem os coeficientes relacionados aos volumes
        #         ----------------------------------------------

        for idvol = 1:maxvol
            coefsVol = PSRIOSDDPFutureCost_getVolume(psrc.fcf, idvol-1)
            i_sddp = fcf_mapping[idvol]
            if i_sddp != 0
                hydro_coefs[i_sddp, cut] = coefsVol
            end
        end

        #         Obtem os coeficientes relacionados aos inflows de cada ordem
        #         ------------------------------------------------------------
        # for idinflow = 1:maxinflow
        #     for idord = 1:maxord
        #         coefsInflow = PSRIOSDDPFutureCost_getInflow( psrc.fcf, idinflow-1, idord-1 )
        #     end
        # end
    end

    minRHS = minimum(RHS)

    # FCF lower bound
    # forall(j in SFCF) Future_Cost_Function_Minm_2(j):= ALFS(j) >= - maxlist(0.0,MRHX(j)) / DESR
    fcf_lb = @constraint(m, fcf[hour, s] >= - max(0.0, min_RHS) * d.vpl_coef )
    set_name(fcf_lb, "FCFlb($hour,$s)")

    # FCF cut
    #forall(j in SFCF,k in SCUT|FCFX(k) = j) Future_Cost_Function(j,k):= ALFS(j)*DESR - sum(i in SHID|FIND(i) > 0) FVOL(FIND(i),k) * VOLM(i,S)  >= FRHS(k)
    @constraint(m, FCFcut[cut in 1:totalCuts], fcf[hour, s] / d.vpl_coef >= RHS - min_RHS + sum(hydro_coefs[i, cut] * volfin[i, hour, s] for (i,h) in enumerate(d.reservoirs)) )

    # set_name(fcf_cuts, "FCFcut($i,$hour,$s)")
end

# using Plots
function plot_fcf( prb::BidDispatchProblem)
    INPUTPATH ="D:\\Repositories\\Outros\\bidbaseddispatch\\test\\EDP\\Chile\\FCF Lucas\\1 sistema\\DMCM light - Deterministico\\"
    fcf_name = "costmexx.psr"
    PATH_FCF = joinpath(INPUTPATH, fcf_name)

    #     Cria handle para funcao de custo futuro
    #     -----------------------------------------
    fcf = C_NULL
    fcf = PSRIOSDDPFutureCost_create(0)

    #     Carrega funcao de custo futuro
    #     ------------------------------
    ret = PSRIOSDDPFutureCost_load( fcf, PATH_FCF)

    totalCuts = PSRIOSDDPFutureCost_numberCuts(fcf)
    maxvol = PSRIOSDDPFutureCost_maxVolume(fcf)
    maxinflow =  PSRIOSDDPFutureCost_maxInflow(fcf)
    maxord = PSRIOSDDPFutureCost_maxOrd(fcf)

    PSRIOSDDPFutureCost_gotoStage(fcf, 1)
    hydro_coefs = zeros(Float64, length(d.reservoirs), totalCuts)
    RHS = zeros(Float64, totalCuts)
    for cut in 1:totalCuts
        #         Le proxima restricao
        #         --------------------
        PSRIOSDDPFutureCost_getCut(fcf)

        #        Stag	Iter	Ser.	Clus
        ser = PSRIOSDDPFutureCost_getHeaderInformationForSimulation(fcf)
        it =  PSRIOSDDPFutureCost_getHeaderInformationForIteration(fcf)
        clust = PSRIOSDDPFutureCost_getHeaderInformationForCluster(fcf)
        RHS[cut] = PSRIOSDDPFutureCost_getRHS(fcf)

        #         Obtem os coeficientes relacionados aos volumes
        #         ----------------------------------------------
        hydro_code = []
        for idvol = 1:maxvol
            coefsVol = PSRIOSDDPFutureCost_getVolume(fcf, idvol-1)
            hydro_coefs[idvol, cut] = coefsVol
            push!(hydro_code, PSRIOSDDPFutureCost_idVolume(fcf, idvol-1))
        end
    end
    xSet = collect(0:1:2000)

    p = plot()
    h=2
    for cut in 1:totalCuts
        line = zeros(Float64, length(xSet))
        for (i,x) in enumerate(xSet)
            line[i] = RHS[cut] + hydro_coefs[h, cut] * x
        end
        plot!(xSet, line)
    end
    display(p)
end
function plot_fcf()
    INPUTPATH = "D:\\Repositories\\Outros\\bidbaseddispatch\\test\\EDP\\Chile\\DMCM orig\\"
    fcf_name = "FCF__xxIrrig.csv"
    PATH_FCF = joinpath(INPUTPATH, fcf_name)

    f = open(PATH_FCF)
    header = split(readline(f), ",")
    RHS = Float64[]
    COEF = Float64[]
    h = 2
    @show h
    @show header[2:end][h]
    for line in eachline(f)
        splited = split(line, ",")
        push!(RHS, parse(Float64, splited[2]) )
        push!(COEF, parse(Float64, splited[h+1]) )
    end
    close(f)
    xSet = collect(89:10:1065)

    min_RHS = minimum(RHS)
    p = plot(title = "$(header[2:end][h])", legend = false)
    ylabel!("FCF [\$]")
    xlabel!("Vol [hm3]")
    for cut in 1:length(RHS)
        y = zeros(Float64, length(xSet))
        for (i,x) in enumerate(xSet)
            y[i] = RHS[cut] -min_RHS + COEF[cut] * x
        end
        plot!(xSet, y)
    end
    display(p)
end