#
# Loading PSRClasses
# ------------------
function load_problem(prb::BidDispatchProblem)

    IO_loading_data(prb)

    println("Inflow...")
    inflow_load!(prb)

    println("Constants...")
    constants_load!(prb)

    IO_mapping_data(prb)
    data_load!(prb)


    IO_loading_scenarios(prb)
    scenarios_load!(prb)

    GC.gc()
end

function options_load!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    # io = prb.io

    psrc.config_ptr = PSRStudy_getConfigurationModel(psrc.study_ptr)

    psrc.iexec   = PSRModel_model2(psrc.config_ptr, "ExecutionParameters")  # Config to Hourly simulation
    psrc.ihourly = PSRModel_model2(psrc.config_ptr, "HourlyData"         )  # Config to Hourly simulation
    ichron       = PSRModel_model2(psrc.config_ptr, "ChronologicalData"  )  # Config to Hourly simulation

    # d.tx_discount = psr_get(Float64, psrc.config_ptr, "TaxaDesconto")
    d.tx_discount       = PSRParm_getReal(PSRModel_parm2(psrc.config_ptr, "TaxaDesconto") )
    stages_in_yer       = PSRStudy_getStagesPerYear(psrc.study_ptr)
    n.OriginalScenarios = PSRStudy_getNumberSimulations(psrc.study_ptr)

    # Execution options for affine model
    # ----------------------------------
    # User-defined options (interface)
    prb.data.dcost             = psr_get_real(psrc.config_ptr, "DeficitCost_af", 0.0)
    prb.n.MaxHours             = psr_get_int(psrc.config_ptr , "Horizon_af"         ) # supposes additional hours = 0
    prb.options.runmode        = RunMode(    psr_get_int(psrc.config_ptr, "RunMode_af", 1 )) 
    prb.options.reserve        = ReserveType(psr_get_int(psrc.config_ptr, "Reserve_af", 0 ))
    prb.n.Scenarios            = psr_get_int(psrc.config_ptr , "Scenarios_af"  , n.OriginalScenarios)
    prb.options.SpillRen       = psr_get_int(psrc.config_ptr , "SpillRen_af"   , 1  ) == 0 ? false : true 
    prb.options.hydroBlocks    = psr_get_int(psrc.config_ptr, "HydroBlocks_af" , 1  )
    prb.options.solver         = SolverId(psr_get_int(psrc.config_ptr, "Solver_id_af", 1  ))
   
    # Pre-setted options (not yet on interface)
    prb.options.initial_hour       = psr_get_int(psrc.config_ptr, "InitialHour_af"    , 1)
    prb.options.network            = psr_get_int(psrc.config_ptr, "ConsiderNetwork_af", 1)

    prb.options.repeat_last_hour   = psr_get_int(psrc.config_ptr, "RepeatLastHour_af", 0) == 0 ? false : true 
    prb.options.solvelp            = psr_get_int(psrc.config_ptr, "SolveLp_af"       , 1) == 0 ? false : true 
    prb.options.logprint           = psr_get_int(psrc.config_ptr, "LogPrint_af"      , 0) == 0 ? false : true 
    prb.options.solver_silent      = psr_get_int(psrc.config_ptr, "SolverSilent_af"  , 1) == 0 ? false : true 

    prb.options.solver_time_limit  = psr_get_int(psrc.config_ptr , "SolverTimeLimit_af", 0     )
    prb.options.coef_variation_dem = psr_get_real(psrc.config_ptr, "CoefVariation_af"  , 0.05/3)

    prb.options.additional_hours = psr_get_int(psrc.config_ptr , "AdditionalHours_af", 0     ) 
    prb.options.horizon_with_additional = prb.n.MaxHours + prb.options.additional_hours
    prb.options.kirchoff2ndlaw   = psr_get_int(psrc.config_ptr , "Kirchoff_2_Law_af", 0     )
    prb.n.ScenariosExtended      = prb.n.Scenarios
    if prb.options.runmode == AFFINE_MODE::RunMode
        prb.n.ScenariosExtended = prb.n.Scenarios + 1
    end

    prb.options.robust              = psr_get_int(psrc.config_ptr, "Robust_af"            , 0) == 0 ? false : true 
    prb.options.ExternalRenewables  = psr_get_int(psrc.config_ptr, "ExternalRenewables_af", 1) == 0 ? false : true 
    prb.options.ExternalInflow      = psr_get_int(psrc.config_ptr, "ExternalInflows_af"   , 1) == 0 ? false : true 
    prb.options.AffineClus          = psr_get_int(psrc.config_ptr, "AffineClus_af"        , 0) == 0 ? false : true 

    d.vpl_coef = 1.0

    # convert units
    d.conv = 1e-6*3600 # = 0.0036 [ (Hm3/h) / (m3/s) ] = 1e-6[Hm3/m3] * 3600[s/h]

    nothing
end

function constants_load!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    psrc = prb.psrc

    # time controller
    psrc.timec_ptr = PSRTimeController_create(0)
    # psrc.timec_ptr = PSRScenarioTimeController_create(0)

    PSRTimeController_addElement(psrc.timec_ptr , psrc.study_ptr , true )
    PSRTimeController_configureFrom(psrc.timec_ptr , psrc.study_ptr )

    # Obtem lista de usinas do estudo temporarias
    # ---------------------------------
    psrc.sys_lst = PSRStudy_getCollectionSystems(psrc.study_ptr)
    psrc.ter_lst = PSRStudy_getCollectionPlants(psrc.study_ptr, PSR_PLANTTYPE_THERMAL)
    PSRCollectionElement_query(psrc.ter_lst, "REMOVEDATA WHERE Existing>=2")
    psrc.hyd_lst = PSRStudy_getCollectionPlants(psrc.study_ptr, PSR_PLANTTYPE_HYDRO)
    PSRCollectionElement_query(psrc.hyd_lst, "REMOVEDATA WHERE Existing>=2")
    psrc.gnd_lst = PSRStudy_getCollectionPlants(psrc.study_ptr, PSR_PLANTTYPE_GND)
    psrc.GndStations_lst = PSRStudy_getCollectionByString(psrc.study_ptr, "PSRGndGaugingStation")
    PSRCollectionElement_query(psrc.gnd_lst, "REMOVEDATA WHERE Existing>=2")
    psrc.sts_lst = PSRStudy_getCollectionGaugingStations(psrc.study_ptr)
    psrc.dem_lst = PSRStudy_getCollectionDemands(psrc.study_ptr)
    psrc.dsg_lst = PSRStudy_getCollectionDemandSegments(psrc.study_ptr)
    psrc.fls_lst = PSRStudy_getCollectionFuels(psrc.study_ptr)
    psrc.int_lst = PSRStudy_getCollectionInterconnections(psrc.study_ptr)
    PSRCollectionElement_query(psrc.int_lst, "REMOVEDATA WHERE Existing>=2")
    psrc.fcs_lst = PSRStudy_getCollectionFuelConsumptions(psrc.study_ptr)

    # auxiliary for network
    # ---------------------
    psrc.bus_lst = PSRStudy_getCollectionBuses(psrc.study_ptr)
    psrc.gen_lst = PSRStudy_getCollectionShunts(psrc.study_ptr, PSR_DEVICETYPE_GENERATOR)
    psrc.lod_lst = PSRStudy_getCollectionShunts(psrc.study_ptr, PSR_DEVICETYPE_LOAD)

    # Obtem a lista de soma de restricoes de interconexoes (para poder utilizar os metodos especificos de colecoes)
    # --------------------------------------------------------------------------------------------------
    isc_lst = PSRStudy_listConstraintSumInterconnections(psrc.study_ptr)
    psrc.isc_lst = PSRConstraintSumList_asCollectionElements(isc_lst )

    psrc.dcl_lst = PSRStudy_getCollectionSeries2(psrc.study_ptr, PSR_DEVICETYPE_LINKDC)
    PSRCollectionElement_query(psrc.dcl_lst, "REMOVEDATA WHERE Existing>=2")
    psrc.cir_lst = PSRStudy_getCollectionSeries(psrc.study_ptr)
    psrc.are_lst = PSRStudy_getCollectionAreas(psrc.study_ptr)
    PSRQueryStatement_updateCollection(PSRQueryStatement_create(0), psrc.are_lst, "REMOVEELEMENT WHERE code == 0")

    csc_lst_temp = PSRStudy_listConstraintSumCircuits(psrc.study_ptr)
    psrc.csc_lst = PSRConstraintSumList_asCollectionElements(csc_lst_temp)

    # must be sorted to match psrnetwork
    PSRCollectionElement_sortOn(psrc.cir_lst, "code")
    PSRCollectionElement_sortOn(psrc.bus_lst, "code")

    # Obtem a lista de reserva como uma colecao (para poder utilizar os metodos especificos de colecoes)
    # --------------------------------------------------------------------------------------------------
    psrc.grc_lst = PSRStudy_getCollectionReserveGenerationConstraints(psrc.study_ptr)
    psrc.ggc_lst = PSRStudy_getCollectionGenerationConstraints(psrc.study_ptr)

    # Obtem total de sistemas e usinas associadas
    # -------------------------------------------
    n.Hydros   = PSRCollectionElement_maxElements(psrc.hyd_lst)
    n.Thermals = PSRCollectionElement_maxElements(psrc.ter_lst)
    n.Gnd      = PSRCollectionElement_maxElements(psrc.gnd_lst)
    n.GndStations = PSRCollectionElement_maxElements(psrc.GndStations_lst)
    if options.ExternalRenewables
        n.GndStations = length( prb.data.quantities["RenScen"].header) - 2
    end

    n.Stations = PSRCollectionElement_maxElements(psrc.sts_lst)
    n.Fuels    = PSRCollectionElement_maxElements(psrc.fls_lst)
    n.DemSeg   = PSRCollectionElement_maxElements(psrc.dsg_lst)
    n.Demand   = PSRCollectionElement_maxElements(psrc.dem_lst)
    n.Intercs  = PSRCollectionElement_maxElements(psrc.int_lst)
    n.FuelCons = PSRCollectionElement_maxElements(psrc.fcs_lst)
    n.Buses    = PSRCollectionElement_maxElements(psrc.bus_lst)
    n.Sys    = PSRCollectionElement_maxElements(psrc.sys_lst)
    n.Blocks    = PSRStudy_getNumberBlocks(psrc.study_ptr)

    n.DCLinks    = PSRCollectionElement_maxElements(psrc.dcl_lst)
    n.Circuits   = PSRCollectionElement_maxElements(psrc.cir_lst)
    n.Areas      = PSRCollectionElement_maxElements(psrc.are_lst)
    n.CircSumCtr = PSRCollectionElement_maxElements(psrc.csc_lst)

    # n.Generators = PSRCollectionElement_maxElements(psrc.gen_lst)
    # n.Loads      = PSRCollectionElement_maxElements(psrc.lod_lst)
    # n.Injections = PSRCollectionElement_maxElements(psrc.inj_lst)

    n.GenResCstr    = PSRCollectionElement_maxElements(psrc.grc_lst)
    n.GenGzdCstr    = PSRCollectionElement_maxElements(psrc.ggc_lst)
    n.IntercSumCstr = PSRCollectionElement_maxElements(psrc.isc_lst)
end

"some data is loaded into data and some data is loaded into options"
function scenarios_load!(prb::BidDispatchProblem)

    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options

    if n.Gnd > 0 && psrc.iosddp != C_NULL && !options.ExternalRenewables
        psrc.gnd_ptr = PSRIOSDDP_getGndHourlyScenarios(psrc.iosddp)
    end
    if options.ExternalInflow
        # d.inflow = zeros(Float64, n.Hydros)
        # d.inflowStage = zeros(Float64, n.Hydros)
    else
        d.inflow = zeros(Float64, n.Stations)
        d.inflowStage = zeros(Float64, n.Stations)
        PSRIOSDDPHydroForwardBackward_mapTo(psrc.inflow, d.inflowStage)
    end

    # FCF
    load_fcf!(prb)
end

function load_fcf!(prb::BidDispatchProblem)
    options = prb.options
    fcf_name = "costmexx.psr"
    PATH_FCF = joinpath(options.INPUTPATH, fcf_name)

    #     Cria handle para funcao de custo futuro
    #     -----------------------------------------
    prb.psrc.fcf = C_NULL
    if isfile(PATH_FCF)
        prb.psrc.fcf = PSRIOSDDPFutureCost_create(0)
        #     Carrega funcao de custo futuro
        #     ------------------------------
        ret = PSRIOSDDPFutureCost_load( prb.psrc.fcf, PATH_FCF)
        if ret != PSR_IO_OK
            throw("Error reading fcf")
        end
        PSRIOSDDPFutureCost_gotoStage(prb.psrc.fcf, options.stage)
    end
end

function data_load!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options

    PSRTimeController_resetDate(psrc.timec_ptr)
    PSRTimeController_gotoStage(psrc.timec_ptr, options.stage)

    if n.Hydros > 0
        println("Hydro...")
        hydro_map!(prb)
    end

    # thermal mappings
    if n.Thermals > 0
        println("Thermal...")
        thermal_map!(prb)
        println("Fuel...")
        fuel_map!(prb)
    end

    # renewable mappings
    if n.Gnd > 0
        println("Renewable...")
        renewable_map!(prb)
    end

    # network mappings
    if n.Demand > 0
        println("Load...")
        demands_map!(prb)
    end

    if n.Sys > 0
        println("System...")
        systems_map!(prb)
    end

    if n.Intercs > 0
        println("Interconnection...")
        interconnections_map!(prb)
    end

    # if prb.n.Buses > 0
    #     println("Network...")
    #     network_map!(prb)
    # end

    if n.DCLinks > 0
        # dclink_map!(prb)
    end
    if n.Circuits > 0
        circuit_map!(prb)
    end
    # constraint mappings
    # generationreserve_map!(prb)
    # generationgeneralized_map!(prb)
    # electrical_area_map!(prb)

    # finalize mappers (nor more association to time controller should be done)
    PSRTimeController_createDimensionMappers(psrc.timec_ptr)

    # to get some registry value such as Existing
    PSRTimeController_gotoStage(psrc.timec_ptr, options.stage)
    PSRTimeController_setDimension(psrc.timec_ptr, "block", 1)
    PSRTimeController_setDimension(psrc.timec_ptr, "segment", 1)

    duraci_t = 0.0
    for b::Int32 in 1:n.Blocks
        duraci_b = PSRStudy_getStageDuration(psrc.study_ptr, options.stage, b )
        duraci_t += duraci_b
        if duraci_b < 0
            error("duration of stage $(stage) block $(b) is negative")
        end
    end
    d.duraci_t = duraci_t

    prb.data.delta = zeros(n.MaxHours, n.Sys, n.ScenariosExtended)
    nothing
end

function hydro_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # time controller
    timec_hydro_ptr = PSRTimeController_create(0)
    PSRTimeController_configureFrom(timec_hydro_ptr, psrc.study_ptr)
    PSRTimeController_addCollection(timec_hydro_ptr, psrc.hyd_lst, false)

    # local pointer to map
    hyd_map_stc_local = PSRMapData_create(0)
    PSRMapData_addElements(hyd_map_stc_local, psrc.hyd_lst)

    # dynamic pointer
    psrc.hyd_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(psrc.hyd_map_dyn, psrc.hyd_lst)

    # mappings
    hName = allocstring(n.Hydros, 12)
    PSRMapData_mapParm(hyd_map_stc_local, "name", hName, 12)
    d.hName = splitstring(hName,12)

    d.hCode = psr_map_parm(hyd_map_stc_local, "code", psrc.hyd_lst, Int32)

    d.hIsCommit = zeros(n.Hydros)

    d.hExist = psr_map_vector(psrc.hyd_map_dyn, "Existing", psrc.hyd_lst, Int32)

    d.downstream_turb = psr_relation(psrc.hyd_lst, psrc.hyd_lst, PSR_RELATIONSHIP_TURBINED_TO )
    d.downstream_spill = psr_relation(psrc.hyd_lst, psrc.hyd_lst, PSR_RELATIONSHIP_SPILLED_TO )
    d.upstream_turb = psr_complex_relation(psrc.hyd_lst, psrc.hyd_lst, PSR_RELATIONSHIP_TURBINED_TO)
    d.upstream_spill = psr_complex_relation(psrc.hyd_lst, psrc.hyd_lst, PSR_RELATIONSHIP_SPILLED_TO)

    if options.ExternalInflow
        d.hyd2station = collect(1:n.Hydros)
    else
        d.hyd2station = psr_relation(psrc.hyd_lst, psrc.sts_lst, PSR_RELATIONSHIP_1TO1 )
    end

    d.fprodt = psr_map_vector(psrc.hyd_map_dyn, "FPMed", psrc.hyd_lst, Float64)

    d.defmax =  psr_map_vector(psrc.hyd_map_dyn, "Qmax", psrc.hyd_lst, Float64)
    d.defmin = psr_map_vector(psrc.hyd_map_dyn, "Qmin", psrc.hyd_lst, Float64)

    d.volini = psr_map_parm(hyd_map_stc_local, "Vinic", psrc.hyd_lst, Float64)
    VinicType = psr_map_parm(hyd_map_stc_local, "VinicType", psrc.hyd_lst, Int32)

    d.volmin = psr_map_vector(psrc.hyd_map_dyn, "Vmin", psrc.hyd_lst, Float64)
    d.volmax = psr_map_vector(psrc.hyd_map_dyn, "Vmax", psrc.hyd_lst, Float64)

    d.hPotInst = psr_map_vector(psrc.hyd_map_dyn, "PotInst", psrc.hyd_lst, Float64)

    table_SxH = psrc_table(Float64, psrc.hyd_lst, "SxH_Head", "SxH_Storage")

    d.conv = 1e-6*3600

    d.hyd2sys = psr_relation(psrc.hyd_lst, psrc.sys_lst, PSR_RELATIONSHIP_1TO1 )
    d.hyd2gen = psr_relation(psrc.hyd_lst, psrc.gen_lst, PSR_RELATIONSHIP_1TO1 )
    d.gen2bus = psr_relation(psrc.gen_lst, psrc.bus_lst, PSR_RELATIONSHIP_1TO1 )

    d.hyd2bus = zeros(Int32, length(d.hyd2gen))
    for i in eachindex(d.hyd2gen)
        gen = d.hyd2gen[i]
        if gen < length(d.gen2bus)
            d.hyd2bus[i] = d.gen2bus[gen]
        end
    end

    PSRTimeController_createDimensionMappers(timec_hydro_ptr)
    PSRTimeController_gotoStageHour(timec_hydro_ptr, Int32(1), Int32(1))

    hName = allocstring(n.Hydros, 12)
    PSRMapData_mapParm(hyd_map_stc_local, "name", hName, 12)

    PSRMapData_pullToMemory(psrc.hyd_map_dyn)
    PSRMapData_pullToMemory(hyd_map_stc_local)

    d.hName = splitstring(hName,12)
    d.turb_max = zeros(Float64, n.Hydros)

    d.reservoirs = []
    d.runoffriver = []
    for i in 1:n.Hydros
        d.turb_max[i] = prb.data.fprodt[i] > 0 ? min(d.hPotInst[i]/prb.data.fprodt[i], d.defmax[i]) : d.defmax[i]
        if d.volmin[i] != d.volmax[i]
            push!(d.reservoirs, i)
        else
            push!(d.runoffriver, i)
        end

        # set volini
        volini2storage!(prb, VinicType, table_SxH[i]["y"], table_SxH[i]["x"], i)
    end

    d.inflowStage = zeros(n.Hydros)
    d.inflow = zeros(n.Hydros)
    vals = split.(readlines(joinpath(options.INPUTPATH, "inflow.dat")), ",")
    hdr = strip.(vals[1])
    inf = parse.(Float64, vals[2])

    has_missing = false
    for (ind, name) in enumerate(d.hName)
        position = findfirst(isequal(strip(name)), hdr)
        if position === nothing
            println("Hydro plant $(name) not found in inflow.dat")
            has_missing = true
        else
            d.inflowStage[ind] = inf[position]
        end
    end
    if has_missing
        error("Some hydro plants were not found in the inflow.dat file")
    end

    nothing
end

# Set volini into storage
function volini2storage!(prb::BidDispatchProblem, VinicType::Vector{Int32}, SxH_Storage, SxH_Head, i)
    d = prb.data
    # 0 is storage p.u.
    if VinicType[i] == 1 # 1 is elevation
        VinicType[i] = 0

        # get storage from height
        d.volini[i] = lininterp(SxH_Storage, SxH_Head, d.volini[i])

    else
        if VinicType[i] == 0 # 0 is pu
            d.volini[i] = d.volini[i] * (d.volmax[i] - d.volmin[i]) + d.volmin[i]
        elseif VinicType[i] == 2 # 2 is volume itself

        end
        if d.volini[i] < d.volmin[i]
            d.volini[i] = d.volmin[i]
        end
        if d.volini[i] > d.volmax[i]
            d.volini[i] = d.volmax[i]
        end
    end

    nothing
end

function lininterp(vec_y, vec_x, x)
    #SxH_Storage : y
    #SxH_Head : x

    # initialize
    i_old = 0.0
    for (idx,i) in enumerate(vec_x)
        # check if is inside interval
        if x < i && x > i_old
            if idx > 1
                # linearization: (vec_y[idx] - vec_y[idx-1])/ (i - i_old) * x
                return (vec_y[idx] - vec_y[idx-1])/ (i - i_old) * (x - i_old) + vec_y[idx-1]
            else
                return (vec_y[idx] - 0.0)/ (i - 0.0) * x
            end
        else
            # update
            i_old = i
        end
    end
    return 0.0
end

# Table methods
function psrc_table(::Type{T},
    collection::PSRClassesPtr,
    field_x::String,
    field_y::String) where T
    # @show field_x, field_y
    x = psrc_table_column(T, collection, field_x)
    y = psrc_table_column(T, collection, field_y)
    tables = Dict()

    for i in eachindex(x)
        tables[i] = Dict("x"=>x[i], "y"=>y[i] )
    end
    return tables
end
function psrc_table_column(::Type{T},
    collection::PSRClassesPtr, field::String) where T
    n = PSRCollectionElement_maxElements(collection)::Int32
    ret = [Float64[] for i in 1:n]
    for element in 1:n
        ret[element] = psrc_table_column(T, collection, element, field)
    end
    return ret
end
function psrc_table_column(::Type{Vector{Float64}}, collection::PSRClassesPtr, index::Integer, field::String)
    n = PSRCollectionElement_maxElements(collection)
    # if n <= 0
    #     error("bad number of arguments")
    # end
    element = PSRCollectionElement_element(collection, index-1)
    model = PSRElement_model(element)

    # first_parm = PSRModel_parm3(model, field, 0)
    # if PSRParm_getDataType(first_parm) != PSR_PARM_REAL
    #     error("wrong data type")
    # end
    # @show index
    max_dim = psrc_get_max_dim(Vector{Float64}, collection, index, field)
    ret = zeros(Float64, max_dim)
    for dim in 1:max_dim
        parm = PSRModel_vector3(model, field, dim)
        ret[dim] = PSRVector_getCurrentReal(parm)::Float64
    end
    return ret
end
function psrc_get_max_dim(::Type{Vector{Float64}}, collection::PSRClassesPtr, index::Integer, name::String)
    n = PSRCollectionElement_maxElements(collection)
    # if index > n
    #     error()
    # end
    if n <= 0
        return 0
    end
    ptr = PSRCollectionElement_element(collection, index-1)
    imodel = PSRElement_model(ptr)
    # @show first_parm = PSRModel_parm3(imodel, name, 0)
    first_parm = PSRModel_vector2(imodel, name*"(1)")
    if first_parm == C_NULL
        return 0
    end
    info = PSRVector_getDimensionInformation(first_parm)
    # if PSRParmDimensionInformation_getNumberDimensions(info) != 1
    #     error("wrong number of dimenstions")
    # end
    max_dim = PSRVectorDimensionInformation_getDimensionSize(info, 0)
    return max_dim
end
function psrc_table_column(::Type{Float64}, collection::PSRClassesPtr, index::Integer, field::String)
    n = PSRCollectionElement_maxElements(collection)
    # if n <= 0
    #     error("bad number of arguments")
    # end
    element = PSRCollectionElement_element(collection, index-1)
    model = PSRElement_model(element)

    # first_parm = PSRModel_parm3(model, field, 0)
    # if PSRParm_getDataType(first_parm) != PSR_PARM_REAL
    #     error("wrong data type")
    # end
    # @show index
    max_dim = psrc_get_max_dim(Float64, collection, index, field)
    ret = zeros(Float64, max_dim)
    for dim in 1:max_dim
        parm = PSRModel_parm3(model, field, dim)
        ret[dim] = PSRParm_getReal(parm)::Float64
    end
    return ret
end
function psrc_get_max_dim(::Type{Float64}, collection::PSRClassesPtr, index::Integer, name::String)
    n = PSRCollectionElement_maxElements(collection)
    # if index > n
    #     error()
    # end
    if n <= 0
        return 0
    end
    ptr = PSRCollectionElement_element(collection, index-1)
    imodel = PSRElement_model(ptr)
    # @show first_parm = PSRModel_parm3(imodel, name, 0)
    first_parm = PSRModel_parm2(imodel, name*"(1)")
    if first_parm == C_NULL
        return 0
    end
    info = PSRParm_getDimensionInformation(first_parm)
    # if PSRVectorDimensionInformation_getNumberDimensions(info) != 1
    #     error("wrong number of dimenstions")
    # end
    max_dim = PSRParmDimensionInformation_getDimensionSize(info, 0)
    return max_dim
end

function thermal_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # local pointer to map
    ter_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(ter_map_stc, psrc.ter_lst)

    # dynamic pointer
    psrc.ter_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(psrc.ter_map_dyn, psrc.ter_lst)

    # mappings
    tName = allocstring(n.Thermals, 12)
    PSRMapData_mapParm(ter_map_stc, "name", tName, 12)
    d.tName = splitstring(tName, 12)

    d.tType = psr_map_parm(ter_map_stc, "thermaltype", psrc.ter_lst, Int32)

    # Ramps
    d.tRampUpCold = psr_map_parm(ter_map_stc, "RampUp", psrc.ter_lst, Float64)
    d.tRampUpWarm = d.tRampUpCold

    d.tRampDown = psr_map_parm(ter_map_stc, "RampDown", psrc.ter_lst, Float64)

    # Uptime
    d.tShutDownCost =  psr_map_parm(ter_map_stc, "ShutDownCost", psrc.ter_lst, Float64)
    d.tMinUptime   = psr_map_parm(ter_map_stc, "MinUptime", psrc.ter_lst, Float64)
    d.tMinDowntime = psr_map_parm(ter_map_stc, "MinDowntime", psrc.ter_lst, Float64)
    d.tGerMin = psr_map_vector(psrc.ter_map_dyn, "GerMin", psrc.ter_lst, Float64)
    if options.commit_for_all
        d.is_commit = ones(Int32, n.Thermals)
    else
        d.is_commit = psr_map_vector(ter_map_stc, "ComT", psrc.ter_lst, Int32)
    end

    # premissas chile
    # d.tMinUptime = 2 .* ones(Int, n.Thermals)
    # d.tMinDowntime = 3 .* ones(Int, n.Thermals)
    # d.is_commit = ones(Int32, n.Thermals)

    # Startup
    d.tMaxStartUps = psr_map_parm(ter_map_stc, "MaxStartUps", psrc.ter_lst, Int32)
    d.startupCold = zeros(Float64, n.Thermals)
    d.timeIsCold = zeros(Int, n.Thermals)

    d.tExist = psr_map_vector(psrc.ter_map_dyn, "Existing", psrc.ter_lst, Int32)

    # generation limits

    d.tGerMax = psr_map_vector(psrc.ter_map_dyn, "GerMax", psrc.ter_lst, Float64)
    d.tPotInst =  psr_map_vector(psrc.ter_map_dyn, "PotInst", psrc.ter_lst, Float64)

    # warm/cold definition
    d.startupWarm = zeros(Float64, n.Thermals)
    d.timeIsWarm = zeros(Float64, n.Thermals)
    d.startupHot = zeros(Float64, n.Thermals)

    # costs
    d.startup_old = true

    d.tCVU = psr_map_vector(psrc.ter_map_dyn, "O&MCost", psrc.ter_lst, Float64)

    # -----------------------------------------------------------
    # relations
    # -----------------------------------------------------------

    #thermals to fuels
    d.ter2fuel = psr_relation(psrc.ter_lst, psrc.fls_lst, PSR_RELATIONSHIP_1TO1 )

    # thermal to system
    d.ter2sys = psr_relation(psrc.ter_lst, psrc.sys_lst, PSR_RELATIONSHIP_1TO1 )

    d.ter2gen = psr_relation(psrc.ter_lst, psrc.gen_lst, PSR_RELATIONSHIP_1TO1 )
    gen2bus = psr_relation(psrc.gen_lst, psrc.bus_lst, PSR_RELATIONSHIP_1TO1 )

    d.ter2bus = zeros(Int32, length(d.ter2gen))
    for i in eachindex(d.ter2gen)
        gen = d.ter2gen[i]
        if gen < length(d.gen2bus)
            d.ter2bus[i] = d.gen2bus[gen]
        end
    end

    # strings
    tName = allocstring(n.Thermals, 12)
    PSRMapData_mapParm(ter_map_stc, "name", tName, 12)

    PSRMapData_pullToMemory(psrc.ter_map_dyn)
    PSRMapData_pullToMemory(ter_map_stc)

    for i in 1:n.Thermals
        if d.tMinDowntime[i] > 0 || d.tMinUptime[i] > 0
            d.is_commit[i] = 1
        end
    end

    # fix names
    d.tName = splitstring(tName,12)

    # get index of existing plants
    d.tExisting = Int32[]
    for i in 1:n.Thermals
        if d.tExist[i] <= 0
            push!(d.tExisting, i)
        end
    end
end

function fuel_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # local pointer to map
    fls_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(fls_map_stc , psrc.fls_lst)

    # dynamic pointer
    psrc.fls_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(psrc.fls_map_dyn, psrc.fls_lst)

    fcs_map_dyn_blk_seg = PSRMapData_create(0)
    PSRMapData_addElements(fcs_map_dyn_blk_seg, psrc.fcs_lst)

    fcs_map_dyn_seg = PSRMapData_create(0)
    PSRMapData_addElements(fcs_map_dyn_seg, psrc.fcs_lst)

    fcs_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(fcs_map_dyn, psrc.fcs_lst)

    # mappings
    fName = allocstring(n.Fuels, 12)
    PSRMapData_mapParm(fls_map_stc, "name", fName, 12)
    d.fName = splitstring(fName, 12)

    fcsCEspTemp = psr_map_vector(fcs_map_dyn_blk_seg,  "CEsp", psrc.fcs_lst, Float64, "segment", "block"  )

    fcG = psr_map_vector(fcs_map_dyn_seg,  "G", psrc.fcs_lst, Float64, "segment")

    fCVaria = psr_map_vector(fcs_map_dyn, "O&MCost", psrc.fcs_lst, Float64)

    fCTransp = psr_map_vector(fcs_map_dyn, "CTransp", psrc.fcs_lst, Float64)

    d.fCost = psr_map_vector(psrc.fls_map_dyn, "Custo", psrc.fls_lst, Float64)
    d.fuelconsumptionrate = psr_map_vector(psrc.fls_map_dyn, "ConsumptionMax", psrc.fls_lst, Float64)
    fuelavailability = psr_map_vector(psrc.fls_map_dyn, "Availability", psrc.fls_lst, Float64)

    # fuel consumption to fuel
    d.fcs2fls = psr_relation(psrc.fcs_lst, psrc.fls_lst, PSR_RELATIONSHIP_1TO1 )
    d.fcs2ter = psr_relation(psrc.fcs_lst, psrc.ter_lst, PSR_RELATIONSHIP_1TO1 )
    d.ter2fcs = [Int32[] for i in 1:n.Thermals]

    # Initial Pull
    PSRMapData_pullToMemory(fls_map_stc)
    PSRMapData_pullToMemory(fcs_map_dyn)
    PSRMapData_pullToMemory(fcs_map_dyn_seg)
    PSRMapData_pullToMemory(fcs_map_dyn_blk_seg)
    PSRMapData_pullToMemory(psrc.fls_map_dyn)

    exist_fuelconsumptionrate = loadExistElement(psrc.fls_lst, "ConsumptionMax")
    exist_fuelavailability = loadExistElement(psrc.fls_lst, "Availability")


    # thermal cost
    # ------------
    # Extra time controller for demand segments
    segment_timecontroller = PSRTimeController_create(0)
    PSRTimeController_configureFrom(segment_timecontroller, psrc.study_ptr)
    PSRTimeController_addCollection(segment_timecontroller, psrc.fcs_lst, false)


    d.fcsCost = zeros(Float64, 3, length(d.fcs2fls))
    d.fcsSegment = zeros(Float64, 3, length(d.fcs2fls))

    for seg::Int32 in 1:3

        PSRTimeController_setDimension(segment_timecontroller, "segment", seg)
        PSRMapData_pullToMemory(psrc.fls_map_dyn)
        PSRMapData_pullToMemory(fcs_map_dyn_seg)

        for i in 1:n.FuelCons

            # REM pega apenas 1 bloco

            fcost = d.fCost[ d.fcs2fls[i] ]

            fcs = fcsCEspTemp[i]

            d.fcsCost[seg, i] = fcs * ( fcost ) + fCTransp[i]+ fCVaria[i]

            d.fcsSegment[seg,i] = fcG[i]/100.0
        end
    end

    for j in 1:n.FuelCons
        t = d.fcs2ter[j]
        push!(d.ter2fcs[t], j)
    end

    d.fcs_nsegments = 3 .* ones(Int32, n.FuelCons)
    for seg in 3:-1:2
        for i in 1:n.FuelCons
            if d.fcs_nsegments[i] == seg && d.fcsSegment[seg,i] == 0.0
                d.fcs_nsegments[i] -= 1
            end
        end
    end
end

function renewable_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # local pointer to map
    gnd_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(gnd_map_stc, psrc.gnd_lst)

    gndStation_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(gndStation_map_stc, psrc.GndStations_lst)

    # dynamic pointer
    psrc.gnd_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(psrc.gnd_map_dyn, psrc.gnd_lst)
    psrc.gnd_map_dyn_hor = PSRMapData_create(0)
    PSRMapData_addElements(psrc.gnd_map_dyn_hor, psrc.gnd_lst)

    # name
    gName = allocstring(n.Gnd, 12)
    PSRMapData_mapParm(gnd_map_stc, "name", gName, 12)

    d.gExist = psr_map_vector(psrc.gnd_map_dyn, "Existing", psrc.gnd_lst, Int32)

    d.rPotInst = psr_map_vector(psrc.gnd_map_dyn, "PotInst", psrc.gnd_lst, Float64)
    d.gFatOp = psr_map_vector(psrc.gnd_map_dyn, "FatOpe", psrc.gnd_lst, Float64)

     # //--- Cenarios de geracao horaria ---
    # VETOR REAL        HourGeneration
    if options.ExternalRenewables
        d.gGerHourly = zeros(Float64, n.Gnd)
    else
        d.gGerHourly = psr_map_vector(psrc.gnd_map_dyn_hor, "HourGeneration", psrc.gnd_lst, Float64)
    end

    # d.rStation = strip(splited[4])

    d.gnd2gen = psr_relation(psrc.gnd_lst, psrc.gen_lst, PSR_RELATIONSHIP_1TO1 )
    gen2bus = psr_relation(psrc.gen_lst, psrc.bus_lst, PSR_RELATIONSHIP_1TO1 )

    gnd2gndstation = psr_relation(psrc.gnd_lst, psrc.GndStations_lst, PSR_RELATIONSHIP_NTO1 )

    d.gnd2bus = zeros(Int32, length(d.gnd2gen))

    # mappings
    d.gnd2sys = psr_relation(psrc.gnd_lst, psrc.sys_lst, PSR_RELATIONSHIP_1TO1 )

    # Initial Pull
    PSRMapData_pullToMemory(gnd_map_stc)
    PSRMapData_pullToMemory(gndStation_map_stc)
    PSRMapData_pullToMemory(psrc.gnd_map_dyn)

    d.gName = splitstring(gName,12)

    d.forecast = zeros(Float64, n.Gnd, n.MaxHours)
    d.rengen =  zeros(Float64, n.Gnd, n.MaxHours, n.ScenariosExtended)

    d.ren2stat = zeros(Int32, n.Gnd)

    header = strip.(d.quantities["RenScen"].header)
    has_missing = false
    for i in 1:n.Gnd
        gnd_element = PSRCollectionElement_element(psrc.gnd_lst, i-1)
        gauging_station = PSRGndPlant_getGaugingStation(gnd_element)
        gauging_station_name = PSRGndGaugingStation_name(gauging_station )

        val = findfirst(isequal(gauging_station_name), header)
        if val === nothing
            println("Station $(gauging_station_name) not found in Renewable Scenarios file header")
            has_missing = true
        else
            d.ren2stat[i] = val
        end
    end
    if has_missing
        error("Some renewable stations were not found in the database")
    end

end

function inflow_load!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options
    options = prb.options

    if options.ExternalInflow
        # do nothing - is loaded with hydros
    else

        # inflow data
        iinflow = PSRIOSDDPHydroForwardBackward_create(0)
        PATH_FORW = joinpath(options.INPUTPATH, "forw.psr")
        PATH_BACK = joinpath(options.INPUTPATH, "forw.psr") # ignores back
        ret = PSRIOSDDPHydroForwardBackward_load(iinflow, psrc.study_ptr , PATH_FORW, PATH_BACK)
        if ret != PSR_IO_OK
            throw("Problem loading forward and backward data")
        end

        # hydro
        ihydro = PSRIOSDDPHydroParameters_create(0)
        PSRIOSDDPHydroParameters_generateIndexedHydroParameters2(ihydro, psrc.study_ptr)

        # store pointer
        psrc.inflow = iinflow
    end

end

function demands_map!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options
    options = prb.options

    # allocations
    d.dsg2sys = zeros(Int32, n.DemSeg)
    d.demandHour = zeros(Float64, n.Demand)
    d.demand = zeros(Float64, n.Demand, n.MaxHours)
    d.netdemand = zeros(Float64, n.Demand, n.MaxHours, n.ScenariosExtended)

    # relations

    # demand to sys
    d.lod2bus = psr_relation(psrc.dem_lst, psrc.sys_lst, PSR_RELATIONSHIP_1TO1 )

    #demand segment to demand
    d.dsg2dem = psr_relation(psrc.dsg_lst, psrc.dem_lst, PSR_RELATIONSHIP_1TO1 )
    d.dem2sys = psr_relation(psrc.dem_lst, psrc.sys_lst, PSR_RELATIONSHIP_1TO1 )
    d.dem2bus = psr_relation(psrc.dem_lst, psrc.bus_lst, PSR_RELATIONSHIP_1TO1 )

    for i in eachindex(d.dsg2dem)
        d.dsg2sys[i] = d.dem2bus[d.dsg2dem[i]]
    end

    # Extra time controller for demand segments
    psrc.demand = PSRTimeController_create(0)
    PSRTimeController_configureFrom(psrc.demand, psrc.study_ptr)
    PSRTimeController_addCollection(psrc.demand, psrc.dsg_lst, false)

    # Map SEGMENTS
    psrc.dsg_map_dyn_hor = PSRMapData_create(0)
    PSRMapData_addElements(psrc.dsg_map_dyn_hor, psrc.dsg_lst)

    d.demandHour = psr_map_vector(psrc.dsg_map_dyn_hor, "HourDemand", psrc.dsg_lst, Float64)

    PSRTimeController_createDimensionMappers(psrc.demand)

    d.sys2dem = [Int32[] for i in 1:n.Sys]
    for i in 1:n.Sys
        for j in 1:n.Demand
            if d.dem2sys[j] == i
                push!(d.sys2dem[i], j)
            end
        end
    end

    nothing
end

function systems_map!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options

    # ----------------------------------------------
    # system data
    # ----------------------------------------------
    sys_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(sys_map_stc , psrc.sys_lst)

    # strings
    # -------
    sName = allocstring(n.Sys, 12)
    PSRMapData_mapParm(sys_map_stc, "name", sName, 12)

    # ------------------------------------------------------------------
    # PULL DATA
    # ------------------------------------------------------------------
    PSRMapData_pullToMemory(sys_map_stc)

    # fix names
    # ---------
    d.bName = splitstring(sName,12)

end

function interconnections_map!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options

    # allocations
    iCapacityTo = zeros(Float64, n.Intercs)
    iCapacityFrom = zeros(Float64, n.Intercs)
    d.iCapacityTo = zeros(Float64, n.Intercs)
    d.iCapacityFrom = zeros(Float64, n.Intercs)
    d.iLossFrom = zeros(Float64, n.Intercs)
    d.iLossFrom = zeros(Float64, n.Intercs)
    d.intExist = zeros(Int32, n.Intercs)

    # intercs to and from systems
    d.intFsys = psr_relation(psrc.int_lst, psrc.sys_lst, PSR_RELATIONSHIP_FROM )
    d.int2sys   = psr_relation(psrc.int_lst, psrc.sys_lst, PSR_RELATIONSHIP_TO )

    # Map

    # strings
    # -------
    int_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(int_map_stc, psrc.int_lst)
    int_map_dyn_blk = PSRMapData_create(0)
    PSRMapData_addElements(int_map_dyn_blk, psrc.int_lst)

    # regular
    d.intExist = psr_map_vector(int_map_stc, "Existing", psrc.int_lst, Int32)

    intName = allocstring(n.Intercs, 12)
    PSRMapData_mapParm(int_map_stc, "name", intName, 12)

    # VETOR   REAL      Capacity->     DIM(block) INDEX Data
    iCapacityFrom = psr_map_vector(int_map_dyn_blk, "Capacity->", psrc.int_lst, Float64, "block")
    # VETOR   REAL      Capacity<-     DIM(block) INDEX Data
    iCapacityTo = psr_map_vector(int_map_dyn_blk, "Capacity<-", psrc.int_lst, Float64, "block")

    # VETOR   REAL      LossFactor->              INDEX Data
    d.iLossFrom = psr_map_vector(int_map_stc, "LossFactor->", psrc.int_lst, Float64)
    # # VETOR   REAL      LossFactor<-              INDEX Data
    d.iLossTo = psr_map_vector(int_map_stc, "LossFactor<-", psrc.int_lst, Float64)

    # Initial Pull
    PSRMapData_pullToMemory(int_map_stc)
    PSRMapData_pullToMemory(int_map_dyn_blk)

    PSRTimeController_gotoStage(psrc.timec_ptr, 1)
    tota_hours= 0.0
    for block::Int32 in 1:n.Blocks
        PSRTimeController_setDimension(psrc.timec_ptr, "block", block)
        PSRTimeController_gotoStage(psrc.timec_ptr, Int32(1) )
        PSRMapData_pullToMemory(int_map_dyn_blk)

        block_duration = PSRStudy_getStageDuration(psrc.study_ptr, Int32(1), block )
        tota_hours += block_duration

        for i in 1:n.Intercs
            d.iCapacityTo[i]  += iCapacityTo[i] * block_duration
            d.iCapacityFrom[i] += iCapacityFrom[i] * block_duration
        end
    end

    d.iCapacityTo  = d.iCapacityTo ./ tota_hours
    d.iCapacityFrom = d.iCapacityFrom ./ tota_hours

    d.intName = splitstring(intName, 12)

    nothing
end

function network_map!(prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    psrc = prb.psrc
    options = prb.options

    # allocations
    dCapacityTo = zeros(n.Buses, n.Blocks)
    dCapacityFrom = zeros(n.Buses, n.Blocks)

    # dclink to and from systems
    d.dclFbus = psr_relation(psrc.dcl_lst, psrc.bus_lst, PSR_RELATIONSHIP_FROM )
    d.dcl2bus   = psr_relation(psrc.dcl_lst, psrc.bus_lst, PSR_RELATIONSHIP_TO )

    # Map

    # strings
    # -------
    dcl_map_stc = PSRMapData_create(0)
    PSRMapData_addElements(dcl_map_stc, psrc.dcl_lst)
    dcl_map_dyn = PSRMapData_create(0)
    PSRMapData_addElements(dcl_map_dyn, psrc.dcl_lst)
    dcl_map_dyn_blk = PSRMapData_create(0)
    PSRMapData_addElements(dcl_map_dyn_blk, psrc.dcl_lst)

    # regular
    d.dExist = psr_map_vector(dcl_map_dyn, "Existing", psrc.dcl_lst, Int32)

    # VETOR   REAL      Capacity->     DIM(block) INDEX Data
    d.dCapacityFrom = psr_map_vector(dcl_map_dyn_blk, "Capacity->", psrc.dcl_lst, Float64, "block")
    # VETOR   REAL      Capacity<-     DIM(block) INDEX Data
    d.dCapacityTo = psr_map_vector(dcl_map_dyn_blk, "Capacity<-", psrc.dcl_lst, Float64, "block")

    # VETOR   REAL      LossFactor->              INDEX Data
    d.dLossFrom = psr_map_vector(dcl_map_dyn, "LossFactor->", psrc.dcl_lst, Float64)
    # VETOR   REAL      LossFactor<-              INDEX Data
    d.dLossTo = psr_map_vector(dcl_map_dyn, "LossFactor<-", psrc.dcl_lst, Float64)


    # Initial Pull
    PSRMapData_pullToMemory(dcl_map_stc)
    PSRMapData_pullToMemory(dcl_map_dyn)
    PSRMapData_pullToMemory(dcl_map_dyn_blk)

    nothing
end

function pull_scenarios_data!(prb::BidDispatchProblem, hour::Int32, scen::Int32)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options
    stageDuration = floor(Int, d.duraci_t)

    if scen == 1
        pull_demand_data!(prb, hour)
    end

    if hour == 1
        pull_inflow_data!(prb, scen)
    end
end

function pull_inflow_data!(prb::BidDispatchProblem, scen::Int32)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options
    d.scen =scen

    if options.ExternalInflow
        # do nothing
    else
        PSRIOSDDPHydroForwardBackward_setForward(psrc.inflow, options.stage, scen)
        # d.inflowStage = [m3/s]
    end
    d.inflow = d.inflowStage

    nothing
end

function pull_renewable_data!(prb::BidDispatchProblem, hour::Int32, scen::Int32)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    if n.Gnd == 0
        return nothing
    end

    if !options.ExternalRenewables
        PSRIOElementHourlyScenarios_gotoStageHour(psrc.gnd_ptr, options.stage, hour, scen)
        # Refaz o pull para a memoria dos atributos
        PSRMapData_pullToMemory(psrc.gnd_map_dyn_hor)
    else
        # simula pull de vetor
        if scen <= size(d.quantities["RenScen"].value)[1]
            d.gGerHourly = d.quantities["RenScen"].value[scen,hour,:][d.ren2stat]
        else
            d.gGerHourly = zeros(n.Gnd)
        end
    end

    nothing
end

function pull_demand_data!(prb::BidDispatchProblem, hour::Int32)
    d = prb.data
    n = prb.n
    psrc = prb.psrc
    options = prb.options

    # Seta o estagio
    # general pointers ( demand)
    PSRTimeController_gotoStageHour(psrc.demand, Int32(options.stage), hour)
    PSRMapData_pullToMemory(psrc.dsg_map_dyn_hor)

    # save demand
    d.demand[:, hour] = d.demandHour

    nothing
end

function set_initial_condition!(prb::BidDispatchProblem, inpts, config)
    # loop to set initial scenario
    for i in 1:config.horizon, j in 1:(prb.n.Scenarios-1)
        set_scenario!(inpts, prb.data.quantities)
    end
    prb.n.Scenarios=1
end

function PSRStudy_getCollectionByString(istudy, entity::String)
    # PSRCollectionString *ptrClassNameFilters = new PSRCollectionString();
    istring = PSRCollectionString_create(istudy)

    # ptrClassNameFilters->addString("PSRMaintenanceData");
    PSRCollectionString_addString(istring, entity)

    # PSRCollectionElement *ptrColElement = getCollectionElements(NULL, ptrClassNameFilters);
    lst = PSRStudy_getCollectionElements(istudy, C_NULL, istring)
	# delete ptrClassNameFilters;

    # ptrColElement->removeRedundant();
    PSRCollectionElement_removeRedundant( lst )

	return lst;
end