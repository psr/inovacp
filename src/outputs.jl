"""
    This script does all the output writing
"""


"""
    Defiend Output Types
"""
abstract type Output end
mutable struct OutputPrimal <: Output
    name::String
    path::String
    agents::Vector{String}
    var::String
    var_serie::Bool
    Ptr::IOStream
    get_value::Function
    OutputPrimal(name, path, agents, var) = new(name, path, agents, var, false, initFile(joinpath(path,name), agents), get_value_std)
    OutputPrimal(name, path, agents, var, var_serie) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), get_value_std)
    OutputPrimal(name, path, agents, var, var_serie, get_value_std) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), get_value_std)
end
get_value_std(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String) = value.(m[Symbol(varname)])

mutable struct OutputAffineResUp <: Output
    name::String
    path::String
    agents::Vector{String}
    var::String
    var_serie::Bool
    Ptr::IOStream
    get_value::Function
    OutputAffineResUp(name, path, agents, var) = new(name, path, agents, var, false, initFile(joinpath(path,name), agents), get_value_std)
    OutputAffineResUp(name, path, agents, var, var_serie) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), get_value_std)
    OutputAffineResUp(name, path, agents, var, var_serie, get_value_std) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), get_value_std)
end
mutable struct OutputAffineResDn <: Output
    name::String
    path::String
    agents::Vector{String}
    var::String
    var_serie::Bool
    Ptr::IOStream
    get_value::Function
    OutputAffineResDn(name, path, agents, var) = new(name, path, agents, var, false, initFile(joinpath(path,name), agents), get_value_std)
    OutputAffineResDn(name, path, agents, var, var_serie) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), get_value_std)
    OutputAffineResDn(name, path, agents, var, var_serie, get_value_std) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), get_value_std)
end
mutable struct OutputDual <: Output
    name::String
    path::String
    agents::Vector{String}
    var::String
    var_serie::Bool
    Ptr::IOStream
    get_value::Function
    OutputDual(name, path, agents, var) = new(name, path, agents, var, false, initFile(joinpath(path,name), agents), get_value_std)
    OutputDual(name, path, agents, var, var_serie) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), get_value_std)
    OutputDual(name, path, agents, var, var_serie, get_value_std) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), get_value_std)
end
mutable struct OutputPrice <: Output
    name::String
    path::String
    agents::Vector{String}
    var::String
    var_serie::Bool
    Ptr::IOStream
    get_value::Function
    OutputPrice(name, path, agents, var) = new(name, path, agents, var, false, initFile(joinpath(path,name), agents), get_value_std)
    OutputPrice(name, path, agents, var, var_serie) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), get_value_std)
    OutputPrice(name, path, agents, var, var_serie, get_value_std) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), get_value_std)
end
mutable struct OutputGenericPrice <: Output
    name::String
    path::String
    agents::Vector{String}
    var::String
    var_serie::Bool
    Ptr::IOStream
    pairs::Vector{Any}
    get_value::Function
    OutputGenericPrice(name, path, agents, var, var_serie, pairs) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), pairs, get_value_std)
end
mutable struct OutputIOR <: Output
    name::String
    path::String
    agents::Vector{String}
    var::String
    var_serie::Bool
    Ptr::IOStream
    get_value::Function
    OutputIOR(name, path, agents, var, var_serie) = new(name, path, agents, var, var_serie, initFile(joinpath(path,name), agents), get_value_std)
end

"""
    Auxiliar methods to avoid acessing memory in wrong places ( undeferror)
"""
function fix_inesistent_ref!(vector::Array{Int32})
    for i in 1:length(vector)
        if vector[i] == 0
            vector[i] = 1
        end
    end
end
function reduce_to_existent(vector::Array{Int32})
    e = []
    for i in 1:length(vector)
        if vector[i] > 0
            push!(e, vector[i])
        end
    end
    return e
end

"""
    Define the outputs to be written
"""
function outputs(m, prb::BidDispatchProblem)
    options = prb.options
    d = prb.data
    n = prb.n
    outputs_list = Output[]
    quantities = prb.data.quantities
    max_hours = prb.n.MaxHours

    if options.runmode == TRUEUP_MODE::RunMode

        push!(outputs_list, OutputPrimal("trueup_thermal_generation", prb.options.INPUTPATH, d.tName[d.tExisting], "TGen", true, get_value_TGen))
        push!(outputs_list, OutputPrimal("trueup_hydro_generation", prb.options.INPUTPATH, d.hName, "HGen", true,get_value_HGen))
        push!(outputs_list, OutputPrimal("trueup_volfin", prb.options.INPUTPATH, d.hName[d.reservoirs], "HVolF", true))
        push!(outputs_list, OutputPrimal("trueup_spil", prb.options.INPUTPATH, d.hName, "spillage", true, get_value_spillage))

    else

        if options.commitment
            push!(outputs_list, OutputPrimal("thermal_commitment_status", prb.options.INPUTPATH, d.tName[d.tExisting], "COMT", false))
            # push!(outputs_list, OutputPrimal("thermal_startup", prb.options.INPUTPATH, d.tName[d.tExisting], "STup", false))
            # push!(outputs_list, OutputPrimal("thermal_shutdown", prb.options.INPUTPATH, d.tName[d.tExisting], "STdn", false))

            # startupcost
            if options.startupcost
                push!(outputs_list, OutputPrimal("startup_cost", prb.options.INPUTPATH, d.tName[d.tExisting], "StartUpCost", false))
            end
        end

        push!(outputs_list, OutputPrimal("thermal_generation", prb.options.INPUTPATH, d.tName[d.tExisting], "TGen", true, get_value_TGen))

        # hydro outptus
        push!(outputs_list, OutputPrimal("hydro_spill", prb.options.INPUTPATH, d.hName, "spillage", true, get_value_spillage))
        push!(outputs_list, OutputPrimal("hydro_turbined", prb.options.INPUTPATH, d.hName, "HTurb", true, get_value_HTurb))
        push!(outputs_list, OutputPrimal("hydro_generation", prb.options.INPUTPATH, d.hName, "HGen", true, get_value_HGen))
        push!(outputs_list, OutputPrimal("hydro_volfin", prb.options.INPUTPATH, d.hName, "HVolF", true))

        ren_agents = d.gName
        if !options.ExplicitRen
            ren_agents = d.bName
        end
        push!(outputs_list, OutputPrimal("renewable_spill", prb.options.INPUTPATH, ren_agents, "Rspill", true, get_value_Rspill))

        # renewable outputs
        push!(outputs_list, OutputPrimal("renewable_generation", prb.options.INPUTPATH, ren_agents, "RGen", true, get_value_RGen))


    end


    # Reserve
    if options.runmode == AFFINE_MODE::RunMode
        push!(outputs_list, OutputAffineResUp("thermal_res_up", prb.options.INPUTPATH, d.tName[d.tExisting], "TGen", false, get_value_resup))

        push!(outputs_list, OutputAffineResUp("hydro_res_up", prb.options.INPUTPATH, d.hName, "HGen", false, get_value_resup))

        push!(outputs_list, OutputAffineResDn("thermal_res_dn", prb.options.INPUTPATH, d.tName[d.tExisting], "TGen", false, get_value_resdn))

        push!(outputs_list, OutputAffineResDn("hydro_res_dn", prb.options.INPUTPATH, d.hName, "HGen", false, get_value_resdn))

    elseif has_reserve(prb)
        push!(outputs_list, OutputPrimal("thermal_res_up", prb.options.INPUTPATH, d.tName[d.tExisting], "RupTer", false))

        push!(outputs_list, OutputPrimal("hydro_res_up", prb.options.INPUTPATH, d.hName, "RupHyd", false))

        push!(outputs_list, OutputPrimal("thermal_res_dn", prb.options.INPUTPATH, d.tName[d.tExisting], "RdnTer", false))

        push!(outputs_list, OutputPrimal("hydro_res_dn", prb.options.INPUTPATH, d.hName, "RdnHyd", false))
    end

    # deficit
    push!(outputs_list, OutputPrimal("deficit", prb.options.INPUTPATH, d.bName, "def", true))

    # demand
    push!(outputs_list, OutputPrimal("demand", prb.options.INPUTPATH, d.bName, "demand", false, get_value_demand))

    if !options.ExplicitRen
        push!(outputs_list, OutputPrimal("netdemand", prb.options.INPUTPATH, d.bName, "netdemand", true, get_value_netdemand))
    end
    # price
    # push!(outputs_list, OutputPrice("price", prb.options.INPUTPATH, ["System"], "price", true))
    # push!(outputs_list,  OutputPrice("price_per_area", prb.options.INPUTPATH,  d.aName, "price_per_area", true))

    # market power analysis
    agents = [String(i) for i in vcat(prb.data.tName, prb.data.hName, prb.data.gName) ]
    # push!(outputs_list,  OutputIOR("ior_analysis", prb.options.INPUTPATH,  agents, "ior_analysis", false))

    if has_duals(m)
        push!(outputs_list, OutputDual("water_value", prb.options.INPUTPATH, d.hName, "HBal", true, get_value_dual))
        if prb.options.network == 0
            push!(outputs_list, OutputDual("load_marginal_cost", prb.options.INPUTPATH, ["System"], "Lbal", true, get_value_CMO))
        else
            push!(outputs_list, OutputDual("load_marginal_cost", prb.options.INPUTPATH, d.bName, "Lbal", true, get_value_CMO))
        end
    end

    # Output price
    if prb.options.network != 0
        # push!(outputs_list, OutputPrice("price_per_bus", prb.options.INPUTPATH, d.bName, "price_per_bus", true))

        # network
        push!(outputs_list, OutputPrimal("interc_flow_to", prb.options.INPUTPATH, d.intName, "IntercTo", true))
        push!(outputs_list, OutputPrimal("interc_flow_from", prb.options.INPUTPATH, d.intName, "IntercFrom", true))
    end

    write_avg_results!(m, prb)
    outputs_dict = open_outputs(prb, max_hours, outputs_list)
    write_outputs!(m, prb, outputs_dict)
    nothing
end

"""
    Open streams for outputs
"""
function open_outputs(prb::BidDispatchProblem, max_hours, outputs_list::Vector{Output})
    n = prb.n
    outputs_dict = Dict{String, Output}()
    for out in outputs_list
        # out.Ptr = initFile( joinpath(prb.options.INPUTPATH, out.name), out.agents)
        outputs_dict[out.name] = out
    end
    return outputs_dict
end

function close_outputs!(outputs_dicts::Vector{Output})
    for (key,out) in outputs_dicts
        close(out.Ptr)
    end
    nothing
end

function initFile(filename::String, agents=[])
    fstream = open(filename*".csv", "w")
    write(fstream, "Stage, Serie, Hour" )
    for ag in agents
        write(fstream, ",$ag" )
    end
    write(fstream, "\n" )

    return fstream
end

"""
    Writes all files
"""
function write_outputs!(m::JuMP.Model, prb::BidDispatchProblem, outputs_dict::Dict{String, Output})
    quantities = prb.data.quantities
    jump_vars = get_jump_varnames(m)

    for (key, out) in outputs_dict
        varname = out.var
        println(" file $(out.name)")

        values = out.get_value(m, prb, quantities, varname)
        if !out.var_serie
            nagent, nhours = size(values)
            for hour in 1:nhours
                write(out.Ptr, "1, 1, $hour")
                write_to_output!(out.Ptr, values[:, hour])
            end
        else
            nagent, nhours, nseries = size(values)
            for s in 1:nseries, hour in 1:nhours
                write(out.Ptr, "1, $s, $hour")
                write_to_output!(out.Ptr, values[:, hour, s])
            end
        end
        close(out.Ptr)
    end
    nothing
end
function get_jump_varnames(m::JuMP.Model)
    jump_vars = Set{String}()

    for i in all_variables(m)
        parsed = match(r"(.*)[\[\(].*", name(i))
        if !(parsed === nothing)
            push!(jump_vars, parsed[1])
        end
    end
    return jump_vars
end

function write_to_output!(iograf, values::Vector{Float64})
    for (idx, value) in enumerate(values)
        write(iograf, ",$(value)")
    end
    write(iograf, "\n" )
    nothing
end


"""
    Special methods using multiple dispatch to get variables/constraints values
"""
function get_value_HTurb(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    v = zeros(n.Hydros, n.MaxHours, n.ScenariosExtended)
    x = get_hturb(m, prb)

    for scen=1:n.ScenariosExtended, hour=1:n.MaxHours, h=1:n.Hydros
        v[h, hour, scen] = value(x[h, hour, scen])
    end
    return v
end
function get_value_HGen(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    v = zeros(n.Hydros, n.MaxHours, n.ScenariosExtended)
    x = get_hturb(m, prb)
    for scen=1:n.ScenariosExtended, hour=1:n.MaxHours, h=1:n.Hydros
        if d.hExist[h] <= 0
            v[h, hour, scen] = prb.data.fprodt[h]*value(x[h, hour, scen])
        end
    end

    return v
end
function get_value_TGen(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    v = zeros(length(d.tExisting), n.MaxHours, n.ScenariosExtended)
    x = get_tgen(m, prb)
    for scen=1:n.ScenariosExtended, hour=1:n.MaxHours, h in 1:length(d.tExisting)
        v[h, hour, scen] = value(x[h, hour, scen])
    end

    return v
end
function get_value_spillage(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    v = zeros(n.Hydros, n.MaxHours, n.ScenariosExtended)
    x = get_spillage(m, prb)
    for  scen=1:n.ScenariosExtended, hour=1:n.MaxHours, h=1:n.Hydros
        v[h, hour, scen] = value(x[h, hour, scen])
    end

    return v
end

function get_value_RGen(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    if prb.options.ExplicitRen
        return value.(m[Symbol("RGen")])
    elseif options.SpillRen

        Rspill = value.(m[:Rspill])
        prb.data.rengen

        Rdem = zeros(n.Demand, n.MaxHours, n.ScenariosExtended)
        for scen in 1:n.ScenariosExtended, hour in 1:n.MaxHours, sys in 1:n.Sys
            for ren in 1:n.Gnd
                if d.gnd2sys[ren] == sys
                    Rdem[sys, hour, scen] += prb.data.rengen[ren, hour, scen] - Rspill[sys, hour, scen]
                end
            end
        end
        return Rdem
    else

        Rdem = zeros(n.Demand, n.MaxHours, n.ScenariosExtended)
        for scen in 1:n.ScenariosExtended, hour in 1:n.MaxHours, sys in 1:n.Sys
            for ren in 1:n.Gnd
                if d.gnd2sys[ren] == sys
                    Rdem[sys, hour, scen] += prb.data.rengen[ren, hour, scen]
                end
            end
        end

        return Rdem
    end
end

function get_value_RGenDisp(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    if prb.options.ExplicitRen
        return value.(m[Symbol(varname)])
    else
        return prb.data.rengen
    end
end

function get_value_Rspill(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    v = zeros(n.Demand, n.MaxHours, n.ScenariosExtended)
    if options.SpillRen
        return value.(m[Symbol(varname)])
    else
        return v
    end

    nothing
end

function get_value_netdemand(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    if options.ExplicitRen
        return zeros(size(d.demand))
    else
        return d.netdemand
    end

    nothing
end

function get_value_demand(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    return d.demand
end

function get_value_CMO(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    # prb.data.cstrs => dictionary
    # prb.data.cstrs[varname] => list of dictionaries, each entry is (bus,hour)

    nscen = prb.options.SpillRen ? prb.n.ScenariosExtended : 1
    if prb.options.network == 1
        vec = zeros(Float64, prb.n.Sys, prb.n.MaxHours, nscen)
    else
        vec = zeros(Float64, 1, prb.n.MaxHours, nscen)
    end

    if has_duals(m)
        # all constraints refs
        all_cstrs = get_constraints(m)
        for cstr in all_cstrs
            parsed = match(r"(.*)\((.*)\)", name(cstr))

            if !isnothing(parsed) && parsed[1] == varname
                idx = parse.(Int, split(parsed[2], ",") )

                scen = prb.options.SpillRen ? idx[3] : 1
                vec[idx[1], idx[2], scen] = dual.(cstr)
            end
        end
    end

    return vec
end

function get_value_dual(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    # prb.data.cstrs => dictionary
    # prb.data.cstrs[varname] => list of dictionaries, each entry is (bus,hour)

    nagents = prb.n.Hydros
    vec = zeros(Float64, nagents, prb.n.MaxHours, prb.n.ScenariosExtended)
    if has_duals(m)
        # all constraints refs
        all_cstrs = get_constraints(m)
        for cstr in all_cstrs
            parsed = match(r"(.*)\((.*)\)", name(cstr))

            if !isnothing(parsed) && parsed[1] == varname
                idx = parse.(Int, split(parsed[2], ",") )

                scen = prb.options.SpillRen ? idx[3] : 1
                vec[idx[1], idx[2], scen] = dual.(cstr)
            end
        end
    end

    return vec
end

function get_value_resup(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    options = prb.options
    n = prb.n

    if varname == "HTurb"
        var = value.(get_hturb(m, prb))
    elseif varname == "HGen"
        var = value.(get_hturb(m, prb))
        var = prb.data.fprodt .* var
    elseif varname == "TGen"
        var = value.(get_tgen(m, prb))
    end

    nhours = size(var)[2]
    nagents = size(var)[1]
    nscen = size(var)[3]
    res = zeros(nagents, nhours)

    for h in 1:nhours, i in 1:nagents
        for s in 1:nscen
            vs = var[i,h,s] - var[i,h,prb.n.ScenariosExtended]
            if vs > res[i,h]
                res[i,h] = vs
            end
        end
    end

    return res
end

function get_value_resdn(m::JuMP.Model, prb::BidDispatchProblem, quantities, varname::String)
    options = prb.options
    n = prb.n
    d = prb.data

    options = prb.options
    n = prb.n

    if varname == "HTurb"
        var = value.(get_hturb(m, prb))
    elseif varname == "HGen"
        var = value.(get_hturb(m, prb))
        var = prb.data.fprodt .* var
    elseif varname == "TGen"
        var = value.(get_tgen(m, prb))
    end

    nhours = size(var)[2]
    nagents = size(var)[1]
    nscen = size(var)[3]
    res = zeros(nagents, nhours)

    for h in 1:nhours, i in 1:nagents
        for s in 1:nscen
            # res[i,h] = abs( maximum( max.(0.0, var[i,h,prb.n.ScenariosExtended] .- var[i,h,:]) ))
            vs = var[i,h,prb.n.ScenariosExtended] - var[i,h,s]
            if vs > res[i,h]
                res[i,h] = vs
            end
        end
    end

    return res
end

function get_value(m, prb::BidDispatchProblem, quantities, varname::String, out::OutputPrice)

    options = prb.options
    n = prb.n
    if n.Thermals > 0
        if options.runmode == DETERMINISTIC_MODE::RunMode
            g_ter = value.(m[:TGen])
        else
            g_ter = value.(get_tgen(m, prb))
        end
    end
    if n.Hydros > 0
        if options.runmode == DETERMINISTIC_MODE::RunMode
            g_hid = value.(m[:HTurb])
        else
            g_hid = value.(get_hturb(m, prb))
        end
    end
    if varname == "price"
        marginal = zeros(1, prb.n.MaxHours, prb.n.Scenarios)
        if n.Thermals > 0
            ind_ter = indicator(g_ter, prb.data.genForCstr2thermal, prb.data.genMinCstr2thermal, quantities["GenMinCstr"*"_ac"])
            marg_ter = ind_ter .* quantities["Ptherm"*"_ac"]

            update_marginal!(marginal, marg_ter)
        end
        if n.Gnd > 0 && options.ExplicitRen
            g_gnd = value.(m[:RGen])
            ind_gnd = indicator(g_gnd)
            marg_gnd = ind_gnd .* quantities["Prenew"*"_ac"]
            update_marginal!(marginal, marg_gnd)
        end
        if n.Hydros > 0
            ind_hid = indicator(g_hid, prb.data.genForCstr2hydro, prb.data.genMinCstr2hydro, quantities["GenMinCstr"*"_ac"])
            marg_hid = ind_hid .* quantities["Phydro"*"_ac"]
            update_marginal!(marginal, marg_hid)
        end
        return marginal

    elseif varname == "price_per_bus"
        # load values
        marginal = zeros(prb.n.Buses, prb.n.MaxHours, prb.n.Scenarios)
        for b in 1:n.Buses
            if n.Thermals>0
                ind_ter = indicator(g_ter, prb.data.genForCstr2thermal, prb.data.genMinCstr2thermal, quantities["GenMinCstr"*"_ac"])

                marg_ter = ind_ter .* quantities["Ptherm"*"_ac"]
                update_marginal!(marginal, marg_ter, prb.data.therm2Bus, b)
            end
            if n.Gnd>0 && options.ExplicitRen
                g_gnd = value.(m[:RGen])
                ind_gnd = indicator(g_gnd)
                marg_gnd = ind_gnd .* quantities["Prenew"*"_ac"]
                update_marginal!(marginal, marg_gnd, prb.data.ren2Bus, b)
            end
            if n.Hydros>0
                ind_hid = indicator(g_hid, prb.data.genForCstr2hydro, prb.data.genMinCstr2hydro, quantities["GenMinCstr"*"_ac"])
                marg_hid = ind_hid .* quantities["Phydro"*"_ac"]
                update_marginal!(marginal, marg_hid, prb.data.hyd2Bus, b)
            end
        end
    elseif varname == "price_per_area"
        marginal = zeros(prb.n.Areas, prb.n.MaxHours, prb.n.Scenarios)
        # get price
        for a in 1:n.Areas
            if n.Thermals>0
                ind_ter = indicator(g_ter, prb.data.genForCstr2thermal, prb.data.genMinCstr2thermal, quantities["GenMinCstr"*"_ac"])

                marg_ter = ind_ter .* quantities["Ptherm"*"_ac"]
                update_marginal!(marginal, marg_ter, prb.data.bus2area[prb.data.therm2Bus], a)
            end
            if n.Gnd>0 && options.ExplicitRen
                g_gnd = value.(m[:RGen])
                ind_gnd = indicator(g_gnd)
                marg_gnd = ind_gnd .* quantities["Prenew"*"_ac"]

                update_marginal!(marginal, marg_gnd, prb.data.bus2area[prb.data.ren2Bus], a)
            end
            if n.Hydros>0
                ind_hid = indicator(g_hid, prb.data.genForCstr2hydro, prb.data.genMinCstr2hydro, quantities["GenMinCstr"*"_ac"])
                marg_hid = ind_hid .* quantities["Phydro"*"_ac"]
                update_marginal!(marginal, marg_hid, prb.data.bus2area[prb.data.hyd2Bus], a)
            end
        end
    else
        throw("ERROR: invalid value! ("*varname*")")
    end
    return marginal
end
function get_value(m, prb::BidDispatchProblem, quantities, varname::String, out::OutputGenericPrice)
    options = prb.options
    n = prb.n
    v = value.(m[Symbol(out.pairs[1][1])])
    s = size(v)
    marginal = zeros(1, s[3])
    for (variablename, price) in out.pairs
        if sum(price) > 0
            v = value.(m[Symbol(variablename)])
            if variablename in ["SRTup", "SRTdn"]
              ind = indicator(v, prb.data.genForCstr2thermal, prb.data.genMinCstr2thermal, quantities["GenMinCstr"*"_ac"])
            elseif variablename in ["SRHup", "SRHdn"]
                ind = indicator(v, prb.data.genForCstr2hydro, prb.data.genMinCstr2hydro, quantities["GenMinCstr"*"_ac"])
            else
                ind = indicator(v)
            end
            ind = ind[:,1,:]
            marg_agent = ind .* price
            update_marginal2!(marginal, marg_agent)
        end
    end
    return marginal
end

function get_value(m, prb::BidDispatchProblem, quantities, varname::String, out::OutputIOR)
    options = prb.options
    n = prb.n

    therm_offer = quantities["Qtherm_ac"]
    ster = size(therm_offer)

    hydro_offer = quantities["Qhydro_ac"]
    shyd = size(hydro_offer)

    renew_offer = quantities["Qrenew_ac"]
    sren = size(renew_offer)

    nagents = ster[1] + shyd[1] + sren[1]
    nhours = ster[2]
    ior = zeros(nagents, nhours)
    for h in 1:nhours
        total = sum(therm_offer[:,h]) + sum(hydro_offer[:,h]) + sum(renew_offer[:,h])
        for ag in 1:ster[1]
            ior[ag, h] = (total - therm_offer[ag,h]) / sum(quantities["Demand_ac"][:,h])
        end
        for ag in 1:shyd[1]
            nag = ag + ster[1]
            ior[nag, h] = (total - hydro_offer[ag,h]) / sum(quantities["Demand_ac"][:,h])
        end
        for ag in 1:sren[1]
            nag = ag + ster[1] + shyd[1]
            ior[nag, h] = (total - renew_offer[ag,h]) / sum(quantities["Demand_ac"][:,h])
        end
    end
    return ior
end

"""
    Methdo to get constraints in the model
"""
# returns array of constraints
function get_constraints(m::JuMP.Model)
    all_cstrs = ConstraintRef[]
    cstr_types = list_of_constraint_types(m)
    for t in cstr_types
        # get constraints of type t
        append!(all_cstrs, all_constraints(m, t[1], t[2]))
    end
    return all_cstrs
end

function get_constraints_number(m::JuMP.Model, cstr_types::Array{Tuple{DataType,DataType},1})
    n = Int(0)
    for t in cstr_types
        # get NUMBER OF constraints of type t
        n+= num_constraints(m, t[1], t[2])
    end
    return n
end
function get_constraints_names(m::JuMP.Model)
    all_cstrs = get_constraints(m)
    names = get_constraints_names(all_cstrs)
    return names
end
function get_constraints_names(all_cstrs::Vector{ConstraintRef})
    # names = Vector{String}(undef, length(all_cstrs) )
    names = String[]
    for (i, cstr) in enumerate(all_cstrs)
        parsed = match(r"(.*)\((.*)\)", name(cstr))
        set_str_name!(names, parsed)
    end
    return names
end
function set_str_name!(names::Vector{String}, parsed::Nothing)
    push!(names, "NoName")
    nothing
end
function set_str_name!(names::Vector{String}, parsed::Any)
    push!(names, parsed[1])
    nothing
end
"""
    Method used to filter variables by some criteria
"""
function indicator(values, forcedplants = nothing, genminmap = nothing, genminval = nothing) #genMinCstr2hydro
    ind = zeros(size(values))
    (ni,nj,nk) =size(ind)
    if isnothing(forcedplants)
        forcedplants = zeros(ni)
    end
    genminplants = genminmap
    if isnothing(genminmap)
        genminplants = zeros(ni)
        genminval = zeros(ni)
    end

    for i = 1:ni, j = 1:nj, k = 1:nk
        gmin_idx = isnothing(genminmap) ? 0 : findfirst(x -> x == i, genminplants)
        # check for forced generation plants
        if values[i,j,k] > 0 && !(i in forcedplants)
            # check for genminplant
            if isnothing(genminmap) || !(i in genminplants)
                ind[i,j,k] = 1
            # check if generation is greater than genmin
            elseif values[i,j,k] > genminval[gmin_idx, j]
                ind[i,j,k] = 1
            end
        end
    end
    return ind
end

"""
    Auxiliary method to get maximum bid price
"""
function update_marginal!(marginal, new_vector)
    (ni,nj,nk) =size(marginal)
    for j = 1:nj, k = 1:nk
        max = maximum(new_vector[:, j, k])
        if max > marginal[1, j, k]
            marginal[1, j, k] = max
        end
    end
    nothing
end
function update_marginal2!(marginal, new_vector)
    (ni,nj) =size(marginal)
    for j = 1:nj
        max = maximum(new_vector[:, j])
        if max > marginal[1, j]
            marginal[1, j] = max
        end
    end
    nothing
end
function update_marginal!(marginal, new_vector, condition_vec, i)
    (ni,nj,nk) =size(marginal)
    for j = 1:nj, k = 1:nk
        # calculates the maximum conditional to i
        for idx in 1:size(new_vector)[1]
            # verifies if element belongs to mapping
            if condition_vec[idx] == i
                value = new_vector[idx, j, k]
                # maps max to i:
                if value > marginal[i, j, k]
                    marginal[i, j, k] = value
                end
            end
        end
    end

    nothing
end

function write_avg_results!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    g_hid = get_hturb(m, prb)
    g_ter = get_tgen(m, prb)
    spil = get_spillage(m, prb)

    if n.Gnd>0
        if options.ExplicitRen
            g_gnd = m[:RGen]
        end
    end

    avg_gen = false
    avg_hydro = false

    if avg_gen
        f = open(joinpath(prb.options.INPUTPATH, "AVG_gen.csv"), "w")
    end
    if avg_hydro
        f2 = open(joinpath(prb.options.INPUTPATH,"AVG_hydro.csv"), "w")
    end
    f3 = open(joinpath(prb.options.INPUTPATH, "generation.csv"), "w")
    avg_gen && write(f, "Hour, HGen, TGen \n")
    avg_hydro && write(f2, "Hour, HTurb, HSpil \n")

    header = join( ["$i - HGen, $i - TGen, $i - Renw" for i in d.bName], ",")
    write(f3, "Hour,"*"$header \n")

    for hour in 1:n.MaxHours
        # AVG GEN

        hyd_gen = 0.0
        ter_gen = 0.0
        for s in 1:n.ScenariosExtended
            for i in 1:n.Hydros
                if d.hExist[i] <= 0
                    hyd_gen += prb.data.fprodt[i] * value(g_hid[i,hour,s])
               end
            end
            for (j,t) in enumerate(d.tExisting)
                if d.tExist[t] <= 0
                    ter_gen += value(g_ter[j,hour,s])
                end
            end
        end

        avg_gen && write(f, "$hour, $(hyd_gen / n.ScenariosExtended), $(ter_gen / n.ScenariosExtended) \n")

        # AVG GEN BY SYS
        line = "$hour"
        if prb.options.network != 0
            for b in 1:n.Sys
                hyd_gen = 0.0
                ter_gen = 0.0
                ren_gen = 0.0

                for s in 1:n.ScenariosExtended
                    for i in 1:n.Hydros
                        if d.hyd2sys[i] == b && d.hExist[i] <= 0
                            hyd_gen += prb.data.fprodt[i] * value(g_hid[i,hour,s]) / n.ScenariosExtended
                        end
                    end
                    for (j,t) in enumerate(d.tExisting)
                        if d.ter2sys[t] == b && d.tExist[t] <= 0
                            ter_gen += value(g_ter[j,hour,s]) / n.ScenariosExtended
                        end
                    end
                    for i in 1:n.Gnd
                        if d.gnd2sys[i] == b && prb.data.gExist[i] <= 0
                            if options.ExplicitRen
                                ren_gen += value(g_gnd[i, hour, s]) / n.ScenariosExtended
                            else
                                ren_gen += d.rengen[i, hour, s] / n.ScenariosExtended
                            end
                        end
                    end
                end
                line *= ",$(hyd_gen), $(ter_gen), $(ren_gen)"
            end
            write(f3, line*"\n")
        end

        # AVG hydro
        v_hturb = sum(value.(g_hid[h,hour,:]) for h in 1:n.Hydros if d.hExist[h] <=0) / n.ScenariosExtended
        v_spil =  sum(value.(spil[h,hour,:]) for h in 1:n.Hydros if d.hExist[h] <=0) / n.ScenariosExtended
        avg_hydro && write(f2, "$hour, $v_hturb, $v_spil \n")
    end
    avg_gen && close(f)
    avg_hydro && close(f2)
    close(f3)
    nothing
end
"""
    Writes total time and total cost
"""
function print_global_results!(prb, m, elapsed)

     println("time elapsed: $elapsed s")
     open(joinpath(prb.options.INPUTPATH, "global_results.csv"), "w") do f
         write(f, "total cost: $(objective_value(m)) \n")
         write(f, "time elapsed: $elapsed s \n")
     end
end


function write_fobj(prb::BidDispatchProblem)
    fobj = prb.data.fobj
    filename = "objective_function_agg.csv"

    open(joinpath(prb.options.INPUTPATH, filename), "w") do f
        header = "hydroCosts, FutureCost, thermalCosts, CommitmentCost, RenewableCosts, ReserveCosts, DeficitCost, RedispatchCost"
        write(f, header*"\n")

        values = "$(sum(value.(fobj.hydroCosts))/prb.n.ScenariosExtended), $(sum(value.(fobj.FutureCost))/prb.n.ScenariosExtended), $(sum(value.(fobj.thermalCosts))/prb.n.ScenariosExtended), $(sum(value.(fobj.CommitmentCost))/prb.n.ScenariosExtended), $(sum(value.(fobj.RenewableCosts))/prb.n.ScenariosExtended), $(sum(value.(fobj.ReserveCosts))/prb.n.ScenariosExtended), $(sum(value.(fobj.DeficitCost)/prb.n.ScenariosExtended)), $(sum(value.(fobj.RedispatchCost))/prb.n.ScenariosExtended)"
        write(f, values*"\n")
    end
end

function write_fobj_scenario(prb::BidDispatchProblem)
    fobj = prb.data.fobj
    filename = "objective_function.csv"

    open(joinpath(prb.options.INPUTPATH, filename), "w") do f
        header = "scenario, hydroCosts, FutureCost, thermalCosts, CommitmentCost, RenewableCosts, ReserveCosts, DeficitCost, RedispatchCost"
        write(f, header*"\n")

        for s in 1:prb.n.ScenariosExtended
            values = "$s, $(value(fobj.hydroCosts[s])), $(value(fobj.FutureCost[s])), $(value(fobj.thermalCosts[s])), $(value(fobj.CommitmentCost)), $(value(fobj.RenewableCosts[s])), $(value(fobj.ReserveCosts[s])), $(value(fobj.DeficitCost[s])), $(value(fobj.RedispatchCost[s]))"
            write(f, values*"\n")
        end
    end
end