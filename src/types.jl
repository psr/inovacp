#
# ENUMS
# --
@enum InputType Graf External Reserve Dictionary Fcf

isnothing(::Any) = false
isnothing(::Nothing) = true
mysum(expr) = reduce(+, expr; init = 0.0)
#
# Helper macro
# ------------

"""
@kwdef mutable structdef
This is a helper macro that automatically defines a keyword-based constructor for the type
declared in the expression `typedef`, which must be a `struct` or `mutable struct`
expression. The default argument is supplied by declaring fields of the form `field::T =
default`. If no default is provided then the default is provided by the `kwdef_val(T)`
function.
```julia
@kwdef struct Foo
a::Cint            # implied default Cint(0)
b::Cint = 1        # specified default
z::Cstring         # implied default Cstring(C_NULL)
y::Bar             # implied default Bar()
end
```
"""
macro kwdef(expr)
    @static if VERSION >= v"0.7"
        expr = macroexpand(__module__, expr) # to expand @static
    else
        expr = macroexpand(expr) # to expand @static
    end
    T = expr.args[2]
    params_ex = Expr(:parameters)
    call_ex = Expr(:call, T)
    _kwdef!(expr.args[3], params_ex, call_ex)
    quote
        Base.@__doc__($(esc(expr)))
        $(esc(Expr(:call,T,params_ex))) = $(esc(call_ex))
    end
end

# @kwdef helper function
# mutates arguments inplace
function _kwdef!(blk, params_ex, call_ex)
    for i in eachindex(blk.args)
        ei = blk.args[i]
        isa(ei, Expr) || continue
        if ei.head == :(=)
            # var::Typ = defexpr
            dec = ei.args[1]  # var::Typ
            var = dec.args[1] # var
            def = ei.args[2]  # defexpr
            push!(params_ex.args, Expr(:kw, var, def))
            push!(call_ex.args, var)
            blk.args[i] = dec
        elseif ei.head == :(::)
            dec = ei # var::Typ
            var = dec.args[1] # var
            def = :(kwdef_val($(ei.args[2])))
            push!(params_ex.args, Expr(:kw, var, def))
            push!(call_ex.args, dec.args[1])
        elseif ei.head == :block
            # can arise with use of @static inside type decl
            _kwdef!(ei, params_ex, call_ex)
        end
    end
    blk
end

"""
    kwdef_val(T)
The default value for a type for use with the `@kwdef` macro. Returns:
 - null pointer for pointer types (`Ptr{T}`, `Cstring`, `Cwstring`)
 - zero for integer types
 - no-argument constructor calls (e.g. `T()`) for all other types
"""
function kwdef_val end

kwdef_val(::Type{Ptr{T}}) where {T} = Ptr{T}(C_NULL)
kwdef_val(::Type{Cstring}) = Cstring(C_NULL)
kwdef_val(::Type{Cwstring}) = Cwstring(C_NULL)

kwdef_val(::Type{T}) where {T<:Real} = zero(T)

kwdef_val(::Type{T}) where {T} = T()
kwdef_val(::Type{IOStream}) = IOStream("tmp")
kwdef_val(::Type{T}) where {T<:Array{Array{Int32,1},1}} = [Int32[]]
kwdef_val(::Type{Array{Array{Int32,1},1}}) = [Int32[]]


kwdef_val(::Type{T}) where {T<:String} = ""
kwdef_val(::Type{T}) where {T<:Symbol} = :NULL

@static if VERSION >= v"0.7"
    kwdef_val(::Type{Array{T,N}}) where {T,N} = Array{T}(undef, zeros(Int,N)...)
else
    kwdef_val{T,N}(::Type{Array{T,N}})::Array{T,N} = Array{T}(tuple(zeros(Int,N)...))
end

# Types
const PSRClassesPtr =  Ptr{UInt8}

@kwdef mutable struct ObjectiveFunction
    hydroCosts::Vector{JuMP.GenericAffExpr{Float64,JuMP.VariableRef}}
    FutureCost::Vector{JuMP.GenericAffExpr{Float64,JuMP.VariableRef}}
    thermalCosts::Vector{JuMP.GenericAffExpr{Float64,JuMP.VariableRef}}
    CommitmentCost::JuMP.GenericAffExpr{Float64,JuMP.VariableRef}
    RenewableCosts::Vector{JuMP.GenericAffExpr{Float64,JuMP.VariableRef}}
    ReserveCosts::Vector{JuMP.GenericAffExpr{Float64,JuMP.VariableRef}}
    DeficitCost::Vector{JuMP.GenericAffExpr{Float64,JuMP.VariableRef}}
    RedispatchCost::Vector{JuMP.GenericAffExpr{Float64,JuMP.VariableRef}}
end

@kwdef mutable struct BidDispatchSizes

    MaxHours   ::Int
    Clusters   ::Int
    # basic
    Hydros     ::Int
    Thermals   ::Int
    Gnd        ::Int
    GndStations::Int
    Stations   ::Int
    Scenarios  ::Int
    ScenariosExtended  ::Int
    OriginalScenarios  ::Int
    Blocks  ::Int

    #
    Demand     ::Int
    DemSeg     ::Int
    duraci_t   ::Int

    # Thermals
    Fuels::Int
    FuelCons::Int

    # network
    Sys      ::Int
    Buses      ::Int
    DCLinks    ::Int
    Circuits    ::Int
    Areas      ::Int
    CircSumCtr ::Int
    Intercs ::Int

    # constraints
    IntercSumCstr ::Int
    GenResCstr ::Int
    GenGzdCstr ::Int

end

@kwdef mutable struct BidDispatchData
    initial_state_commit::Vector{Int}
    initial_state_time  ::Vector{Int}
    initial_power       ::Vector{Float64}
    cstrs::Dict{Any,Any}
    quantities::Dict{Any,Any}
    inpts::Dict{Any,Any}
    forecast::Array{Float64,2}
    logstream::IOStream
    mapping::Dict{Any,Any}
    hour::Int32
    scen::Int32
    delta::Array{Float64,3}
    tau::Array{Float64,3}
    HTurb::Array{GenericAffExpr{Float64,VariableRef},3}
    spillage::Array{GenericAffExpr{Float64,VariableRef},3}
    TGen::Array{GenericAffExpr{Float64,VariableRef},3}

    tx_discount::Float64
    vpl_coef::Float64

    scen2clus::Vector{Int}
    clus2scen::Vector{Vector{Int}}

    # local static
    dcost::Float64 #load shedding cost
    duraci_t::Int

    #
    # Fobj
    # ---------------
    fobj::ObjectiveFunction

    # --------------------
    # HYDRO
    #---------------------
    hyd2sys::Vector{Int32}
    hyd2gen::Vector{Int32}
    hyd2bus::Vector{Int32}

    # mapped static
    hName::Vector{String}
    hCode::Vector{Int32}
    hIsCommit::Vector{Int32}#VECTOR
    hExist::Vector{Int32}#VECTOR # 0/1
    downstream_turb::Vector{Int32}#VECTOR # 0/1
    downstream_spill::Vector{Int32}#VECTOR # 0/1
    upstream_turb::Array{Array{Int32,1},1}#VECTOR # 0/1
    upstream_spill::Array{Array{Int32,1},1}#VECTOR # 0/1
    fprodt::Vector{Float64}#VECTOR
    hyd2station::Vector{Int32}#VECTOR
    volini::Vector{Float64}#VECTOR
    volmax::Vector{Float64}#VECTOR
    defmax::Vector{Float64}#VECTOR
    defmin::Vector{Float64}#VECTOR
    turb_max::Vector{Float64}#VECTOR
    hPotInst::Vector{Float64}#VECTOR
    conv::Float64#VECTOR
    inflow::Vector{Float64}#VECTOR
    inflowStage::Vector{Float64}#VECTOR
    inflow_forecast::Vector{Float64}#VECTOR
    reservoirs::Vector{Int}#VECTOR
    runoffriver::Vector{Int}#VECTOR
    volmin::Vector{Float64}#VECTOR

    # --------------------
    # THERMAL
    # --------------------
    # mapped static
    tName::Vector{String}
    tType::Vector{Int32}
    tExistRegistry::Vector{Int32}

    # relations
    ter2sys::Vector{Int32}
    ter2gen::Vector{Int32}
    ter2bus::Vector{Int32}
    ter2fuel::Vector{Int32}

    tGerMax::Vector{Float64}
    tGerMax_orig::Vector{Float64}
    tPot::Vector{Float64}# MW
    tGerMin::Vector{Float64}# MW
    tExist::Vector{Int32}
    tExisting::Vector{Int32}
    is_commit::Vector{Int32}
    startupCold::Vector{Float64}
    timeIsCold::Vector{Float64}
    startupWarm::Vector{Float64}
    timeIsWarm::Vector{Float64}
    startupHot::Vector{Float64}
    timeIsHot::Vector{Float64}
    startup_old::Bool

    # COMT
    tShutDownCost::Vector{Float64}
    tPotInst::Vector{Float64}
    tCVU::Vector{Float64}
    tRampUpCold::Vector{Float64}
    tRampUpWarm::Vector{Float64}
    tRampDown::Vector{Float64}
    tMinUptime::Vector{Float64}
    tMinDowntime::Vector{Float64}
    tMaxStartUps::Vector{Int32}
    tMaxShutDowns::Vector{Int32}

    # fuel
    fName::Vector{String}
    fCost::Vector{Float64}
    fuelconsumptionrate::Vector{Float64}
    fcs2fls::Vector{Int}
    fcs2ter::Vector{Int}
    ter2fcs::Vector{Vector{Int}}
    fcsCost::Array{Float64,2}
    fcsSegment::Array{Float64,2}
    fcs_nsegments::Array{Int32,1}

    # --------------------
    # GND
    #---------------------
    gName::Vector{String}
    gStationsName::Vector{String}
    gExist::Vector{Int32}
    gFatOp::Vector{Float64}
    rPotInst::Vector{Float64}
    rBusName::Vector{String}
    rStation::Vector{String}
    Stations::Vector{String}
    ren2stat::Vector{Int}
    gnd2gen::Vector{Int32}
    gnd2bus::Vector{Int32}
    gnd2sys::Vector{Int32}

    gGerHourly::Vector{Float64}
    rengen::Array{Float64,3}

    # ---------------
    # Buses
    # ---------------

    # mapped static
    bName::Vector{String}

    # ---------------
    # LOADS
    # ---------------

    # relations
    dem2bus::Vector{Int32}
    dem2sys::Vector{Int32}
    sys2dem::Array{Array{Int32,1},1}
    bus2dem::Vector{Int32}
    dsg2sys::Vector{Int32}
    dsg2dem::Vector{Int32}
    lod2bus::Vector{Int32}
    lod2dem::Vector{Int32}
    demandHour::Vector{Float64}
    demand::Array{Float64,2}
    netdemand::Array{Float64,3}


    # Interconnection
    intName::Vector{String}
    intExist::Vector{Int32}
    iCapacityFrom::Vector{Float64}
    iCapacityTo::Vector{Float64}
    iLossFrom::Vector{Float64}
    iLossTo::Vector{Float64}
    intFsys::Vector{Int32}# ->
    int2sys::Vector{Int32}# <-

    #
    # NETWORK
    #
    # ----------------

    # mapped static
    dName::Vector{String}
    dAVId::Vector{String}
    dCode::Vector{Int32}

    # relations
    dclFbus::Vector{Int32}# ->
    dcl2bus::Vector{Int32}# <-

    # mapped dynamic
    dExist::Vector{Int32}
    dCapacityFrom::Vector{Float64}
    dCapacityTo::Vector{Float64}
    dLossFrom::Vector{Float64}
    dLossTo::Vector{Float64}

    dReactanceFrom::Vector{Float64}
    dReactanceTo::Vector{Float64}
    dNameBusFrom::Vector{String}
    dNameBusTo::Vector{String}
    hBusName::Vector{String}
    tBusName::Vector{String}

    therm2Bus::Vector{Int}
    hyd2Bus::Vector{Int}
    gen2bus::Vector{Int}
    ren2Bus::Vector{Int}

    # ------------------
    # Electrical Area
    # ------------------
    aName::Vector{String}
    aAVId::Vector{String}
    aCode::Vector{Int32}

    bus2area::Vector{Int32}
    area2bus::Vector{Int32}
    areaCstr2area::Vector{Int32}
    genForCstr2hydro::Vector{Int32}
    genForCstr2thermal::Vector{Int32}
    genMinCstr2hydro::Vector{Int32}
    genMinCstr2thermal::Vector{Int32}
    aCstr::Array{Tuple{Int,Int,Float64},1}
    aExpCstr::Array{Tuple{Int,Int,Float64},1}

    # Reserve
    therm2resPup::Vector{Int32}
    resPup2therm::Vector{Int32}
    therm2resPdn::Vector{Int32}
    resPdn2therm::Vector{Int32}
    therm2QresSup::Vector{Int32}
    PresSup2therm::Vector{Int32}
    resSup2therm::Vector{Int32}
    therm2QresSdn::Vector{Int32}
    PresSdn2therm::Vector{Int32}
    resSdn2therm::Vector{Int32}
    therm2resT::Vector{Int32}
    resT2therm::Vector{Int32}
    hyd2resPup::Vector{Int32}
    resPup2hyd::Vector{Int32}
    hyd2resPdn::Vector{Int32}
    resPdn2hyd::Vector{Int32}
    hyd2resSup::Vector{Int32}
    hyd2QresSup::Vector{Int32}
    PresSup2hyd::Vector{Int32}
    hyd2resSdn::Vector{Int32}
    hyd2QresSdn::Vector{Int32}
    PresSdn2hyd::Vector{Int32}
    hyd2resT::Vector{Int32}
    resT2hyd::Vector{Int32}
    ren2resPup::Vector{Int32}
    resPup2ren::Vector{Int32}
    ren2resPdn::Vector{Int32}
    resPdn2ren::Vector{Int32}
    ren2resSup::Vector{Int32}
    resSup2ren::Vector{Int32}
    ren2resSdn::Vector{Int32}

    resSdn2ren::Vector{Int32}
    ren2resT::Vector{Int32}
    resT2ren::Vector{Int32}
    nresp::Int
    nress::Int
    nrest::Int
    has_resPup::Bool
    has_resPdn::Bool
    has_resSup::Bool
    has_resSdn::Bool
    has_resT::Bool
    tup2ter::Vector{Int32}
    tup2hyd::Vector{Int32}
    ter2tup::Vector{Int32}
    hyd2tup::Vector{Int32}
end

# deterministic =1 stochastic classic=2 perfect information=3 stochastic affine=4 true-up=5
# @enum RunMode DETERMINISTIC_MODE=1 estclassica=2 affine=4 trueup=5 bid=6
@enum RunMode DETERMINISTIC_MODE=1 AFFINE_MODE=2 TRUEUP_MODE=3
@enum ReserveType NoReserve=0 Individual=1 Coopt=2 CAGLoad=93 CAGPercent=94 CAGCoopt=95 CooptByCategory=96
@enum SolverId CBC_SOLVER=1 GLPK_SOLVER=2 CPLEX_SOLVER=3 XPRESS_SOLVER=4

@kwdef mutable struct BidDispatchOptions
    # paths
    # -----------
    INPUTPATH::String # ends with "\\"
    PSRCPATH::String
    OUTPUTPATH::String
    IHMPATH::String
    solver::SolverId = CBC_SOLVER::SolverId

    network::Int32 = 1
    kirchoff2ndlaw::Int32 = 1
    networklosses::Int32 = 0
    runmode::RunMode=DETERMINISTIC_MODE::RunMode
    robust::Bool
    reserve::ReserveType = NoReserve::ReserveType
    ExternalRenewables::Bool
    ExternalInflow::Bool
    SpillRen::Bool

    additional_hours::Int
    horizon_with_additional::Int

    coef_variation_dem::Float64 = 0.05/3

    # commitment
    commit_for_all::Bool = false
    commitment::Bool = true
    ramp::Bool = true
    minupdntime::Bool = true
    maxupdntime::Bool = true
    startupcost::Bool = true
    solvelp::Bool = true
    logprint::Bool = true
    hydroBlocks::Int32 = 1
    thermalAffine::Bool = true
    AffineFeas::Bool = true
    AffineClus::Bool = false
    ExplicitRen::Bool = true
    fuelConsumption::Bool = true
    stage::Int32 = 1

    initial_hour::Int
    repeat_last_hour::Bool
    solver_silent::Bool = true
    solver_time_limit::Int = 0

    # solver
    maxtime::Int32 = 18000
    mipstop::Float64 = 0.005
end

@kwdef mutable struct BidDispatchPointers

    # basic
    study_ptr   ::PSRClassesPtr
    iosddp   ::PSRClassesPtr

    # timecontroller
    timec_ptr ::PSRClassesPtr
    timec_shortterm ::PSRClassesPtr

    # configuration
    config_ptr ::PSRClassesPtr
    iexec ::PSRClassesPtr
    ihourly ::PSRClassesPtr

    #Scenarios
    gnd_ptr::PSRClassesPtr

    # inflow
    inflow::PSRClassesPtr

    # demand
    demand::PSRClassesPtr

    # Fcf
    fcf::PSRClassesPtr

    # lists of elements
    sys_lst::PSRClassesPtr
    ter_lst::PSRClassesPtr
    hyd_lst::PSRClassesPtr
    gnd_lst::PSRClassesPtr
    GndStations_lst::PSRClassesPtr
    sts_lst::PSRClassesPtr
    dem_lst::PSRClassesPtr
    dsg_lst::PSRClassesPtr
    fls_lst::PSRClassesPtr
    fcs_lst::PSRClassesPtr

    # list of network
    dcl_lst::PSRClassesPtr
    cir_lst::PSRClassesPtr
    int_lst::PSRClassesPtr
    bus_lst::PSRClassesPtr
    gen_lst::PSRClassesPtr
    lod_lst::PSRClassesPtr

    # list of constraints
    isc_lst::PSRClassesPtr
    csc_lst::PSRClassesPtr
    are_lst::PSRClassesPtr
    grc_lst::PSRClassesPtr
    ggc_lst::PSRClassesPtr

    # mappings
    hyd_map_dyn::PSRClassesPtr
    ter_map_dyn::PSRClassesPtr
    fls_map_dyn::PSRClassesPtr
    gnd_map_dyn::PSRClassesPtr
    gnd_map_dyn_hor::PSRClassesPtr
    int_map_dyn::PSRClassesPtr
    isc_map_dyn::PSRClassesPtr
    int_map_dyn_blk::PSRClassesPtr
    dsg_map_dyn_hor::PSRClassesPtr

    # shortterm mapping
    shortterm ::PSRClassesPtr
end

@kwdef mutable struct BidDispatchProblem
    data::BidDispatchData
    n::BidDispatchSizes
    options::BidDispatchOptions
    psrc::BidDispatchPointers
    starttime::Float64
end

function has_reserve(prb::BidDispatchProblem)
    prb.options.reserve != NoReserve::ReserveType
end
