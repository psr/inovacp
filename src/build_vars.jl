"""
    Methodos for creation of standard variables
"""

function var_storage!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Hydros>0
        # stored water (in hm3)
        hset = hydroBlockSet(prb)

        @variable(m, d.volmin[d.reservoirs[i]]  <= HVolF[i=1:length(d.reservoirs), hblock=1:length(hset), s=1:n.ScenariosExtended] <= d.volmax[d.reservoirs[i]])
        for i=1:length(d.reservoirs), hblock=1:length(hset), s=1:n.ScenariosExtended
            set_name(HVolF[i, hblock, s], "HVolF($i,$hblock,$s)")
        end
    end
end



# system deficit slack variable
function var_deficit!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    buses = 1
    if options.network == 1
        buses = n.Sys
    elseif  options.network == 2
        buses = n.Buses
    end

    @variable(m, def[1:buses, 1:prb.n.MaxHours, 1:(n.ScenariosExtended)]>=0)
    for bus=1:buses, hour=1:prb.n.MaxHours, scen=1:(n.ScenariosExtended)
        set_name(def[bus, hour, scen], "defct($bus,$hour,$scen)")
    end
    nothing
end
function var_interc!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    if n.Intercs > 0 && options.network == 1
        # S = options.runmode == AFFINE_MODE::RunMode ? 1 : n.Scenarios
        S =  n.ScenariosExtended

        @variable(m, IntercTo[1:n.Intercs, 1:prb.n.MaxHours, 1:S])
        @variable(m, IntercFrom[1:n.Intercs, 1:prb.n.MaxHours, 1:S])
        for h in 1:prb.n.MaxHours, i in 1:n.Intercs, s in 1:S
            set_name(IntercTo[i, h, s], "IntercTo($i,$h,$s)")
            set_name(IntercFrom[i, h, s], "IntercFrom($i,$h,$s)")

            set_lower_bound(IntercTo[i, h, s], 0.0)
            set_upper_bound(IntercTo[i, h, s], d.iCapacityTo[i])
            set_lower_bound(IntercFrom[i, h, s], 0.0)
            set_upper_bound(IntercFrom[i, h, s], d.iCapacityFrom[i])
        end
    end
    nothing
end
function var_dclinks(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    if n.DCLinks > 0 && options.network == 2
        @variable(m, DCTo[1:n.DCLinks, 1:prb.n.MaxHours, 1:n.Scenarios])
        @variable(m, DCF[1:n.DCLinks, 1:prb.n.MaxHours, 1:n.Scenarios])
        for s in 1:n.Scenarios, h in 1:prb.n.MaxHours, i in 1:n.DCLinks
            set_lower_bound(DCTo[i, h, s], 0.0)
            set_upper_bound(DCTo[i, h, s], d.dCapacityTo[i])
            set_lower_bound(DCF[i, h, s], 0.0)
            set_upper_bound(DCF[i, h, s], d.dCapacityFrom[i])
        end
    end
    nothing
end
function var_thetas(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    if n.Buses > 0 && options.network == 2
        @variable(m, -Inf <= theta[1:n.Buses, 1:prb.n.MaxHours, 1:n.Scenarios] <= Inf)
    end
    nothing
end
function var_COMT(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    quantities = prb.data.quantities

    if n.Thermals>0
        if options.runmode != TRUEUP_MODE::RunMode
            @variable(m, COMT[1:length(d.tExisting), 1:prb.n.MaxHours], Bin)
            for t=1:length(d.tExisting), h=1:prb.n.MaxHours
                if d.is_commit[ d.tExisting[t] ] == 0
                    set_upper_bound(COMT[t,h], 0)
                end
                set_name(COMT[t, h], "COMT($t,$h)")
            end
        else # var is continuous

            # fix commitment

            @variable(m, COMT[j=1:length(d.tExisting), hour=1:prb.n.MaxHours] == quantities["TUP_cmit"].value[1 ,1, hour, j])

            for t=1:length(d.tExisting), h=1:prb.n.MaxHours
                set_name(COMT[t, h], "COMT($t,$h)")
            end
        end
    end
    nothing
end
function var_startup(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    if n.Thermals>0
        @variable(m, 0 <= STup[1:length(d.tExisting),1:prb.n.MaxHours] <= 1)
        for t=1:length(d.tExisting), h=1:prb.n.MaxHours
            set_name(STup[t, h], "STup($t,$h)")
        end
        if options.startupcost
            # @variable(m, StartUpCost[1:length(d.tExisting), 1:prb.n.MaxHours] >= 0)
            # @variable(m, State[1:length(d.tExisting), 1:prb.n.MaxHours, 1:prb.n.MaxHours], Bin)
        end
    end
    nothing
end
function var_shutdown(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    if n.Thermals>0
        @variable(m, 0 <= STdn[1:length(d.tExisting), 1:prb.n.MaxHours] <= 1)
        for t=1:length(d.tExisting), h=1:prb.n.MaxHours
            set_name(STdn[t, h], "STdn($t,$h)")
        end
    end
    nothing
end
function var_reserve(m::JuMP.Model, prb::BidDispatchProblem, mapping)
    n = prb.n
    d = prb.data
    options = prb.options

    if has_reserve(prb)
        if n.Thermals>0
            @variable(m, RdnTer[1:length(d.tExisting), 1:prb.n.MaxHours] >= 0)
            @variable(m, RupTer[1:length(d.tExisting), 1:prb.n.MaxHours] >= 0)
            @variable(m, RupTerSlk[1:length(d.tExisting), 1:prb.n.MaxHours] >= 0)
            @variable(m, RdnTerSlk[1:length(d.tExisting), 1:prb.n.MaxHours] >= 0)
            for t=1:length(d.tExisting), h=1:prb.n.MaxHours
                set_name(RdnTer[t, h], "RdnTer($t,$h)")
                set_name(RupTer[t, h], "RupTer($t,$h)")
            end
        end
        if n.Hydros > 0
            @variable(m, RdnHydSlk[1:n.Hydros, 1:prb.n.MaxHours] >= 0)
            @variable(m, RupHydSlk[1:n.Hydros, 1:prb.n.MaxHours] >= 0)
            @variable(m, RdnHyd[1:n.Hydros, 1:prb.n.MaxHours] >= 0)
            @variable(m, RupHyd[1:n.Hydros, 1:prb.n.MaxHours] >= 0)
            for i=1:n.Hydros, h=1:prb.n.MaxHours
                set_name(RdnHyd[i, h], "RdnHyd($i,$h)")
                set_name(RupHyd[i, h], "RupHyd($i,$h)")
            end
        end
    end
    nothing
end


"""
    Especial variables

    These variables may not be created depending on the RunMode
"""
function var_thermalgen!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Thermals>0
        # Thermal genration (in MWh = MW*h)
        # ---------------------------------        @show 2
        if options.runmode == DETERMINISTIC_MODE::RunMode  || ( prb.options.runmode == AFFINE_MODE::RunMode && !prb.options.thermalAffine)
            @variable(m,  0.0 <= TGen[t in d.tExisting, hour=1:(prb.n.MaxHours), s=1:n.ScenariosExtended] <= d.tPotInst[t])
            for t=1:length(d.tExisting), h=1:prb.n.MaxHours, s=1:n.ScenariosExtended
                set_name(TGen[t, h, s], "TGen($t,$h,$s)")
            end
        end
    end
    nothing
end
function var_fuelconsumptions!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options
    MAX_EFF_SEGS = maximum(d.fcs_nsegments)

    if n.FuelCons>0
        # Thermal genration (in MWh = MW*h)
        # ---------------------------------

        @variable(m,  FC[l=1:MAX_EFF_SEGS, j=1:n.FuelCons, hour=1:(prb.n.MaxHours), s=1:n.ScenariosExtended] >= 0.0 )
        for l=1:MAX_EFF_SEGS, j=1:n.FuelCons, h=1:(prb.n.MaxHours), s=1:n.ScenariosExtended
            set_name(FC[l, j, h, s], "FC($l,$j,$h,$s)")
        end
        for j in 1:n.FuelCons
            t = d.fcs2ter[j]
            if d.tExist[t] == 0
                for l in 1:MAX_EFF_SEGS
                    if l in d.fcs_nsegments[j]
                        set_upper_bound.(FC[l,j,:,:], fcssegment_ub(l, j, prb))
                    else
                        set_upper_bound.(FC[l,j,:,:], 0.0)
                    end
                end
            else
                for l in 1:d.fcs_nsegments[j]
                    set_upper_bound.(FC[l,j,:,:], 0.0)
                end
            end
        end

        if prb.options.runmode != AFFINE_MODE::RunMode || ( prb.options.runmode == AFFINE_MODE::RunMode && !prb.options.thermalAffine)
            # build Tgen expression with fuel consumption
            d.TGen = @expression(m, [t=1:length(d.tExisting), hour=1:n.MaxHours, scen=1:n.ScenariosExtended], sum(FC[ l , j , hour, scen] for j in 1:n.FuelCons, l in 1:d.fcs_nsegments[j] if d.tExisting[t] == d.fcs2ter[j]) )
        end

    end

    nothing
end
function fcssegment_ub(l::Integer, j::Integer, prb::BidDispatchProblem)
    d = prb.data
    return max(0.0, d.tGerMax[d.fcs2ter[j]]*d.fcsSegment[l,j])
end
function fcs_ub(j::Integer, prb::BidDispatchProblem)
    d = prb.data
    return max(0.0, d.tGerMax[d.fcs2ter[j]])
end
function thermal_ub(j::Integer, prb::BidDispatchProblem)
    d = prb.data
    return max(0.0, d.tGerMax[j])
end

function var_gergnd!(m::JuMP.Model, prb::BidDispatchProblem)
    # GND with max gen representation (possible to spill) (in 1000*GWh)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Gnd>0
        if options.ExplicitRen
            @variable(m, 0.0 <= RGen[r=1:n.Gnd, hour=1:prb.n.MaxHours, s=1:n.ScenariosExtended] <= d.rPotInst[r])
        elseif options.SpillRen
            # spill variable
            @variable(m, Rspill[r=1:n.Demand, hour=1:prb.n.MaxHours, s=1:n.ScenariosExtended] >= 0)
        end
    end
    nothing
end

function var_trueup!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    if options.runmode == TRUEUP_MODE::RunMode
        if n.Thermals> 0
            nexist = length(d.tExisting)
            @variable(m, TPter[1:nexist, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
            @variable(m, TPTs1[1:nexist, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
            @variable(m, TPTs2[1:nexist, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
            @variable(m, TPTs1slk[1:nexist, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
            @variable(m, TPTs2slk[1:nexist, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
            for j=1:nexist, h=1:(prb.n.MaxHours), s=1:n.Scenarios
                set_name(TPter[j, h, s], "TPter($j,$h,$s)")
                set_name(TPTs1[j, h, s], "TPTs1($j,$h,$s)")
                set_name(TPTs2[j, h, s], "TPTs2($j,$h,$s)")
            end
        end
        if n.Hydros > 0
            @variable(m, TPhyd[1:n.Hydros, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
            @variable(m, TPspil[1:n.Hydros, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
            @variable(m, TPHs1[1:n.Hydros, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
            @variable(m, TPHs2[1:n.Hydros, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
            @variable(m, TPHs1slk[1:n.Hydros, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
            @variable(m, TPHs2slk[1:n.Hydros, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
            for h=1:n.Hydros, hour=1:(prb.n.MaxHours), s=1:n.Scenarios
                set_name(TPhyd[h, hour, s], "TPhyd($h,$hour,$s)")
                set_name(TPspil[h, hour, s], "TPspil($h,$hour,$s)")
                set_name(TPHs1[h, hour, s], "TPHs1($h,$hour,$s)")
                set_name(TPHs2[h, hour, s], "TPHs2($h,$hour,$s)")
            end
        end
    end
    nothing
end

# Turbined water (in hm3)
function var_turbining!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Hydros>0
        # Turbined water (in hm3)

        @variable(m, 0.0 <= HTurb[h=1:n.Hydros, hour=1:prb.n.MaxHours, 1:n.Scenarios] <= d.turb_max[h] )

        @variable(m, spillage[h=1:n.Hydros, 1:prb.n.MaxHours, 1:n.Scenarios] >= 0)
        for h=1:n.Hydros, hour=1:(prb.n.MaxHours), s=1:n.Scenarios
            defmax = d.defmax[h] > 0 ? 100*d.defmax[h] : 1e3
            set_upper_bound(spillage[h, hour, s], defmax)
            set_name(HTurb[h, hour, s], "HTurb($h,$hour,$s)")
            set_name(spillage[h, hour, s], "Hspil($h,$hour,$s)")
        end
    end
end

function var_outflow!(m::JuMP.Model, prb::BidDispatchProblem)
    n = prb.n
    d = prb.data
    options = prb.options

    if n.Hydros>0
        # Turbined water (in hm3)
        @variable(m, def_slack[1:n.Hydros, 1:n.Scenarios] >= 0.0)
        for h=1:n.Hydros, s=1:n.Scenarios
            set_name(def_slack[h, s], "outslk($h,$s)")
        end
    end
end
