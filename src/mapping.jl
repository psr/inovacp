#
# Mapping
# -------
function map_problem(prb::BidDispatchProblem)
    load_problem(prb)

    # map trueup
    if prb.options.runmode == TRUEUP_MODE::RunMode
        map_trueup!(prb)
    end

    nothing
end

function map_trueup!(prb::BidDispatchProblem)
    d = prb.data
    quants = prb.data.quantities
    d.tup2hyd = map_vector(strip.(quants["hydro_res_up"].header), strip.(prb.data.hName))
    d.hyd2tup = map_vector(strip.(prb.data.hName), strip.(quants["hydro_res_up"].header))

    d.tup2ter = map_vector(strip.(quants["thermal_res_up"].header), strip.(prb.data.tName) )
    d.ter2tup = map_vector(strip.(prb.data.tName), strip.(quants["thermal_res_up"].header))

end
function map_vector(vecfrom::Vector, vecto::Vector)
    mapping = zeros(Int32, length(vecfrom))
    for (idx_from, name) in enumerate(vecfrom)
        idx_to = findfirst(x -> x == name, vecto)
        if idx_to isa Number
            mapping[idx_from] = idx_to
        end
    end
    return mapping
end

function load_general(PATH_CASE)

    prb = BidDispatchProblem()
    prb.data.logstream = open(joinpath(PATH_CASE, "dispatch.log"), "w")
    prb.options.INPUTPATH = PATH_CASE
    PATH_PSRC = joinpath(@__DIR__, "..", "deps", "psrclassesinterfacejulia")
    prb.options.PSRCPATH = PATH_PSRC
    PATH_IHM = joinpath(@__DIR__, "..", "Interface", "Oper")
    prb.options.IHMPATH = PATH_IHM
    IO_header(prb)
    IO_running_path(prb, PATH_CASE)

    return prb
end
#
# PSRClasses Mapping
#
# -------------------

function psrclasses_mapping!(prb::BidDispatchProblem)

    initialize!(prb)

    options_load!(prb)

    cluster_map!(prb)

    nothing
end

function initialize!(prb::BidDispatchProblem)
    options = prb.options
    d = prb.data
    psrc = prb.psrc

    psrc.study_ptr, psrc.iosddp = PSRCinit(options)
    nothing
end


function cluster_map!(prb::BidDispatchProblem)
    d = prb.data
    n = prb.n

    if "Clusters" in keys(d.quantities) && prb.options.AffineClus
        n.Clusters = min(maximum(d.quantities["Clusters"].value), n.Scenarios)
        n_scenarios = length(d.quantities["Clusters"].value)
        d.clus2scen = [Int[] for i in 1:n.Clusters]
        d.scen2clus = zeros(n_scenarios)

        for (i,v) in enumerate(d.quantities["Clusters"].value)
            d.scen2clus[i] = v
        end

        for s in 1:n.Scenarios
            push!(d.clus2scen[d.scen2clus[s]], s)
        end
    else
        prb.n.Clusters = 1
        d.scen2clus = ones(n.Scenarios)
    end

    nothing
end