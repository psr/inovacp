"""
    Script to set Xpress solver parameters
"""
# import Sys
import MathOptInterface
const MOI = MathOptInterface

function set_solver_params!(OPTIMIZER, prb::BidDispatchProblem)
    # Old way to set params: Xpress.setparam!(backend(model).optimizer.model.inner, Xpress.XPRS_CONTROLS_DICT[:OUTPUTLOG], 1)
    # New way:MOI.set(OPTIMIZER, MOI.RawParameter("logfile"), "output.log")

    # OPTIMIZER = with_optimizer(GLPK.Optimizer)
    # set CPU threads
    set_proc!(OPTIMIZER)

    # set the solution strategy parameters
    solution_strategy!(OPTIMIZER)

    set_callback_frequency!(OPTIMIZER)

    solution_parameters!(OPTIMIZER)

    set_crossover!(OPTIMIZER, prb)

    set_memory!(OPTIMIZER)

    # set_maxtime!(OPTIMIZER, prb)

    nothing
end


"""
    Set the maximum number of threads possible, considering the available RAM and the 2GB RAM per thread proportion.
"""
function get_proc_number()

    # The number of logical CPU cores available in the system
    n_threads = Sys.CPU_THREADS

    # The number of CPUS
    n_cpu = length(Sys.cpu_info())

    # the free RAM in giga bytes
    ram_size = Sys.free_memory() * 1e-9

    # get the maximum threads with 2 GB RAM
    max_threads_ram = round(Int, ram_size / 2, RoundDown)

    # uses maximum number of threads possible.
    procs = min(n_threads, max_threads_ram)

    return procs
end
function set_proc!(OPTIMIZER)
    nproc = get_proc_number()
    @show nproc
    # if (PARL = 0) then
    #     setparam('XPRS_THREADS',minlist(4,NCOR))
    # else
    #     setparam('XPRS_THREADS',NTHR)
    # end-if
    MOI.set(OPTIMIZER, MOI.RawParameter("THREADS"), nproc)

    nothing
end

"""
    Set the solution strategy parameters
"""
function solution_strategy!(OPTIMIZER)

    #
    # setparam('XPRS_PRESOLVE',1)
    MOI.set(OPTIMIZER, MOI.RawParameter("PRESOLVE"),2)

    # setparam('XPRS_CUTSTRATEGY',-1)
    MOI.set(OPTIMIZER, MOI.RawParameter("CUTSTRATEGY"), -1)

    # setparam('XPRS_BARPRESOLVEOPS',2)
    MOI.set(OPTIMIZER, MOI.RawParameter("BARPRESOLVEOPS"), 1)
    # setparam("BARGAPSTOP",0)
    MOI.set(OPTIMIZER, MOI.RawParameter("BARGAPSTOP"), 0.0)

    # setparam('XPRS_HEURTHREADS',0)
    MOI.set(OPTIMIZER, MOI.RawParameter("HEURTHREADS"), 0)
    # command("HEURDIVESPEEDUP=-2")
    MOI.set(OPTIMIZER, MOI.RawParameter("HEURDIVESPEEDUP"), -2)

    #HEURISTICA
    # MOI.set(OPTIMIZER, MOI.RawParameter("HEURSTRATEGY"), 3) #1
    # MOI.set(OPTIMIZER, MOI.RawParameter("HEURSEARCHTREESELECT"), 3) #1

    # PRESOLVe
    # MOI.set(OPTIMIZER, MOI.RawParameter("PREPROBING"), 3)

    # NODE
    # MOI.set(OPTIMIZER, MOI.RawParameter("NODESELECTION"), 3)

    # metodo de solucao LP
    MOI.set(OPTIMIZER, MOI.RawParameter("DEFAULTALG"), 4)

    #
    # MOI.set(OPTIMIZER, MOI.RawParameter("BARORDER"), 0) #
    MOI.set(OPTIMIZER, MOI.RawParameter("MIPPRESOLVE"), 3) #3

    nothing
end

function set_callback_frequency!(OPTIMIZER)

    # callback function, lpLog, to be called every 500 iterations of the optimization
    # setparam('XPRS_LPLOG',500)
    MOI.set(OPTIMIZER, MOI.RawParameter("LPLOG"), 500)
    nothing
end

function solution_parameters!(OPTIMIZER)
    # what it does???
    # setparam("XPRS_CPUPLATFORM",0)
    MOI.set(OPTIMIZER, MOI.RawParameter("CPUPLATFORM"), 0)

    # mip tolerance
    # setparam('XPRS_MIPTOL',1.0E-06)
    MOI.set(OPTIMIZER, MOI.RawParameter("MIPTOL"), 1.0E-06)

    # Default C printing format for real numbers (string, default: "%g")
    # setparam('REALFMT','%.15g')
    # MOI.set(OPTIMIZER, MOI.RawParameter("REALFMT"), "%.15g")

    nothing
end

function set_crossover!(OPTIMIZER, prb)
    options = prb.options
    MOI.set(OPTIMIZER, MOI.RawParameter("CROSSOVER"), 0)
    # if options.crossover
    #     # setparam('XPRS_CROSSOVER',-1)
    #     MOI.set(OPTIMIZER, MOI.RawParameter("CROSSOVER"), -1)
    # else
    #     # setparam('XPRS_CROSSOVER',0)
    #     MOI.set(OPTIMIZER, MOI.RawParameter("CROSSOVER"), 0)
    # end

    nothing
end

function set_memory!(OPTIMIZER)

    # setparam("XPRS_MAXMEMORY",MEMO)

    nothing
end

function set_maxtime!(OPTIMIZER, prb)
    options = prb.options

    # if (D = 0 and NTRY = 1) then
    #     MAXX:= integer(- MAXT * (S - A + 1) / NSTG)
    # else
    #     MAXX:= integer(- MAXT * NUM3 * (S - A + 1) / NSTG)
    #     MIPR:= MIPR * NUM3
    # end-if

    # setparam('XPRS_MAXTIME', MAXX)
    MOI.set(OPTIMIZER, MOI.RawParameter("MAXTIME"), options.maxtime)

    # setparam('XPRS_MIPRELSTOP',MIPR)
    MOI.set(OPTIMIZER, MOI.RawParameter("MIPRELSTOP"), options.mipstop)

    nothing
end