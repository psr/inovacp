using Distributions
import Random

Random.seed!(123) # Setting the seed

function get_dem_scenarions(μ::Float64, cv::Float64, N::Int)
    # μ -+ 5%μ <= 3σ
    σ = cv*μ #0.05/3
    dist = Normal(0, σ)

    scenarios = rand(dist, N)

    return scenarios
end