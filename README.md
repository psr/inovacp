![logo-inovaCP](https://bitbucket.org/psr/inovacp/raw/master/images/logo-inovaCP-600x333.png "Logo InovaCP")

## Modelo de despacho estocástico

# Instalação

Baixar Julia Versão 1.0.5: https://julialang-s3.julialang.org/bin/winnt/x64/1.0/julia-1.0.5-win64.exe

Instalar julia.

Caso opte por uma instalação não padrão adicione o executável "julia.exe" no path do usuário. Desse modo é possível inicial o terminal julia apenas escrevendo "julia" no terminal windows.

Rodar via linha de comando do windows o "install.bat" contido nessa pasta.

Pronto já é possível rodar o programa.

# Utilizando o programa:

## Edição de dados

Para abrir a ferramenta de edição de dados basta clicar no arquivo "interface.bat".

Um caso exemplo é disponibilizado na pasta "example".

## Execução na interface

Basta clicar na aba "Run" e apertar o botão "Run". Os resultados estarão na pasta do caso.

## Execução em linha de comando

Abra a linha de comando windows.

Troque de diretório para a pasta do caso de estudo.

Execute o "run.bat" na linha de comando do windows.

# Modos de execução

### Operação

0. Determinístico: usa a média dos cenários selecionados

1. Affine: usa a metodologia de regras lineares para calcular os arquivos de reserva.

2. True up: testa as decisões para os diferentes cenários

### Reserva

0. Sem reserva

1. Reserva individual: tenta seguir a alocação de reserva dos arquivos Up Down.

2. Cootimização: soma a reserva dos arquivos Up Down e redistribui para todas as usinas.

3. Alocação para o CAG a: usa 5% da carga e distribui entre as usinas do CAG.

4. Alocação para o CAG b: soma a reserva dos arquivos Up Down e redistribui para o CAG apenas, onde as usinas ficam com o mesmo valor absoluto.

5. Alocação para o CAG c: soma a reserva dos arquivos Up Down e redistribui para o CAG apenas, otimizando a alocação.

6. Cootimização por fonte: soma a reserva dos arquivos Up Down e redistribui para as usinas, porém separando o que é reserva termoelétrica e hidroelétrica.

# Solvers

O programa vem com os solvers Cbc e GLPK ja preprados para uso.

Para uso do solver Xpress, além da instalção do solver e licença é necessário remover as linhas começando em
`SET XPRESS_JL...` do arquivo install.bat e rerrodar o arquivo.

Outros solvers como CPLEX, Gurobi e SCIP requerem licença, instalação do solver,
instalação do "wrapper" em julia e mudanças bastante simples no código.

# Mais...

Manuais de metodologia, arquivos e usuário podem ser acessados via interface na aba
"Help".