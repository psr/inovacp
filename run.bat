
@echo off

SET BASEPATH=%~dp0

SET CURRENT_PATH=%CD%

SET PATH="%PATH%;%USERPROFILE%\AppData\Local\Julia-1.0.5\bin"
SET PATH=%PATH:"=%

SET juliafile="%BASEPATH%src\test.jl"

SET XPRESS_JL_NO_AUTO_INIT=0

SET CASE_PATH="%1"

IF %CASE_PATH% == "" GOTO skip_cd
cd /d %CASE_PATH%
:skip_cd

julia.exe --project=%BASEPATH% %juliafile%

cd /d %CURRENT_PATH%