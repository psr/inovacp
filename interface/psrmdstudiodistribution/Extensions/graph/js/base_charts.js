var customColors = ["#4e79a7", "#a0cbe8", "#f28e2b", "#ffbe7d", "#59A14F", "#8cd17d", "#b6992d", "#f1ce63", "#499894", "#86bcb6", "#E15759", "#ff9d9a", "#79706e", "#BAB0AB", "#d37295", "#fabfd2", "#b07aa1", "#d4a6c8", "#9d7660", "#d7b5a6"];

		function visibilityTraces(listTraces) {
		  listTraces = listTraces || [];
		  if (listTraces.length > 30) {
			for (i = 0; i < listTraces.length; i++) {
				var trace = listTraces[i];
				trace.visible = (trace.y.reduce((a, b) => a + b, 0) != 0);
				//if (!trace.visible) trace.visible = 'legendonly';
			}
		  }
		  if (listTraces.length == 1)
			  listTraces[0].showlegend = true;
		}
	
        function plotChart(idDiv, dataListGraph, config) {
			if (!dataListGraph.dados || dataListGraph.dados.length == 0) {
				return;
			}
			config = $.extend({}, { chartType: "LINE", stage: 0 }, config || {});
			$("#" + idDiv).html("<svg></svg>");
            var data_plotly = []
			
			var x_list = []; var x_type = null;
			var x_listData = dataListGraph.etapasHorarias || dataListGraph.legendas;
			if (x_listData.length == 1) {
				x_list = x_listData;
				x_type = 'category';
			} else {
				x_list = convertToDate(x_listData);
			}
            
			for (i = 0; i < dataListGraph.dados.length; i++) {
                var trace = {
                    y: dataListGraph.dados[i].values,
                    x: x_list,
                    name: dataListGraph.dados[i].key,
                };
				switch (config.chartType) {
					case "LINE": 
						trace.type = 'scatter';
						trace.line = { dash: "solid", color: customColors[i], width: 1.5 };
						break;
					case "BAR": 
					case "BARSTACKED": 
						trace.type = 'bar';
						trace.marker = { color: hexToRgbA(customColors[i], 0.7) };
						break;
					case "BARH": 
					case "BARSTACKEDH": 
						var _tmp = trace.y;
						trace.y = trace.x;
						trace.x = _tmp;					
						trace.type = 'bar';
						trace.orientation = 'h';
						trace.marker = { color: hexToRgbA(customColors[i], 0.7), width: 1};
						break;
					case "WATERFALL": //need adjusts
						trace.type = 'waterfall';
						trace.marker = { color: hexToRgbA(customColors[i], 0.7) };
						break;
					case "BOXPLOT": //need adjusts
						trace.type = 'box';
						trace.marker = { color: hexToRgbA(customColors[i], 0.7) };
						break;
					case "AREA":
						trace.hoverinfo = 'x+y';
						trace.mode = 'lines';
						trace.stackgroup = 'one';
						trace.line = { dash: "solid", color: hexToRgbA(customColors[i], 0.7), width: 0.5};
						break;
					default: //LINE - DEFAULT
						trace.type = 'scatter';
						trace.line = { dash: "solid", color: customColors[i], width: 1.5 };
						break;
				}
				data_plotly.push(trace);
            }
			
			if (config.chartType == "DONUT" || config.chartType == "PIE")
			{
				var _values = dataListGraph.dados.map(function(d) { return d.values[config.stage]; });
				var _names = dataListGraph.dados.map(function(d) { return d.key; });

				var trace = {};
				trace.type = 'pie';
				trace.marker = { colors: customColors };
				trace.values = _values;
				trace.labels = _names;
				trace.textinfo = "label+percent";
				trace.textposition = "outside";
				trace.automargin = true;
				if (config.chartType == "DONUT")
				{
					 trace.hole = 0.6 
				}					

				data_plotly = [trace]; //set single trace
			}
			
			if (config.chartType == "HEATMAP" || config.chartType == "SURFACE")
			{
				for (i = 0; i < data_plotly.length; i++) {
					var trace = data_plotly[i];
					trace.marker = { color: hexToRgbA(customColors[i], 0.7) };
					var list = [];
					for (var j = 0; j < trace.x.length; j++) {
						list.push({
							x: moment(trace.x[j]).startOf('day')._d,
							y: (trace.x[j]).getMinutes() == 0 ? (trace.x[j]).getHours() + 'h' : (trace.x[j]).getHours() + ':' + (trace.x[j]).getMinutes(),
							value: trace.y[j],
						});
					}
					var groups = groupBy(list, "y");
					trace.y = Object.keys(groups);
					trace.x = groups[trace.y[0]].map(function(g) { return g.x; });
					trace.z = [];
					for (var j = 0; j < trace.y.length; j++) {
						trace.z.push(groups[trace.y[j]].map(function(g) { return g.value }));
					}
					trace.type = config.chartType == "SURFACE" ? "surface" : "heatmap";
					trace.showlegend = true;
					trace.opacity = 1;
					trace.hovertemplate = 'Day: %{x}<br>Hour: %{y}<br>Value: %{z}';
				}
			}
			
			visibilityTraces(data_plotly);
			
			//unit
			if (dataListGraph.unidade == "") {
				var currentUnit = dataListGraph.unidades[0];
				var countUnit = 1;
				for (i = 0; i < dataListGraph.dados.length; i++) {
					if (dataListGraph.unidades[i] != currentUnit) {
						countUnit++;
						currentUnit = dataListGraph.unidades[i];
						dataListGraph.unidade2 = currentUnit;
					}
					if (countUnit > 1) {
						data_plotly[i].yaxis = 'y' + countUnit;
					}
				}
				dataListGraph.unidade = dataListGraph.unidades[0];
			}
			
            var layout = {
				hovermode: (data_plotly.length > 20) ? 'closest' : 'x', //tooltip group for less itens
                xaxis: {
                    showgrid: true,
				},
                margin: {
                    l: 50, b: 35, r: 35, t: 30, pad: 0,
                    autoexpand: true
                },
				scene: {
					aspectmode: 'manual',
					xaxis: { titlefont: { size: 12 } },
					yaxis: { titlefont: { size: 12 } },
					zaxis: { titlefont: { size: 12 } },
				},
				yaxis: {
					title: { text: dataListGraph.unidade },
					titlefont: { size: 12 },
				},
				yaxis2: { 
					title: { text: dataListGraph.unidade2 },
					titlefont: { size: 12 },
					side: 'right',
					overlaying: 'y',
					//rangemode: 'tozero',
				},
				legend: {
					orientation: "v",
					x: 1.1,
					xanchor: 'left',
					y: 1
				}
            };

			if (x_list.length == 1) {
				layout.xaxis.type = x_type;
			}
			if (graf.pageSettings.darkMode) {
				layout.xaxis = layout.xaxis || {};
				layout.xaxis.gridcolor = '#83878f';

				layout.yaxis = layout.yaxis || {};
				layout.yaxis.gridcolor = '#83878f';
				
				layout.yaxis2 = layout.yaxis2 || {};
				layout.yaxis2.gridcolor = '#83878f';
				
				layout.scene.xaxis = layout.scene.xaxis || {};
				layout.scene.xaxis.gridcolor = '#83878f';
				layout.scene.yaxis = layout.scene.yaxis || {};
				layout.scene.yaxis.gridcolor = '#83878f';
				layout.scene.zaxis = layout.scene.zaxis || {};
				layout.scene.zaxis.gridcolor = '#83878f';
				
				layout.font = layout.font || {};
				layout.font.color = '#dee2e6';
				
				layout.plot_bgcolor = '#3b434e';
				layout.paper_bgcolor = '#3b434e';
				
			}
			
			if (config.chartType == "BARSTACKED" || config.chartType == "BARSTACKEDH")
			{
				layout.barmode = 'stack';
			}
			if (config.chartType == "BARH" || config.chartType == "BARSTACKEDH")
			{
				layout.margin.l = 80;
				layout.xaxis.title = layout.yaxis.title;
				layout.xaxis.titlefont = layout.yaxis.titlefont;
				layout.xaxis.type = null;
				layout.yaxis = { type: x_type };
				layout.hovermode = (data_plotly.length > 20) ? 'closest' : 'y';
			}
			if (config.chartType == "HEATMAP" || config.chartType == "SURFACE")
			{
				layout.legend = {};
				layout.showlegend = false;
				layout.scene.xaxis.title = 'Day';
				layout.scene.yaxis.title = 'Hour';
				layout.scene.zaxis.title = 'Value';
				
				if (config.chartType == "HEATMAP")
				{
					if (data_plotly[0].y.length == 24) layout.yaxis.dtick = 3;
					if (data_plotly[0].y.length == 48) layout.yaxis.dtick = 6;
					if (data_plotly[0].y.length == 96) layout.yaxis.dtick = 12;
				}
				if (config.chartType == "SURFACE")
				{
					layout.margin.b = 5;
					layout.margin.t = 5;
				}
			}
            Plotly.newPlot(idDiv, data_plotly, layout, { showLink: false, displayModeBar: false, displaylogo: false, doubleClickDelay: 750 });
            window.addEventListener('resize', function () {
				if ($('#'+idDiv).length > 0) {
					Plotly.Plots.resize($('#'+idDiv)[0]);
				}
            });
			//change mouse grab to grab yaxis
			$('#'+idDiv + ' .nsdrag.drag.cursor-ns-resize').css({ cursor: 'grab' });
        };
	
		function tryGetFormat(LabelListGraph) {
			LabelListGraph = LabelListGraph || [""];
			
			var formats = ["DD/MM/YYYY HH:mm", "YYYY-MM-DD HH:mm", "YYYY-MM-DD", "YYYY/MM", "YYYY-MM", "MM/YYYY", "YYYY/WW", "YYYY-WW", "WW/YYYY", "YYYY", "X"];
			var format = "";
			
			if ((LabelListGraph[0] + "").trim().length < 4) return format;
			for (var i = 0; i < formats.length; i++) {
				var bValid = true; var format = formats[i];
				for (var j = 0; j < LabelListGraph.length; j++) {
					bValid = bValid && moment(LabelListGraph[j], format, true).isValid()
				}
				if (bValid) {
					return format;
				}
			}
			return "";
		}
		function convertToDate(LabelListGraph) {
			var format = tryGetFormat(LabelListGraph);
			if (format != "")
			{
				var arr = [];
				for (var i = 0; i < LabelListGraph.length; i++)
					arr.push(moment(LabelListGraph[i], format)._d);
				return arr;
			} else {
				return LabelListGraph; // sem formato identificado
			}
		}

		function getAggregatePlot (pjson) {
			var aggPlot = {};

			aggPlot.avid = pjson.avid;
			aggPlot.titulo = pjson.title;
			aggPlot.unidades = [];
			aggPlot.dados = [];
			aggPlot.unidade = "";

			var firstLayer = pjson.results[0];
			if (!firstLayer) return null;
			aggPlot.legendas = firstLayer.datetime;
			if (firstLayer.frequency == "h")
				aggPlot.etapasHorarias = firstLayer.datetime;
			
			(pjson.results || []).forEach(function(d) {
				(d.layer || []).forEach(function(dl) {
					aggPlot.unidades.push(d.y_unit);
					aggPlot.dados.push({ values: dl.data, key: dl.label });
				});
			});
			if (aggPlot.unidades.length == 1)
				aggPlot.unidade = aggPlot.unidades[0];
			
			return aggPlot;
		}

		function hexToRgbA(hex, opacity){
			var c; opacity = opacity || 1;
			if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
				c= hex.substring(1).split('');
				if(c.length== 3){
					c= [c[0], c[0], c[1], c[1], c[2], c[2]];
				}
				c= '0x'+c.join('');
				return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+','+opacity+')';
			}
			return hex;
		}
		function groupBy(objectArray, property) {
			return objectArray.reduce(function (acc, obj) {
				let key = obj[property];
				if (!acc[key]) {
					acc[key] = []
				}
				acc[key].push(obj);
				return acc;
			}, {});
		}