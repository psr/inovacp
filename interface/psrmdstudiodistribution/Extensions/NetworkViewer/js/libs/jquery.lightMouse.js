(function($){
	
	$.fn.lightMouse = function(pOptions){
		
		// valeurs par défauts
		var defaults = {
			color:'#fff',
			width:16,
			height:16
		};
		var options = $.extend(defaults,pOptions);
		
		return this.each(function(){
			
			// this = that
			var that = $(this);
			
			// variables
			var posX, posY, boule, timerCreeBoule, overThis;
			
			// écoute mouvement souris
			$(this).on('mousemove',function(e){
				posX = e.pageX;
				posY = e.pageY;
				boule.css({
					'top':posY + 'px',
					'left':posX + 'px'
				});
			});
			
			// la boule est crée si dessus
			$(this).on('mouseover',function(){ overThis = true; boule.show(); });
			$(this).on('mouseout',function(){ overThis = false; boule.hide(); });
			
			boule = $('<div></div>');
			boule.css({
				'position':'absolute',
				'width':options.width,
				'height':options.height,
				'border-radius':'50%',
				'background':options.color,
				//'box-shadow':'0 0 10px' + options.color +', 0 0 10px' + options.color + ', 0 0 10px'  + options.color + ', 0 0 10px'  + options.color,
				'top':0 + 'px',
				'left':0 + 'px',
				'margin-left': (options.width / 2) * (-1) + 'px',
				'margin-top': (options.height / 2) * (-1) + 'px',
				'pointer-events': 'none'
			});
			boule.appendTo(that);
			
			// création boule
			//timerCreeBoule = setInterval(creeBoule,10);
			function creeBoule(){
				if (overThis){
					boule.css({
						'position':'absolute',
						'width':options.width,
						'height':options.height,
						'border-radius':'50%',
						'background':options.color,
						//'box-shadow':'0 0 10px' + options.color +', 0 0 10px' + options.color + ', 0 0 10px'  + options.color + ', 0 0 10px'  + options.color,
						'top':posY + 'px',
						'left':posX + 'px',
						'margin-left': (options.width / 2) * (-1) + 'px',
						'margin-top': (options.height / 2) * (-1) + 'px',
						'pointer-events': 'none'
					});
					boule.show();
				} else {
					boule.hide();
				}
			}
			
		});
		
	};
	
})(jQuery);