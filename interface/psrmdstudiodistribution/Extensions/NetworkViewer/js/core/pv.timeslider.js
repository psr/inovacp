﻿	pv.timeslider = pv.timeslider || {};
	pv.timeslider.isRunning = false;
	pv.timeslider.loop = true;
	pv.timeslider.maxValue = 262800 / 30 / 365;
		
	$('#btnSlider').on('click', function (e) {
		$(this).toggleClass('stop');
		pv.timeslider.isRunning = !pv.timeslider.isRunning;
	});
	$('#btnSliderLoop').on('click', function (e) {
		$(this).toggleClass('loop');
		pv.timeslider.loop = !pv.timeslider.loop;
	});

	var setStages = function(stages) { 
		$.notify({ message: 'setStages is disabled. Please use setCaseInfo({stagesStudy:[]...});', status: "warning", timeout: 5000});
	};
	var _setStages = function(stages) {
		var currentStateFlagIsRunning = pv.timeslider.isRunning;
		if (stages.length == 0) return false;
		pv.stages = stages;
		$("#lbTime").text(stages[0]);
		$('#pv-timeslider').fadeIn();

		pv.timeslider.maxValue = stages.length;
		var customSlider = $('#ex1').bootstrapSlider({
			formatter: function(value) {
				return 'Current value: ' + value;
			},
			tooltip: 'hide',
			max: pv.timeslider.maxValue,
			min: 1,
			value: 1
		});
		$("#ex1").change(function(slideEvt) {
			//var strDate = moment("2017-01-01T00:00:00").add(slideEvt.value, 'hours').format("YYYY-MM-DD HH:mm");
			slideEvt.value = customSlider.bootstrapSlider('getValue');
			pv.currentIndexStage = slideEvt.value - 1;
			$("#lbTime").text(stages[pv.currentIndexStage]);
			$(document).trigger("changeStage", slideEvt.value);
		});
		
		if (pv.timeslider.intervalFunc) clearInterval(pv.timeslider.intervalFunc);
		
		pv.timeslider.intervalFunc = setInterval(function() {
			if (pv.timeslider.isRunning) {
				var currentValue = customSlider.bootstrapSlider('getValue') + Math.max(1, (pv.timeslider.maxValue * 0.0005));
				if (currentValue > pv.timeslider.maxValue) { 
					if (pv.timeslider.loop) currentValue = 1; 
					else $('#btnSlider').click();
				}
				customSlider.bootstrapSlider('setValue', currentValue, true);
				$("#ex1").change();
			}
		}, 800);
		
		function updateSizeSlider(){
			$(".sliderDiv .slider").css("width", (window.innerWidth - 332) + "px");
		}
		updateSizeSlider();
		
		$(window).on("resize", function() {
		  updateSizeSlider();
		});
		
		//startStage1
		customSlider.bootstrapSlider('setValue', 1);
		customSlider.change();
		pv.timeslider.isRunning = currentStateFlagIsRunning;
	}

	//$(document).on('changeStage', function(e, stage) {
	//	if (typeof callbackObj !== 'undefined') callbackObj.changeStage(stage);
	//});
