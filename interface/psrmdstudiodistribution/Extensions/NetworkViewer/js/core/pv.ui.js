	//Tableau20
	var colorsTableau20 = ["#4e79a7", "#a0cbe8", "#f28e2b", "#ffbe7d", "#59A14F","#8cd17d","#b6992d","#f1ce63", "#499894","#86bcb6","#E15759","#ff9d9a", "#79706e","#BAB0AB","#d37295","#fabfd2", "#b07aa1","#d4a6c8","#9d7660","#d7b5a6"];
	//Pastel2
	var colorsPastel2 = ["#B3E2CD", "#FDCDAC", "#CBD5E8", "#F4CAE4", "#E6F5C9", "#FFF2AE", "#F1E2CC", "#CCCCCC"];
	//Tableau10
	var colorsTableau10 = ["#4E79A7", "#F28E2C", "#E15759", "#76B7B2", "#59A14F", "#EDC949", "#AF7AA1", "#FF9DA7", "#9C755F", "#BAB0AB"];
	//Custom
	var colorsCustom = ["#66c2a5","#fc8d62","#8da0cb","#e78ac3","#a6d854","#ffd92f","#e5c494"];
	
	var colorslist = [colorsTableau20];

	$(window).on('orientationchange', function() {
		setTimeout(function() {
			   window.dispatchEvent(new Event('resize'));
			 }, 200); //force update after rotation mobile change
	});

	map.once('postrender', function() {
		
		////LOAD CASE
		//updateTableResultsSystem('system', sampleResults.SystemConfiguration.variables);
		//updateTableResultsSystem('buses', sampleResults.BusConfiguration.variables);
		//updateTableResultsSystem('circuits', sampleResults.CircuitConfiguration.variables);
		
		//changeLabelDefaultMarker('name');
		//plotMarkes(sampleMarkers); updateSourceSearchBus();
		//plotLines(sampleLines);
		
		//getVoronoi(sampleMarkers);
		
		//setStages(pv.stages.values);
		//setTimeout(function() { fitZoom(); }, 1500);
		
	});
	
	
	var defaultUserSettings = [
		{"idResult":"cirflw","selected":true},
		{"idResult":"dclink","selected":true}
	];
	
	$('#styleShape').selectpicker({ iconBase: '', tickIcon: 'fa-check'});
	pv.userSettings = pv.userSettings || {};
	pv.userSettings.resultsPreferences = pv.userSettings.resultsPreferences || defaultUserSettings || [];
	var getResultPreferences = function(idResult = 'default') {
		var pref = pv.userSettings.resultsPreferences.filter(function (pref) { return (pref.idResult == idResult) })[0];
		if (pref === undefined) {
			pref = {
				idResult: idResult,
				shapeType: 1, 
				color: '#4e79a7', 
				radiusFactor: 50, 
				styleShape: 3
			};
			pv.userSettings.resultsPreferences.push(pref);
		}
		return pref;
	}
	
	var loadUserSettings = function() {
		updateArrayLayerGeometry(pv.userSettings.resultsPreferences.filter(function(pref) { return pref.selected } ));
	}
	
	//setTimeout(function() {
	//	loadUserSettings()
	//}, 1000);
	
	var getVoronoi = function (sampleMarkers) {
		var ignore_Bolivia = [5, 32, 33, 51, 55, 68, 40];
		var bounds = sourceFeatures.getExtent();
		var extent = ol.proj.transformExtent(bounds, 'EPSG:3857', 'EPSG:4326');
		var options = {
			bbox: extent
		};
		var listPoint = [];
		
		sampleMarkers.forEach(function(m) {
			if (ignore_Bolivia.indexOf(m.code) == -1)
				listPoint.push(turf.point([m.longitude, m.latitude], m));
		});
		
		var features = turf.featureCollection(listPoint);
		var voronoiPolygons = turf.voronoi(features, options);
		
		// //Pega os repetidos
		// var repeated = [];
		// for (i = 0; i < voronoiPolygons.features.length; i++) {
		// 	if (voronoiPolygons.features[i] == null) 
		// 		repeated.push(i);
		// };
		// repeated.forEach(function(i) { bus.push(sampleMarkers[i]) });

		var layer = new ol.layer.Vector({
				source: new ol.source.Vector({
					features: new ol.format.GeoJSON().readFeatures(voronoiPolygons, { featureProjection: 'EPSG:3857' })
				}),
				style: new ol.style.Style({ stroke: new ol.style.Stroke({ color: '#ccc', width: 1 }),
											fill: new ol.style.Fill({ color: 'rgba(0, 0, 0, 0)' }) })
			});
		map.addLayer(layer);
			
		return voronoiPolygons;
	}
	
	//STYLES LAYERS 
	colorslist.forEach(function(colors) {
		$.each(colors, function (i, item) {
			$('#colorselector').append($('<option>', {
				value: item,
				text: item,
				'data-color': item
			}));
		});
	});
	$('#colorselector').colorselector();
		
	$("#scaleSlider").bootstrapSlider( {  tooltip_position:'bottom'  } );
	$(".scaleSlider .slider").css("width", "100%");
	
	//Events Style
	var flag_inLoadStyleValues = false;
	var updateStylesEvent = function () {
		if (!flag_inLoadStyleValues) {
			var currentShapeType = $('#shapeType').val();
			var currentColor = $('#colorselector').val();
			var currentStyleShape = $('#styleShape').val();
			var currentRadius = $("#scaleSlider").val();

			var resultSeleted = $("#styleBox-results-buses").data('idResult');
			var userPref = getResultPreferences( resultSeleted );
			userPref.shapeType = currentShapeType;
			userPref.color = currentColor;
			userPref.styleShape = currentStyleShape;
			userPref.radiusFactor = currentRadius;
			
			updateLayerGeometryStyles(userPref);
		}
	}
	var setDefaultsStyleValues = function (options) {
		flag_inLoadStyleValues = true;
		$('#shapeType').val(options.shapeType || 1);
		$('#colorselector').colorselector("setColor", options.color || '#4e79a7');
		$('#styleShape').selectpicker('val', options.styleShape || 3);
		$("#scaleSlider").bootstrapSlider('setValue', options.radiusFactor || 50);
		flag_inLoadStyleValues = false;
	}
	setDefaultsStyleValues({shapeType: 1, color: '#4e79a7', radiusFactor: 50, styleShape: 3 });
	
	$("#scaleSlider").change(updateStylesEvent);
	$('#colorselector').change(updateStylesEvent);
	$('#shapeType').change(updateStylesEvent);
	$('#styleShape').change(updateStylesEvent);

    $("#styleBox-results-buses")
	   .css({"pointer-events" : "none" , "opacity" :  "0.4", "background" :  "#e8e8e8"})
	   .attr("tabindex" , "-1");
	
	
	// // getting result data - Beta
	// var asyncFunction = function() {
	// 	var dfd = jQuery.Deferred();
	// 	setTimeout(function() { 
	// 		dfd.resolve( { testing: 123 } );
	// 	}, 5000);
	// 	return dfd.promise();
	// };
	// 	
	// $.when(asyncFunction()).done(function( x ) {
	// 	alert( x.testing );
	// });	
	
	
	//adding event click table
	var addHandleEventClickTableResults = function(idTbody) {
		$('#' + idTbody + ' > tr').on('click', function(e) {
			$('#' + idTbody + ' .info').removeClass("info");
			$tr = $(e.currentTarget);
			$tr.addClass("info");
			
			var $checkbox       = jQuery('input:checkbox', $tr);
			var $checkedStatus  = $checkbox.prop('checked');
			
			var userPref = getResultPreferences( $checkbox.prop('name') );
			userPref.selected = $checkedStatus;

		    if ( (pv.getTypeResult(userPref.idResult) == 0) && userPref.selected) {
				$.notify({ message: 'This seleted result not loaded yet, please Reload data.', status: "warning", timeout: 5000});
			}

			// only buses
		    if ($checkbox.data('type') == 'buses') {
				updateLayerGeometry(userPref);
				if ($checkedStatus) { //enable`
					$("#styleBox-results-buses")
					 .css({"pointer-events" : "initial" , "opacity" :  "initial", "background" :  "initial"})
					 .attr("tabindex" , undefined)
					 .data('idResult', $checkbox.prop('name') );
					 setDefaultsStyleValues( userPref );
				} else {  //disable
					$("#styleBox-results-buses")
					 .css({"pointer-events" : "none" , "opacity" :  "0.4", "background" :  "#e8e8e8"})
					 .attr("tabindex" , "-1");
					 setDefaultsStyleValues({shapeType: 1, colorselector: '#4e79a7', radiusFactor: 50, styleShape: 3 });
				}
			}
			if (($checkbox.data('type') == 'circuits') || ($checkbox.data('type') == 'buses') ||
				($checkbox.data('type') == 'dclink') ) {
				pv.createResultFeatures(); //update result painel
			}
			if ($checkbox.data('type') == 'system') {
				pv.processCharts();
			}
		});
	};
	
	
	//default options
	pv.userSettings = pv.userSettings || {}; 
	pv.userSettings.options = pv.userSettings.options || {}; 
	pv.userSettings.options.optAnimationFlow = true;
	pv.userSettings.options.optDisplayResultPanel = true;
	pv.userSettings.options.optConvertGWhToMW = true;
	pv.userSettings.options.optMouseSelectResults = 'M';
	pv.userSettings.options.optColorPreferences = '3';
	
	//Options form
	pv.functions.options.events.on('changed:optAnimationFlow', function(e, v) { //Capture events before create options
		if (v) startAnimation();
		 else  stopAnimation();
	});
	pv.functions.options.events.on('changed:optDarkMode', function(e, v) { //Capture events before create options
		pv.darkmode = v;
		if (pv.darkmode) {
			$('#page-container').addClass('darkMode');
		} else {
			$('#page-container').removeClass('darkMode');
		}
	});
	pv.functions.options.events.on('changed:optDisplayCapture', function(e, v) {
		if (v) $('.pv-capture').show();
		 else  $('.pv-capture').hide();
    });
	pv.functions.options.events.on('changed:optDisplayResultPanel', function(e, v) { //Capture events before create options
		if (v) $('#blockResultPanel').show();
		 else  $('#blockResultPanel').hide();
	});
	$('#closeBlockResultPanel').click(function() {
		pv.functions.options.updateValue('optDisplayResultPanel', pv.userSettings.options, !pv.userSettings.options.optDisplayResultPanel);
	});	
	
	pv.functions.options.createOptions('optMouseSelectResults', 'How select lines in results', [
		{ id: 'M', text: 'Mouse hover' }, 
		{ id: 'C', text: 'Single click' }
	], '#formOptions', pv.userSettings.options);
	pv.functions.options.createFlag('optAnimationFlow',       'Show flow animation', 'Display animation path',        '#formOptions', pv.userSettings.options);
	pv.functions.options.createFlag('optDisplayCapture',      'Show Video Recorder',   'Display Video Recorder',      '#formOptions', pv.userSettings.options);
	pv.functions.options.createFlag('optDisplayResultPanel',  'Show Result Panel',   'Results of circuits and buses', '#formOptions', pv.userSettings.options);
	pv.functions.options.createFlag('optIgnoreNullValues',    'Ignore null values',  'Filter on Result Panel',        '#formOptions', pv.userSettings.options);
	pv.functions.options.createFlag('optConvertGWhToMW',      'Convert GWh to MW',  'Convert values GWh to MW',       '#formOptions', pv.userSettings.options);
	pv.functions.options.createOptions('optColorPreferences', 'Color Preferences', [
		{ id: '1', text: 'None' }, 
		{ id: '2', text: 'Circuit Flow' },
		{ id: '3', text: 'Volt' }
	], '#formOptions', pv.userSettings.options);
	pv.functions.options.createFlag('optDarkMode',    'Dark Mode',    'Set Dark Mode appearance',    '#formOptions', pv.userSettings.options);
	//
	
	$('.basemaps a').click(function(e) { 
		e.preventDefault();
		changeTile($(this).data('tile'));
		$('.basemaps a i').remove();
		$(this).append('<i class="fa fa-check text-success"></i>');
		
	});
	
	
	//button to expand menu
	var button = document.createElement('button');
	button.innerHTML = '<i class="fa fa-tasks"></i>';
	button.setAttribute("data-action", "side_overlay_toggle");
	button.setAttribute("data-toggle", "layout");
	var element = document.createElement('div');
	element.className = 'tools-button toggle-button ol-unselectable ol-control';
	element.appendChild(button);
	var toggleControl = new ol.control.Control({
		element: element
	});
	map.addControl(toggleControl);
	
	
	//drag box
	 $( "#draggable" ).draggable({ cancel: '.block-content.infobox' });
	 var updateInfoBoxHeight = function (height)
	 {
		$( ".block-content.infobox" ).slimScroll({
			height: height,
			alwaysVisible: true,
			size: '5px',
			opacity : .35,
			wheelStep : 15,
			distance : '2px',
			railVisible: false,
			railColor: '#999',
			railOpacity: 0.3
		});
	 }
	 updateInfoBoxHeight(200);
	 $( "#draggable" ).show();
	 $( "#draggable" ).resizable({
		maxWidth: 320,
		minWidth: 300,
		minHeight: 200,
		resize: function( event, ui ) {
			updateInfoBoxHeight(ui.size.height -38);
			$('#draggable').css({height: 'initial'});
		}
	 });
	
	//RESULTS PANEL
	var resultTableSetEvents = function () {
		jQuery('.js-table-sections').each(function(){
			var $table = jQuery(this);
			// When a row is clicked in tbody.js-table-sections-header
			jQuery('.js-table-sections-header > tr', $table).on('click', function(e) {
				var $row    = jQuery(this);
				var $tbody  = $row.parent('tbody');
				//if (! $tbody.hasClass('open')) {
				//	jQuery('tbody', $table).removeClass('open');
				//}
				$tbody.toggleClass('open');
			});
		});
	}
	resultTableSetEvents();
	var scrollBus = $( ".table-results" ).slimScroll({
		height: 180,
		alwaysVisible: true,
		size: '3px',
		opacity : .35,
		wheelStep : 25,
		distance : '1px',
		railVisible: true,
		railColor: '#999',
		railOpacity: 0.3
	});
	//RESULTS PANEL
	
	
	//BUS SEARCH
	var buttonSearch = document.createElement('button');
	buttonSearch.innerHTML = '<i class="fa fa-search"></i>';
	buttonSearch.setAttribute("onclick", "$('#search-bus-modal').modal('show')");
	var elementSearch = document.createElement('div');
	elementSearch.className = 'tools-button search-bus-button ol-unselectable ol-control';
	elementSearch.appendChild(buttonSearch);
	var searchControl = new ol.control.Control({
		element: elementSearch
	});
	map.addControl(searchControl);
	
	var sourceSearchBus = [];
	var updateSourceSearchBus = function () {
		sourceSearchBus = [];
		var vetFeatures = layerFeatures.getSource().getFeatures();
		for (i = 0; i < vetFeatures.length; i++) {
			var item = vetFeatures[i].getProperties().properties;
			item.name = item.name.trim();
			sourceSearchBus.push(item);
		}
	}
	var disableSearchBusButton = function () 
	{
		$('#search-bus-goto').removeClass('btn-primary').addClass('btn-default').prop("disabled",true);
		$('#search-bus-goto').data('obj', null);
	}

	var enableSearchBusButton = function (idsource) 
	{
		$('#search-bus-goto').removeClass('btn-default').addClass('btn-primary').prop("disabled",false);
		$('#search-bus-goto').data('obj', sourceSearchBus[idsource]);
	}
	$('#search-bus-goto').click(function() {
		$('#search-bus-modal').modal('hide');
		var obj = $('#search-bus-goto').data('obj');
		$('#search-bus-input').val('');
		disableSearchBusButton();
		if (obj)
		{
			var vetPoint = ol.proj.transform([ obj.longitude, obj.latitude], 'EPSG:4326', 'EPSG:3857');
			map.getView().animate({center: vetPoint, zoom: 9, duration: 1000});
		}
	});
	
	jQuery('#search-bus-input').autoComplete({
		minChars: 1,
		cache: false,
		suggestionHeight: 10,
		source: function(term, suggest){
			disableSearchBusButton();
			term = term.toLowerCase();
			var suggestions = [];
			for (i = 0; i < sourceSearchBus.length; i++) {
				var item = sourceSearchBus[i];
				if ( (item.name.toLowerCase().indexOf(term) != -1) || (item.code.toString().indexOf(term) != -1)) {
					suggestions.push(item);
				}
			}
			suggest(suggestions);
		},
		renderItem: function (item, search){
			search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
			var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
			return '<div class="autocomplete-suggestion" data-val="'+item.name + ' [' + item.code + ']' +'" data-idsource="'+sourceSearchBus.indexOf(item)+'">' + 
			item.name.replace(re, "<b>$1</b>") +
			'<span style="float: right;">' + item.code.toString().replace(re, "<b>$1</b>") + '</span>' +
			'</div>';
		},
		onSelect: function(e, term, item){
			enableSearchBusButton(item.data('idsource'));
		}
		
	});
	//BUS SEARCH


    //Application labels
	var updateLabels = function (objTranslate) {
		objTranslate = objTranslate || {};
		$('[data-translate-id]').each(function () {
			var valueKey = $(this).data('translate-id');
			if (valueKey != "") {
				var valueTranslate = objTranslate[valueKey];
				if (valueTranslate) {
					$(this).html(valueTranslate);
				}
			}
		});
		$('[data-translate-placeholder]').each(function () {
			var valueKey = $(this).data('translate-placeholder');
			if (valueKey != "") {
				var valueTranslate = objTranslate[valueKey];
				if (valueTranslate) {
					$(this).attr("placeholder", valueTranslate);
				}
			}
		});
	}
    //Application labels
	
	// Results
	var updateTableResultsSystem = function (type, colletion) {
		var st = '';
		colletion.forEach(function (properties) {
			var userPref = getResultPreferences(properties.fileName);
			st = st + "<tr>" +
				"<td style=\"width: 30px;\"><label class=\"css-input css-checkbox css-checkbox-sm css-checkbox-primary\">" +
				"<input type=\"checkbox\" " + (userPref.selected ? "checked=\"checked\"" : "") +
				" data-type=\"" + type + "\"" +
				" id=\"table-results-check-" + type + "\" name=\"" + properties.fileName + "\" >" +
				"<span></span></label></td>" +
				"<td title=\"" + properties.name.trim() + "\">" + properties.name.trim() + "</td>" +
				"<td style=\"width: 70px;\">" + properties.unit.trim() + "</td>" + "</tr>";
		});
		$('#table-results-' + type).html(st);
		addHandleEventClickTableResults('table-results-' + type);
	};
	
	//DataSet
	$(document).on('keyup', 'input[type="number"]', function () {
		var _this = $(this);
		var min = parseInt(_this.attr('min')) || 1; // if min attribute is not defined, 1 is default
		var max = parseInt(_this.attr('max')) || 100; // if max attribute is not defined, 100 is default
		var val = parseInt(_this.val()) || (min - 1); // if input char is not a number the value will be (min - 1) so first condition will be true
		if(val < min)
			_this.val( min );
		if(val > max)
			_this.val( max );
	});
	
	//Util
	var loadScript = function(uri) {
		var script = document.createElement('script');
		script.src = uri;
		document.head.appendChild(script); 
	}
	var loadStyle = function(uri) {
		var linkS = document.createElement('link');
		linkS.rel = "stylesheet"
		linkS.href = uri;
		document.head.appendChild(linkS); 
	}
	//Util
	 
	//hard
	var shiftPressed = false;
	var getPrefixURL = function() { return pv.caseInfo.prefixURL || ""; }
	$(document).keydown(function(event) { shiftPressed = event.keyCode==16; });
	$(document).keyup(function(event) { if( event.keyCode==16 ){ shiftPressed = false; } });
	$('.hardinvoker').on('dblclick', function(e){ if( shiftPressed ){ hard(); hard = _restore; } });
	var _restore = function() { 
		$('#gameHard').html('<video id="gameoffV" autoplay="autoplay" width="100%" height="107%" muted="muted"> <source src="' + getPrefixURL() + 'hard/assets/gameoff.webm" type="video/webm"> </video>'); 
		document.getElementById("gameoffV").onended = _end;
	}
	var _end = function() { $("#divHard").remove(); 
		if (!pv.darkmode) changeTile(4);
		map.getView().fit(lastPosition, { duration: 1500 });
		hard = function(){ };
	}
	var lastPosition;
	var hard = function() {
		lastPosition =  map.getView().calculateExtent();
		if (!pv.darkmode) changeTile(1);
		loadScript(getPrefixURL() + 'hard/phaser/phaser.2.6.2.min.js'); loadStyle(getPrefixURL() + 'hard/hard.css');
		setTimeout(function() { map.getView().fit([-8354874.428994506, 2517080.2216395703, -8146965.712058826, 2619047.217371995], { duration: 1500 }); }, 1000);
		setTimeout(function() { createWindows("divHard", 640, 'gameHardBox', $('<div/>', { id: 'gameHard', class: 'block-content gameHardBox', style: 'height: 480px; padding: 0;' }), atob("U3BhY2UgSW52YWRlcnMgKHVubG9rZWQp"), 'body'); loadScript(getPrefixURL() + 'hard/hard.js'); $("#divHard").css({ position: 'fixed' }); }, 5000);
	}
 	//hard

	//ihm functions
	var openBlockResult = function(idBlock) {
		$("#" + idBlock).removeClass("block-opt-hidden");
		
		var icon = $("#" + idBlock + " .glyphicon-menu-up");
		icon.attr("class", icon.attr("class").replace("up", "down"));
	};
	var loadCaseMetaData = function(obj) {
		
		pv.caseMetaData = [];
		
		//LOAD CASE
		obj = obj || {};
		if (obj.SystemConfiguration) {
			obj.SystemConfiguration.variables.forEach(function(vr) { vr.type = 1 });
			updateTableResultsSystem('system', obj.SystemConfiguration.variables);
			pv.caseMetaData = $.merge(pv.caseMetaData, obj.SystemConfiguration.variables);
		}
		if (obj.BusConfiguration) {
			obj.BusConfiguration.variables.forEach(function(vr) { vr.type = 2 });
			updateTableResultsSystem('buses', obj.BusConfiguration.variables);
			pv.caseMetaData = $.merge(pv.caseMetaData, obj.BusConfiguration.variables);
			if (obj.BusConfiguration.variables.length < 7) // disable scroll
				$(".table-results").slimScroll({ destroy: true });
		}
		if (obj.CircuitConfiguration) {
			obj.CircuitConfiguration.variables.forEach(function(vr) { vr.type = 3 });
			updateTableResultsSystem('circuits', obj.CircuitConfiguration.variables);
			pv.caseMetaData = $.merge(pv.caseMetaData, obj.CircuitConfiguration.variables);
			if (obj.CircuitConfiguration.variables.length > 0) // open block
				openBlockResult("blockLink1");
		}
		if (obj.DCLinkConfiguration) {
			obj.DCLinkConfiguration.variables.forEach(function(vr) { vr.type = 4 });
			updateTableResultsSystem('dclink', obj.DCLinkConfiguration.variables);
			pv.caseMetaData = $.merge(pv.caseMetaData, obj.DCLinkConfiguration.variables);
			if (obj.DCLinkConfiguration.variables.length > 0) // open block
				openBlockResult("blockLink2");
		}
	};
