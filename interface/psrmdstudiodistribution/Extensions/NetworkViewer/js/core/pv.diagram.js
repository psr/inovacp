var buttonDiagramMode = document.createElement('button');
buttonDiagramMode.innerHTML = '<i class="fa fa-share-alt"></i>';
buttonDiagramMode.setAttribute("onclick", "makeDiagram();");
var elementDiagramMode = document.createElement('div');
elementDiagramMode.className = 'tools-button diagram-mode-button ol-unselectable ol-control';
elementDiagramMode.appendChild(buttonDiagramMode);
var diagramModeControl = new ol.control.Control({
	element: elementDiagramMode
});
map.addControl(diagramModeControl);

 
//Create diagram
var makeDiagram = function() {
	if (pv.graph.diagramModeActive) {
		$(".full-map").addClass("block block-opt-refresh");
		changeTile(4);
		$(".full-map").removeClass("diagram");
		setTimeout(function() { 
			setTimeout(function() { 
				prepareAnimationMain();
				pv.animation.waitToAnimate = false;
			}, 100);
			restoreCoodinates();
			setTimeout(function() { 
				$(".full-map").removeClass("block block-opt-refresh");
				fitZoom();
				pv.animation.waitToAnimate = true;
			}, 100);
		}, 100);		
	} else {
		$(".full-map").addClass("block block-opt-refresh");
		changeTile(10);
		$(".full-map").addClass("diagram");
		map.getView().setZoom(7.529892699861739);
		setTimeout(function() { 
			setTimeout(function() { 
				prepareAnimationMain();
				pv.animation.waitToAnimate = false;
			}, 100);
			makeDiagramWork();
			setTimeout(function() { 
				$(".full-map").removeClass("block block-opt-refresh");
				fitZoom();
				pv.animation.waitToAnimate = true;
			}, 100);
		}, 100);		
	}
}
var makeDiagramWork = function() {

	pv.graph.diagramModeActive = true;

	var _markers = pv.graph.nodes;

	var arrayLinks = [];
	arrayLinks = arrayLinks.concat(pv.graph.links);
	arrayLinks = arrayLinks.concat(pv.graph.links2);

	var _lines = arrayLinks;
// Commands

	_lines.forEach(function(l) { 
		l["source"] = l["fromId"];
		l["target"] = l["toId"];
		l.locationBase = l.locationBase || l.location;
	});
	_markers.forEach(function(m) { 
		m["id"] = m["uniqueId"];
		m.y = undefined;
	});
			
	_lines = _lines.filter(function(l) { return ( (_markers.filter(function(m) { return m.id == l.source;  }) || []).length > 0)
												&& ( (_markers.filter(function(m) { return m.id == l.target;  }) || []).length > 0);});
	
	var plotMakerSimulation = function(_markers) {  
		var _features = [];
		_markers.forEach(function(m) { 
			var _feature = new ol.Feature({ 
					geometry: new ol.geom.Point(map.getCoordinateFromPixel([m.x, m.y])),   
					properties: m
				});
			_feature.setId(m.uniqueId);
			_features.push(_feature);
		});
		sourceFeatures.addFeatures(_features);
		//map.getView().fit(sourceFeatures.getExtent());
	}

	var updateMakerSimulation = function(_markers) {  
		var _features = [];
		_markers.forEach(function(m) { 
			var coord = map.getCoordinateFromPixel([m.x, m.y]);
			var _feature = sourceFeatures.getFeatureById(m.uniqueId);
			if (_feature) {
				_feature.getGeometry().setCoordinates(coord);
			} else {
				var _feature = new ol.Feature({ 
						geometry: new ol.geom.Point(coord),   
						properties: m
					});
				_feature.setId(m.uniqueId);
				_features.push(_feature);
			}
		});
		sourceFeatures.addFeatures(_features);
	}											
	  var simulation = d3.forceSimulation(_markers)
		.force("link", d3.forceLink().id(function(d) { return d.id; }))
		.force("charge", d3.forceManyBody())
		.force("center", d3.forceCenter(120, 75))
		.stop();
		
	  simulation.force("link").links(_lines);
	  
	  for (var i = 0; i < 2000; ++i) {
		  simulation.tick();
	  }
	  updateMakerSimulation(_markers);
	  
	var plotLinesFromMarkes = function () {

		_lines.forEach(function (l) {

			var _featureA = sourceFeatures.getFeatureById(l["fromId"]);
			var coorA = _featureA.getGeometry().getCoordinates();
			coorA = ol.proj.transform(coorA, 'EPSG:3857', 'EPSG:4326');

			var _featureB = sourceFeatures.getFeatureById(l["toId"]);
			var coorB = _featureB.getGeometry().getCoordinates();
			coorB = ol.proj.transform(coorB, 'EPSG:3857', 'EPSG:4326');

			l.location = [];
			l.location.push({ latitude: coorA[1], longitude: coorA[0] });
			l.location.push({ latitude: coorB[1], longitude: coorB[0] });
		});

		renderLines();
	};

	plotLinesFromMarkes();

};
//Create diagram