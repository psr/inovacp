//  ===================       Layers      ===================	
      var baseMapsLayers = [];
      var styles = [
		'AerialWithLabels',
        'Aerial',
        'RoadOnDemand'
     ];
      var i, ii;
      for (i = 0, ii = styles.length; i < ii; ++i) {
        baseMapsLayers.push(new ol.layer.Tile({
          visible: false,
          preload: Infinity,
          source: new ol.source.BingMaps({
		    key: 'Anqm0F_JjIZvT0P3abS6KONpaBaKuTnITRrnYuiJCE0WOhH6ZbE4DzeT6brvKVR5',
			maxZoom: 17,
            imagerySet: styles[i]
          })
        }));
      }
		  
	//função para converter sistema xyz para Bing quadKey 
	var quadkey = function (x, y, z) {
		var quadKey = [];
		for (var i = z; i > 0; i--) {
			var digit = '0';
			var mask = 1 << (i - 1);
			if ((x & mask) != 0) {
				digit++;
			}
			if ((y & mask) != 0) {
				digit++;
				digit++;
			}
			quadKey.push(digit);
		}
		if (quadKey.length == 0) return '302311'; // Default para o Mar, para não retornar vazio
		return quadKey.join('');
	};
									
   sourceBingNoLabels = new ol.source.XYZ({
        maxZoom: 17,
        tileUrlFunction: function (tileCoord, pixelRatio, projection) {
            var z = tileCoord[0]; var x = tileCoord[1]; var y = tileCoord[2];
			//return 'https://t1.ssl.ak.dynamic.tiles.virtualearth.net/comp/ch/' + quadkey(x, y, z) + '?mkt=en-US&it=G&shading=hill&og=565&n=z';
            return 'http://ecn.t2.tiles.virtualearth.net/tiles/r' + quadkey(x, y, z) + '?g=671&mkt=fr-fr&lbl=l0&stl=h';
        }
    });
    baseMapsLayers.push(new ol.layer.Tile({ visible: false,  preload: Infinity, source: sourceBingNoLabels}));  
	
	var source_gray_light = new ol.source.XYZ({ url: 'http://a.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png' });
	var source_gray_light_label = new ol.source.XYZ({ crossOrigin: 'Anonymous', url: 'https://cartodb-basemaps-a.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png' });
	//source_gray_light_label = new ol.source.XYZ({ tileUrlFunction: function (tC, pR, prj) {  var z = tC[0]; var x = tC[1]; var y = tC[2];  return `https://cartodb-basemaps-a.global.ssl.fastly.net/light_all/${z}/${x}/${y}.png`; } });
	
	var sourceOpenStreetMap = new ol.source.XYZ({ url: 'http://a.tile.openstreetmap.org/{z}/{x}/{y}.png' });
	var sourceArcgis = new ol.source.XYZ({ url: 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}.png' });

	baseMapsLayers.push(new ol.layer.Tile({ visible: false,  preload: Infinity, source: source_gray_light_label}));
	baseMapsLayers.push(new ol.layer.Tile({ visible: false,  preload: Infinity, source: sourceOpenStreetMap}));
	baseMapsLayers.push(new ol.layer.Tile({ visible: false,  preload: Infinity, source: sourceArcgis}));

     var changeTile = function(index) {
       for (var i = 0, ii = baseMapsLayers.length; i < ii; ++i) {
         baseMapsLayers[i].setVisible(i == index);
       }
     }
	 setLoadingEvents(baseMapsLayers);
	 changeTile(4);
//  ===================       Layers      ===================	

  var scaleLineControl = new ol.control.ScaleLine();


//  ===================       Controle Mapa      ===================	
      var map = new ol.Map({
		renderer: (['webgl', 'canvas']),
	    controls : ol.control.defaults({
           attribution : false,
        }).extend([
          scaleLineControl
        ]),
		pixelRatio: 1,
        layers: baseMapsLayers,
        loadTilesWhileInteracting: true,
        target: 'map',
        view: new ol.View({
          center: [-30, -20],
          zoom: 3,
          minZoom: 3,
          maxZoom: 17
        })
      });
	  
	  
var setZIndex = function() {
	map.getLayerGroup().getLayersArray().forEach(function(l, i) { return l.setZIndex(i); });
}
//  ===================       Controle Mapa      ===================	
//  ===================       Layer Heatmap      ===================	
var heatmapLayer = new ol.layer.Heatmap({
	source: new ol.source.Vector({ }),
	blur: 40,
	radius: 12
});
var sourceHeatmap = heatmapLayer.getSource();
map.addLayer(heatmapLayer);
var setPositionHeatmap = function() {
	sourceHeatmap.clear();
	var listFeatures = sourceFeatures.getFeatures();
	listFeatures.forEach(function(featureMarker) {
		var feature = new ol.Feature(new ol.geom.Point( featureMarker.getGeometry().getCoordinates() ));
		feature.setProperties(featureMarker.getProperties().properties, 'properties');
		feature.set("weight", 0);
		sourceHeatmap.addFeature(feature);
	});
}
//  ===================       Layer Heatmap      ===================	
//  ===================       Layer Group Geometrys      ===================	
	  
	ol.layer.Group.prototype.addLayer = function (layer) { this.getLayers().getArray().push(layer) }
	
	var layerGroupGeometry = new ol.layer.Group({ layers: [] });
	map.addLayer(layerGroupGeometry);

//  ===================       Layer Geometrys      ===================	
	  
	  
//	Main Object PV  
   window.pv = {};
   pv.graph = {};
	  
//                  Examplo de Linha                         //
function lineStyleFunction(feature, resolution) {

  var _width = 2.5;//feature.get('properties').style.width || 3;
  var _color = feature.get('properties').style.color || '#00a54f';
  _color = ol.color.asArray(_color).slice(); _color[3] = 0.8; 
  
  var _currentStage = pv.currentIndexStage || 0;
  var _lineDash = ((feature.get('properties').existingAtStage || 1) - 1) > _currentStage;
  feature.get('properties').style.lineDash = _lineDash;
  
  var styleReturn =  new ol.style.Style({
			   stroke: new ol.style.Stroke({
				   width: _width, color: _color,
				   lineDash: _lineDash ? [5, 5] : null
			   }),
			   text: new ol.style.Text({
					font: 'bold 11px "Open Sans", "Arial Unicode MS", "sans-serif"',
					placement: 'line',
					fill: new ol.style.Fill({
					  color: '#333'
					})
			   })
		   });
  //styleReturn.getText().setText(feature.get('properties').name);
  return styleReturn;
}

var layerLines = new ol.layer.Vector({
        source: new ol.source.Vector({ }),
		declutter: true,
		style: lineStyleFunction
    });
map.addLayer(layerLines);

var layerLines2 = new ol.layer.Vector({
        source: new ol.source.Vector({ }),
		declutter: true,
		style: lineStyleFunction
    });
map.addLayer(layerLines2);


function plotLines2(arrLines) {
	pv.graph.links2 = arrLines;
}

function _plotLines2(arrLines)
{
	var anyLineWithoutLocation = arrLines.filter(function(l) { return (!l.location || (l.location || []).length < 2); })[0];
	if (!anyLineWithoutLocation) { //PlotLine only if all lines contains locations
		plotLines_Base(arrLines, layerLines2);
	}
}

function plotLines(arrLines) {
	pv.graph.links = arrLines;
}

function _plotLines(arrLines)
{
	var anyLineWithoutLocation = arrLines.filter(function(l) { return (!l.location || (l.location || []).length < 2); })[0];
	if (!anyLineWithoutLocation) { //PlotLine only if all lines contains locations
		plotLines_Base(arrLines, layerLines);
	}
}

function renderLines() {

	var arrayLinks = [];
	arrayLinks = arrayLinks.concat(pv.graph.links || []);
	arrayLinks = arrayLinks.concat(pv.graph.links2 || []);

	var groups = groupLineByCoordinates(arrayLinks);
	processLineCurves(groups);

	_plotLines(pv.graph.links || []);
	_plotLines2(pv.graph.links2 || []);
}

function plotLines_Base(arrLines, layer) {
	layer = layer || layerLines;
	clearLayer(layer);
	var sourceLines = layer.getSource();
	for (i = 0; i < arrLines.length; i++)
	{
		var circ = arrLines[i];
		var lineString = new ol.geom.LineString([]);
		var path = [];
		var currentLocation = circ.location;
		if (circ.curveLine) currentLocation = circ.curveLine;
		for (r = 0; r < currentLocation.length; r++)
		{
			path.push(ol.proj.transform([currentLocation[r].longitude, currentLocation[r].latitude], 'EPSG:4326', 'EPSG:3857'));
		}
		lineString.setCoordinates(path);
		circ.style = circ.style || {};
		var feature = new ol.Feature({ geometry: lineString, properties: circ });
		feature.set("layer", layer);
		sourceLines.addFeature(feature);
	}
}


//curve line
var generateCurveBezier = function(arr) {
	var line = turf.lineString(arr);
	var curved = turf.bezierSpline(line);
	//Fix bezierSpline bug, repetting first and last point
	curved.geometry.coordinates.unshift(arr[0]);
	curved.geometry.coordinates.push(arr[arr.length-1]);
	return curved;
}
function projectPoint(x1, y1, x2, y2, r, distanceFactor)
{
	var angle = Math.atan2(y2 - y1, x2 - x1);
	if (r == true) {angle += Math.PI};
	if (!distanceFactor) distanceFactor = 1;
	var d = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)) * distanceFactor;
	var res = {};
	res.x = -Math.sin(angle) * d + x1;
	res.y = Math.cos(angle) * d + y1;
	return res;
}
function getMidPoint(point1, point2) {
	return [(point1[0] + point2[0]) / 2, (point1[1] + point2[1]) / 2];
}
//curve line

var groupLineByCoordinates = function(arrLines) {
	var groups = [];
	// {firstLat: 10, firstLon: 12, listLines: []}
	(arrLines || []).forEach(function(l) {
		if (l.location.length == 2) {
			var firstLon = l.location[0].longitude;
			var lastLon = l.location[1].longitude;
			var firstLat = l.location[0].latitude;
			var lasttLat = l.location[1].latitude;
			var current = groups.filter(function(lf) { return (
				
				(
					(lf.firstLon == firstLon && lf.firstLat == firstLat) &&
					(lf.lastLon == lastLon && lf.lasttLat == lasttLat)
				) || 
				(
					(lf.firstLon == lastLon && lf.firstLat == lasttLat) &&
					(lf.lastLon == firstLon && lf.lasttLat == firstLat)
				)
				
				); })[0];
			if (current) {
				current.listLines.push(l);
			} else {
				groups.push({ firstLon: firstLon, firstLat: firstLat, lastLon: lastLon, lasttLat: lasttLat, listLines: [l] });
			}
		} else {
			groups.push({ listLines: [l] });
		}
	});
	return groups;
}

var processLineCurves = function(groups) {
	
	var distance_init = 0.02;
	var distance_increment = 0.027;
	
	(groups || []).forEach(function(iG) {
		
		var lineDirect; var arrLeft = []; var arrRight = [];
		if (iG.listLines.length == 1) {
			lineDirect = iG.listLines[0];
		} else {
			if (iG.listLines.length % 2 == 1) {
				lineDirect = iG.listLines.shift(0);
			}
			for (iL = 0; iL < iG.listLines.length; iL+=2)
			{
				arrLeft.push(iG.listLines[iL]);
				arrRight.push(iG.listLines[iL+1]);
			}
		}
		for (iL = 0; iL < arrLeft.length; iL++)
		{
			var distance = distance_init + (iL * distance_increment);
			
			var lineObj = arrLeft[iL];
			getCoordinateCurve(lineObj, iG.firstLat, distance, false);
			
			lineObj = arrRight[iL];
			getCoordinateCurve(lineObj, iG.firstLat, distance, true);
		}
	});
}

var getCoordinateCurve = function(lineObj, groupLat, distance, sideRight, twopoins = true)
{
	var locationList = lineObj.location;
	if (lineObj.location[0].latitude != groupLat)
		locationList = lineObj.location.reverse();

	var arrPoints = [
		[ locationList[0].longitude, locationList[0].latitude ],
		[ locationList[1].longitude, locationList[1].latitude ]
	];

	//var midpoint = turf.midpoint(point1, point2);
	var coordPoint1 = ol.proj.transform([arrPoints[0][0], arrPoints[0][1]], 'EPSG:4326', 'EPSG:3857');
	var coordPoint2 = ol.proj.transform([arrPoints[1][0], arrPoints[1][1]], 'EPSG:4326', 'EPSG:3857');

	if (twopoins) { // Netplan Style

		var linestring = new ol.geom.LineString([coordPoint1, coordPoint2]);
		var coord_split1 = linestring.getCoordinateAt(0.01);
		var coord_split2 = linestring.getCoordinateAt(0.99);
		
		var pixelPoint1 = map.getPixelFromCoordinate(coordPoint1);
		var pixelPoint2 = map.getPixelFromCoordinate(coordPoint2);
		var pixelSplit1 = map.getPixelFromCoordinate(coord_split1);
		var pixelSplit2 = map.getPixelFromCoordinate(coord_split2);
		
		distance -= 0.01;

		var projectPoint1 = projectPoint(pixelSplit1[0], pixelSplit1[1], pixelPoint2[0], pixelPoint2[1], !sideRight, distance);
		var projectPoint2 = projectPoint(pixelSplit2[0], pixelSplit2[1], pixelPoint1[0], pixelPoint1[1], sideRight, distance);
		
		var coordResult1 = map.getCoordinateFromPixel([projectPoint1.x, projectPoint1.y]);
		var coordLatLongResult1 = ol.proj.transform([coordResult1[0], coordResult1[1]], 'EPSG:3857', 'EPSG:4326');
		
		var coordResult2 = map.getCoordinateFromPixel([projectPoint2.x, projectPoint2.y]);
		var coordLatLongResult2 = ol.proj.transform([coordResult2[0], coordResult2[1]], 'EPSG:3857', 'EPSG:4326');
	
		var arrCoordFinal = [
			[arrPoints[0][0], arrPoints[0][1]],
			[coordLatLongResult1[0], coordLatLongResult1[1]],
			[coordLatLongResult2[0], coordLatLongResult2[1]],
			[arrPoints[1][0], arrPoints[1][1]]
		]

		lineObj.curveLine = arrCoordFinal.map(function(c) { return {latitude: c[1], longitude: c[0] }; });
		
	} else { //Curve Style

		var pixelPoint1 = map.getPixelFromCoordinate(coordPoint1);
		var pixelPoint2 = map.getPixelFromCoordinate(coordPoint2);
		var pixelMidPoint = getMidPoint(pixelPoint1, pixelPoint2);
		
		//line1
		var projectPointResult = projectPoint(pixelMidPoint[0], pixelMidPoint[1], pixelPoint1[0], pixelPoint1[1], sideRight, distance);
		var coordResult = map.getCoordinateFromPixel([projectPointResult.x, projectPointResult.y]);
		var coordLatLongResult = ol.proj.transform([coordResult[0], coordResult[1]], 'EPSG:3857', 'EPSG:4326');
		var curved = generateCurveBezier([
			[arrPoints[0][0], arrPoints[0][1]],
			[coordLatLongResult[0], coordLatLongResult[1]],
			[arrPoints[1][0], arrPoints[1][1]]
		]);
		
		lineObj.curveLine = curved.geometry.coordinates.map(function(c) { return {latitude: c[1], longitude: c[0] }; });
	}

}

function clearLayer(vectorLayer)
{
    var features = vectorLayer.getSource().getFeatures();
    features.forEach(function (feature) {
        vectorLayer.getSource().removeFeature(feature);
    });
}

function updateLinesByStage(stage)
{
	layerLines.setStyle(layerLines.getStyle());
}

$(document).on('changeStage', function(e, stage) {
	updateLinesByStage(stage);
});
   
//                  Examplo de Linha                         //



//      Exemplo de Ponto   //


    var labelEngine = new labelgun["default"](
        function(label){
            label.labelObject.hide = true;
        }, 
        function(label){
            label.labelObject.hide = false;
        }
    );
	
    function updateLabelsMarkers() {
        
		iCount++;
        var features = layerFeatures.getSource().getFeatures();
        features.forEach(function(feature){
			if (feature.getGeometry().getType() == 'Point' && feature.get('properties')) {
				// Get the label text as a string
				var text = feature.get('properties')[_labelDefaultMarker];
				if (text == null) 
					console.log(feature);
				
				// Get the center point in pixel space
				var center = ol.extent.getCenter(feature.getGeometry().getExtent());
				var pixelCenter = map.getPixelFromCoordinate(center);

				var size = 12;
				var halfText = (size + 1) * (text.length / 4);

				// Create a bounding box for the label using known pixel heights
				var minx = parseInt(pixelCenter[0] - halfText);
				var maxx = parseInt(pixelCenter[0] + halfText);
				
				var maxy = parseInt(pixelCenter[1] - (size / 2));
				var miny = parseInt(pixelCenter[1] + (size / 2));

				// Get bounding box points back into coordinate space
				var min = map.getCoordinateFromPixel([minx, miny]);
				var max = map.getCoordinateFromPixel([maxx, maxy]);
				
				// Create the bounds
				var bounds = {
					bottomLeft: min,
					topRight: max
				};
				// Weight longer labels higher, use their name as the ID
				labelEngine.ingestLabel(bounds, text, text.length, feature)
			}
        });

        // Call the label callbacks for showing and hiding
        labelEngine.update();

    }
iCount = 0;
function getTextSimple(feature, resolution)
{
	  if (feature.hide) {
            return new ol.style.Text(); 
        }
        return new ol.style.Text({
          textAlign: 'center',
          textBaseline: 'top',
		  font: 'bold 12px "Open Sans", "Arial Unicode MS", "sans-serif"',
          text: getText(feature, resolution),
          fill: new ol.style.Fill( {color: '#555'} ),
          stroke: new ol.style.Stroke({color: 'rgba(255, 255, 255, 0.9)', width: 1.8}),
          offsetX: 0,
          offsetY: 10,
          placement: null,
          maxAngle: null,
          exceedLength: null,
		  overflow: false,
          rotation: 0
        });

}
var maxResolution = 4000;
var maxResolution_EPSG_3857 = 0.01167;
var getText = function(feature, resolution) {
  var text = feature.get('properties')[_labelDefaultMarker];
  if (resolution > maxResolution && !pv.graph.diagramModeActive) {
    text = '';
  } else {
    text = ('' + text).trunc(12);
  }
  return text.toUpperCase();
};


/**
 * @param {number} n The max number of characters to keep.
 * @return {string} Truncated string.
 */
String.prototype.trunc = String.prototype.trunc ||
    function(n) {
      return this.length > n ? this.substr(0, n - 1) + '...' : this.substr(0);
    };
	

function pointStyleFunction(feature, resolution) {
	
  var _color = ol.color.asArray('#4e79a7').slice(); _color[3] = 0.8; 

  return new ol.style.Style({
    image: new ol.style.Circle({
                radius: 3, 
                fill: new ol.style.Fill({ color: _color })
            }),
    text: getTextSimple(feature, resolution)
  });
}

function pointStyle2Function(feature, resolution) { // triangle rotated
	
  var _color = ol.color.asArray('#4e79a7').slice(); _color[3] = 0.8; 

  return new ol.style.Style({
	  image: new ol.style.RegularShape({
                radius: 5, 
				fill: new ol.style.Fill({ color: _color }),
				points: 3,
				angle: Math.PI
            }),
    text: getTextSimple(feature, resolution)
  });
}		
		
var
	sourceFeatures = new ol.source.Vector(),
    layerFeatures = new ol.layer.Vector({
        source: sourceFeatures,
		style: pointStyleFunction
    });

	map.renderSync();
    
	// // On end of zoom handle all the labels
    // layerFeatures.on("postcompose", updateLabelsMarkers);
	// 
    // // Update on the first load
    // var listenerKey = layerFeatures.on('change', function(e) {
    //     updateLabelsMarkers();
    //     ol.Observable.unByKey(listenerKey);   
    // });
	
map.addLayer(layerFeatures);

var _labelDefaultMarker = 'name';
function changeLabelDefaultMarker(label)
{
	_labelDefaultMarker = label;
	layerFeatures.changed();
}

var updateMarkerCoordinates = function(_markers) {
	_markers.forEach(function(markerObj) { 
		var vetPoint = ol.proj.transform([markerObj.longitude, markerObj.latitude], 'EPSG:4326', 'EPSG:3857');
		var _feature = sourceFeatures.getFeatureById(markerObj.uniqueId);
		if (_feature) {
			_feature.getGeometry().setCoordinates(vetPoint);
		}
	});
}


function plotMarkes_Base(arrMarkes)
{
	//console.log(JSON.stringify(ps));
	//{"name":"AGUAYTIA220 ","code":1301,"latitude":-9.03084,"longitude":-75.4934}
	var pontsList = [];
	for (r = 0; r < arrMarkes.length; r++)
	{
		var markerObj = arrMarkes[r];
		var vetPoint = ol.proj.transform([markerObj.longitude, markerObj.latitude], 'EPSG:4326', 'EPSG:3857');
		var pointFeature = new ol.Feature({ 
			geometry: new ol.geom.Point(vetPoint),   
     		properties: markerObj
		});
		pointFeature.setId(markerObj.uniqueId);
		pontsList.push(pointFeature);
	}
	sourceFeatures.addFeatures(pontsList);
}

function plotMarkes(arrMarkes)
{
	pv.graph.nodes = arrMarkes;
	var anyMarkerWithoutLocation = arrMarkes.filter(function(m) { return (!m.latitude || !m.longitude); })[0];
	
	if (!anyMarkerWithoutLocation) { //PlotLine only if all markes contains locations
		plotMarkes_Base(arrMarkes);
	}
}
//      Exemplo de Ponto   //


var restoreCoodinates = function () {
	pv.graph.diagramModeActive = false;

	var _markers = pv.graph.nodes;
	updateMarkerCoordinates(_markers);

	var arrayLinks = [];
	arrayLinks = arrayLinks.concat(pv.graph.links);
	arrayLinks = arrayLinks.concat(pv.graph.links2);
	arrayLinks.forEach(function (l) {
		l.location = l.locationBase;
	});
	renderLines();
};


function fitZoom()
{
	if (sourceFeatures.getFeatures().length > 0)
	{
		map.getView().fit(sourceFeatures.getExtent(), { duration: 1500 });
	}
}


//Update labels - Force
var _updateLabelsRunning = false;
var lastCenter = map.getView().getCenter();
function updateLabels(evt)
{
   var _currentCenter = map.getView().getCenter();
   
   if (!_updateLabelsRunning && 
	   ( Math.abs(_currentCenter[0] - lastCenter[0]) > 0.000000002 ) && 
	   ( Math.abs(_currentCenter[1] - lastCenter[1]) > 0.000000002 )
	   )
   {
	   _updateLabelsRunning = true;
	   //map.getView().fit(map.getView().calculateExtent());
	   var _center = map.getView().getCenter();
	   var newCenter = [(_center[0] + 0.000000001), (_center[1] + 0.000000001)];
	   map.getView().setCenter(newCenter);
	   lastCenter = newCenter;
	   _updateLabelsRunning = false;
   }
}

map.on('moveend', updateLabels);


