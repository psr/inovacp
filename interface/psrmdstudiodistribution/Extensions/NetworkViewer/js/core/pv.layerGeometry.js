if (!ol.style.RegularShape.prototype.setRadius) {
	ol.style.RegularShape.prototype.setRadius = function(t) {this.radius_=t; this.render()};
}

var styleCircleFunctionBase = function(color, radius, styleShape) {

   var fillObj, strokeObj;
   var colorFill = ol.color.asArray(color).slice(); colorFill[3] = 0.3; 
   if (styleShape != 2) fillObj = new ol.style.Fill({ color: colorFill });
   if (styleShape != 1) strokeObj = new ol.style.Stroke({ color: color, width: 1.0 });
	
   return new ol.style.Style({
    image: new ol.style.Circle({
      radius: radius,
      stroke: strokeObj,
      fill: fillObj
    })
  });
};
var styleSquareFunctionBase = function(color, radius, styleShape) {
	
   var fillObj, strokeObj;
   var colorFill = ol.color.asArray(color).slice(); colorFill[3] = 0.3; 
   if (styleShape != 2) fillObj = new ol.style.Fill({ color: colorFill });
   if (styleShape != 1) strokeObj = new ol.style.Stroke({ color: color, width: 1.0 });
   
   return new ol.style.Style({
	  image: new ol.style.RegularShape({
		stroke: strokeObj,
	    fill: fillObj,
		points: 4,
		radius: radius,
		angle: Math.PI / 4
	  })
  });
};
var styleTriangleFunctionBase = function(color, radius, styleShape, customAngle) {

   var fillObj, strokeObj;
   var colorFill = ol.color.asArray(color).slice(); colorFill[3] = 0.3; 
   customAngle = customAngle || 0;
   if (styleShape != 2) fillObj = new ol.style.Fill({ color: colorFill });
   if (styleShape != 1) strokeObj = new ol.style.Stroke({ color: color, width: 1.0 });
   
   return new ol.style.Style({
	  image: new ol.style.RegularShape({
		stroke: strokeObj,
	    fill: fillObj,
		points: 3,
		radius: radius,
		angle: customAngle
	  })
  });
};

var getLayerGeometry = function(pref) {
	var layer = layerGroupGeometry.getLayers().getArray().filter(function (layer) { 
		return (pref.idResult == layer.getProperties().idResult) 
	})[0];
	if (layer === undefined) {
		var sourceGeometry = new ol.source.Vector();
		var layer = new ol.layer.Vector({
			renderMode: 'image',
			source: sourceGeometry,
			renderBuffer: window.screen.width * 2,
		});
		layer.setProperties(pref);
		layerGroupGeometry.addLayer(layer);
		
		var listFeatures = sourceFeatures.getFeatures();
		var defaultStyle = getNullStyle(); //styleCircleFunctionBase('#4e79a7', 0);

		listFeatures.forEach(function(feature) {
			var geometry = new ol.Feature(new ol.geom.Point( feature.getGeometry().getCoordinates() ));
			geometry.setProperties(feature.getProperties().properties, 'properties');
			geometry.setStyle(defaultStyle.clone());	
			sourceGeometry.addFeature(geometry);
		});
	}
	return layer;
}
var updateLayerGeometry = function ( pref ) {

	var layer = getLayerGeometry(pref);
	layer.setVisible(pref.selected || false);
    loadLayerValuesAndStyles(layer);
    renderMap();
}
var updateArrayLayerGeometry = function (arrayPref) {
	arrayPref.forEach(function(pref) {
		if (pv.getTypeResult(pref.idResult) == 2)
			updateLayerGeometry(pref);
	});
}

var updateLayerGeometryStyles = function (pref) {
	var layer = getLayerGeometry(pref);
	layer.setProperties(pref);
	loadLayerValuesAndStyles(layer);
	renderMap();
}
var updateLayerGeometryValues = function(stage) {
	layerGroupGeometry.getLayers().getArray().forEach(function(layer) {
		loadLayerValuesAndStyles(layer);
	});
	renderMap();
}
var loadLayerValuesAndStyles = function(layer) {
	if (layer.getVisible()) {
    	var layerProperties = layer.getProperties();
		if (pv.getTypeResult(layerProperties.idResult) == 2) {
			var listFeatures = layer.getSource().getFeatures();
			var currentResult = pv.dataSet.filter(function(rs) { return rs.idResult == layerProperties.idResult; })[0] || {};

			var sameStyle = (currentResult.shapeType == layerProperties.shapeType) &&
							(currentResult.color == layerProperties.color) &&
							(currentResult.styleShape == layerProperties.styleShape);
			if (!sameStyle) {
				currentResult.shapeType = layerProperties.shapeType;
				currentResult.color = layerProperties.color;
				currentResult.styleShape = layerProperties.styleShape;
			}
			
			if (!currentResult.getScale) {
				currentResult.getScale = d3.scaleLinear().domain([ Math.sqrt(currentResult.abs_min || 0), Math.sqrt( (currentResult.abs_max || 1000) * 0.9) ]).range([0, 2 * layerProperties.radiusFactor]);
			}
			var getScale = currentResult.getScale;
			getScale.range([0, 2 * layerProperties.radiusFactor]);
			
			listFeatures.forEach(function(feature) {
				var currentValue = pv.getValueLineSelected(layerProperties.idResult, feature.getProperties(), true);
				var defaultStyle = getNullStyle();
				
				if (currentValue > 0) { 
					currentValue = getScale( Math.sqrt(currentValue) );
					currentValue = Math.max(currentValue, 0);
					currentValue = Math.min(currentValue, (2 * layerProperties.radiusFactor));
					
					var currentStyle = feature.getStyle();
					if (sameStyle && currentStyle && currentStyle.getImage() && currentStyle.getImage().getRadius()) { 
						currentStyle.getImage().setRadius(currentValue);
					} else {
						switch(layerProperties.shapeType) {
							case "1":
								defaultStyle = styleCircleFunctionBase(layerProperties.color, currentValue, layerProperties.styleShape);
								break;
							case "2":
								defaultStyle = styleSquareFunctionBase(layerProperties.color, currentValue, layerProperties.styleShape);
								break;
							case "3":
								defaultStyle = styleTriangleFunctionBase(layerProperties.color, currentValue, layerProperties.styleShape);
								break;
							case "4":
								defaultStyle = styleTriangleFunctionBase(layerProperties.color, currentValue, layerProperties.styleShape, Math.PI);
								break;
							default:
								defaultStyle = styleCircleFunctionBase(layerProperties.color, currentValue, layerProperties.styleShape);
						}
						feature.setStyle(defaultStyle);
					}
				} else {
					feature.setStyle(defaultStyle);
				}
			});
		}
	}
}

var _nullStyle = null;
var getNullStyle = function() {
	if (!_nullStyle)
		_nullStyle = new ol.style.Style({ });
	return _nullStyle;
}

$(document).on('changeStage', function(e, stage) {
	updateLayerGeometryValues(stage);
});

var renderMap = function() {
	var layers = layerGroupGeometry.getLayers().getArray().filter(function(l) { return l.getVisible(); });

	(layers || []).forEach(function(l) {
		l.getSource().getFeatures().forEach(function(f) {
			f.changed();
		});
	});
	map.render();
};
