window.pv = window.pv || {};
pv.functions = pv.functions || {};

pv.functions.options = {
	events: $(this),
	typeOptions: [],
	createFlag: function(idFlag, text, description, appendTo, bindTo) {
		this.typeOptions.push( { type: 'checkbox', idFlag: idFlag } );
		var _events = this.events;
		//Create element html
		var _component = '' +		
			'<div class="form-group">                                                         ' +
			'	<div class="row">                                                             ' +
			'		<div class="col-xs-8">                                                    ' +
			'			<div class="font-s13 font-w600">' + text + '</div>                    ' +
			'			<div class="font-s13 font-w400 text-muted">' + description + '</div>  ' +
			'		</div>                                                                    ' +
			'		<div class="col-xs-4 text-right">                                         ' +
			'			<label class="css-input switch switch-sm switch-primary push-10-t">   ' +
			'				<input id="' + idFlag + '" type="checkbox"><span></span>          ' +
			'			</label>                                                              ' +
			'		</div>                                                                    ' +
			'	</div>                                                                        ' +
			'</div>                                                                           ';
		_component = $(_component);
		_component.appendTo(appendTo);
		//Create event
		$('#'+idFlag).change(function() {
			bindTo[idFlag] = $('#'+idFlag).prop('checked');
			_events.trigger('changed:' + idFlag, bindTo[idFlag])
		});
		//Update default value and fire event
		this.updateValue(idFlag, bindTo, bindTo[idFlag]);
	},
	updateValue: function(idFlag, bindTo, newValue, fireEvent = true) {
		bindTo[idFlag] = newValue;
		var typeFlag = this.typeOptions.filter(function(t) { return t.idFlag == idFlag })[0];
		if ( (typeFlag != undefined) && (typeFlag.type == 'checkbox') ) $('#'+idFlag).prop('checked', bindTo[idFlag] ? 'checked' : '');
		if ( (typeFlag != undefined) && (typeFlag.type == 'radio') ) {
			for (i = 0; i < typeFlag.itens.length; i++)
				if (typeFlag.itens[i].id == bindTo[idFlag])
					$('input:radio[name=radio-'+idFlag+']', '#'+idFlag)[i].checked = true;
		};
		if (fireEvent) $('#'+idFlag).change();
	},
	createOptions: function(idFlag, text, itens, appendTo, bindTo) {
		this.typeOptions.push( { type: 'radio', idFlag: idFlag, itens: itens } );
		var _events = this.events;
		var _component = '' +	
			'<div class="form-group">                                                                                    ' +
			'	<div class="row">                                                                                        ' +
			'		<div class="col-xs-12">                                                                              ' +
			'			<div class="font-s13 font-w600">' + text + '</div>                                               ' +
			'			<div class="text-right" id="' + idFlag + '">                                                     ';
		itens.forEach(function(item) {
			var checked = item.selected ? 'checked' : '';
			_component = _component +
			'   <label class="css-input css-radio css-radio-sm css-radio-primary  push-10-r">                ' +
			'   	<input type="radio" name="radio-' + idFlag + '" value="' + item.id + '" ' + checked + '> ' + 
			'       <span></span> ' + item.text +
			'   </label>                                                                                     ';
		});
		_component = _component +
			'			</div>                                                                                           ' +
			'		</div>                                                                                               ' +
			'	</div>                                                                                                   ' +
			'</div>                                                                                                      ';
		_component = $(_component);
		_component.appendTo(appendTo);
		//Create event
		$('#'+idFlag+' input').on('change', function() {
			bindTo[idFlag] = $('input[name=radio-'+idFlag+']:checked', '#'+idFlag).val();
			_events.trigger('changed:' + idFlag, bindTo[idFlag])
		});
		
		//Update default value and fire event
		this.updateValue(idFlag, bindTo, bindTo[idFlag]);
	}
};			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			