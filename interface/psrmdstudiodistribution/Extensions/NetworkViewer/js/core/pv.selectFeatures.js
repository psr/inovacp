//https://gis.stackexchange.com/questions/128043/select-tolerance-or-snapping-in-openlayers-3#169097
function featuresAtPixel(pixel, layer) {
    var features = [];
    // get all features in extent
    var featuresInExtent = [];
    var currentExtent = map.getView().calculateExtent(map.getSize());

	if (layer.getVisible()) {
		featuresInExtent = featuresInExtent.concat(
				layer.getSource().getFeaturesInExtent(currentExtent));
	}

    featuresInExtent.forEach(function(f) {
        var coord = map.getCoordinateFromPixel(pixel);
        // the closest point for this feature
        var closest = f.getGeometry().getClosestPoint(coord);
        var closePixel = map.getPixelFromCoordinate(closest);
        var dist = pixelsBetween(pixel, closePixel);
        // tolerance in this case is 10
        if (dist < 10) {
            features.push(f);
        }
    });
    return features;
}

function pixelsBetween(pixel1, pixel2) {
    return Math.sqrt(Math.pow(pixel1[0]-pixel2[0],2) + Math.pow(pixel1[1]-pixel2[1],2));
}

var getLineIcon = function (size, color, lineDash) 
{
	return "  <svg height='"+size+"' width='"+size+"' style='stroke:" + color + ";stroke-width:3;stroke-dasharray:" + lineDash + "'>" +
			"    <line x1='"+size+"' y1='0' x2='0' y2='"+size+"' />" +
			"  </svg>";
};
var getCircleIcon = function (size, color) 
{
	return "  <svg height='"+size+"' width='"+size+"' style='fill:" + color + "'>" +
			"    <circle cx='" + size/2 + "' cy='" + size/2 + "' r='5' />" + 
			"  </svg>";
};
var getSquareIcon = function (size, color) 
{
	return "  <svg height='"+size/2+"' width='"+size/2+"' style='fill:" + color + "; margin-left: 5px;'>" +
			"    <rect width='" + size/2 + "' height='" + size/2 + "' />" +
			"  </svg>";
};
var getTriangleIcon = function (size, color) 
{
	return "  <svg height='"+size/2+"' width='"+size/2+"' style='fill:" + color + "; margin-left: 5px;' viewBox='0 0 20 20'>" +
			"    <path d='M0,20 L 10,0  L 20,20z'></path>" +
			"  </svg>";
};
var getInvertedTriangleIcon = function (size, color) 
{
	return "  <svg height='"+size/2+"' width='"+size/2+"' style='fill:" + color + "; margin-left: 5px;' viewBox='0 0 20 20'>" +
			"    <path d='M0,0 L10,20 L20,0z'></path>" +
			"  </svg>";
};

var selectFetures = function (e) {
  highlightedFeatures = [];
  var st = '';
  [].forEach(function(f) {
    var properties = f.getProperties().properties;

/*     st = st + "<tr>" + 
        "<td>" + getTriangleIcon(20, properties.style.color, properties.style.lineDash ? "3" : "none") + "</td>" + 
        "<td>" + properties.code + "</td>" + 
        "<td colspan='4'>" + properties.name.trim() + "</td>" +     
        '<td style="text-align: right;"><b>' +  Math.floor((Math.random() * 100) + 1) + "." +  Math.floor((Math.random() * 100) + 1) + "</b></td>" + 
		"</tr>" ;
		for (i = 0; i < Math.min(parseFloat(Math.random() * 10).toFixed(0), 2); i++) {
			 st = st + "<tr>" + 
			  "<td></td>" + "<td>-</td>" +
			  "<td></td>" +
			  "<td colspan='3'>" + 'hydro' + i + "</td>" + 
			  '<td style="text-align: right;">' + parseFloat(Math.random() * i).toFixed(2) + '</td>' +
			 "</tr>" ;
		}
		 */
  });
  $('#InfoBox').html(st);

  //features marker
  var featuresMarker = featuresAtPixel(e.pixel, layerFeatures);
  pv.resultFeatures.filter(function(f) { return f.type == 2; }).forEach(function(resultFeature) {
	  var resultPreference = pv.userSettings.resultsPreferences.filter(function(r) { return r.idResult == resultFeature.idResult; })[0];

	  if (resultPreference) {
		  var st = '';
		  featuresMarker.forEach(function(f) {
			var properties = f.getProperties().properties;
			var valueDouble = pv.getValueLineSelected(resultFeature.idResult, properties);
			var valueS = parseFloat(Math.round(valueDouble * 100) / 100).toFixed(2);
			var iconFunction = getCircleIcon;
			switch(resultPreference.shapeType) {
				case "1": iconFunction = getCircleIcon; break;
				case "2": iconFunction = getSquareIcon; break;
				case "3": iconFunction = getTriangleIcon; break;
				case "4": iconFunction = getInvertedTriangleIcon; break;
			}
			st = st + "<tr>" + 
				"<td>" + iconFunction(20, resultPreference.color) + "</td>" + 
				"<td>" + properties.code + "</td>" + 
				"<td colspan='4'>" + properties.name.trim() + "</td>" +     
				'<td style="text-align: right;">' + valueS + "</td>" + "</tr>";   

			pv.getValueSubagentsSelected(resultFeature.idResult, properties).forEach(function(sAgVal) {
				var valueS = parseFloat(Math.round(sAgVal.value * 100) / 100).toFixed(2);
				st = st + "<tr>" + 
					"<td></td>" + "<td>-</td>" +
					"<td></td>" +
					"<td colspan='3'>" + sAgVal.name + "</td>" + 
					'<td style="text-align: right;">' + valueS + '</td>' +
					"</tr>" ;
			});
				
		  });
		  $('#featuresBox-' + resultFeature.idResultSafe).html(st);
	  }
  });

  
  //features line
  var featuresLine = featuresAtPixel(e.pixel, layerLines);
  pv.resultFeatures.filter(function(f) { return f.type == 3; }).forEach(function(resultFeature) {
	pv.processSelectFeatureLine(resultFeature, featuresLine);
  });
  
  //features line2 - DCLink
  var featuresLine = featuresAtPixel(e.pixel, layerLines2);
  pv.resultFeatures.filter(function(f) { return f.type == 4; }).forEach(function(resultFeature) {
	pv.processSelectFeatureLine(resultFeature, featuresLine);
  });
  
  //refresh scroll
  var box = $('.block-content.infobox'); box.slimScroll();

}

pv.processSelectFeatureLine = function(resultFeature, featuresLine)
{
	var st = '';
	featuresLine.forEach(function(f) {
		
		var properties = f.getProperties().properties;
		var _color = properties.style.color;
		var _lineDash = properties.style.lineDash;
		if (f.get("layer")) {
			var _layer = f.get("layer");
			var _fnStyle = _layer.getStyle();
			if (_fnStyle) {
				var vetColor = _fnStyle(f).getStroke().getColor();
				if (vetColor.length == 4) vetColor.pop();
				_color = ol.color.asString(vetColor);
				_lineDash = _fnStyle(f).getStroke().getLineDash();
			}
		}
		
		var valueDouble = pv.getValueLineSelected(resultFeature.idResult, properties);
		var valueS = parseFloat(Math.round(valueDouble * 100) / 100).toFixed(2);
		st = st + "<tr>" + 
			"<td>" + getLineIcon(20, _color, _lineDash ? "3" : "none") + "</td>" + 
			"<td>" + properties.code + "</td>" + 
			"<td colspan='4'>" + properties.name.trim() + "</td>" +     
			'<td style="text-align: right;">' + valueS + "</td>" + "</tr>";    
	});
	$('#featuresBox-' + resultFeature.idResultSafe).html(st);
}

pv.resultFeatures = [];
pv.createResultFeatures = function() {

	//get result selected
	var arrSelected = pv.userSettings.resultsPreferences.filter(function(r) { return r.selected; }).map(function(r) { return r.idResult; });
	//get results avaliable
	var chartResults = pv.dataSet.filter(function(rs) { return arrSelected.indexOf(rs.idResult) > -1 && (rs.type == 2 || rs.type == 3 || rs.type == 4); });
	var arrResults = chartResults.map(function(rs) {
		var info = pv.caseMetaData.filter(function(md) { return md.fileName == rs.idResult; })[0] || {};
		return {title: info.name, unit: rs.newUnit || info.unit, idResult: info.fileName, idResultSafe: (info.fileName).replace(/\./g, ""), type: rs.type};
	});

	pv.resultFeatures = [];
	var st = '';
	arrResults = arrResults || [];
	arrResults.sort((a, b) => (a.type > b.type) ? 1 : (a.type === b.type) ? ((a.title > b.title) ? 1 : -1) : -1 );
	arrResults.forEach(function(r) { // {title: '', unit: '', idResult: '' } 
		st += `<tbody class="js-table-sections-header open">
					<tr>
						<td style="width: 40px;"> <i class="fa fa-angle-right"></i> </td>
						<td class="text-left" colspan="7" nowrap="true">
							<span title="${r.title} (${r.unit})" style="overflow: hidden; text-overflow: ellipsis; display: inline-block; width: 220px;"> ${r.title} (${r.unit}) </span>
						</td>
					</tr>
				</tbody>
				<tbody id="featuresBox-${r.idResultSafe}">
				</tbody>`;
	});
	pv.resultFeatures = arrResults;
	$('#table-resultPainel').html(st);
	var box = $('.block-content.infobox'); box.slimScroll();
}

map.on('pointermove', function(e) {
	if (pv.userSettings.options['optMouseSelectResults'] == "M")
		selectFetures(e);
});

map.on("singleclick", function (e) {
	if (pv.userSettings.options['optMouseSelectResults'] == "C")
		selectFetures(e);
});