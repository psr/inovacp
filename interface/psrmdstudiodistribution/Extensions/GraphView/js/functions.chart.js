var locale={ moduleType:"locale", name:"internal", dictionary: { "min:":"P5:", "q1:":"P25:", "median:":"P50:", "mean:":"Avrg:", "q3:":"P75:", "max:":"P95:",
				"min":"P5", "q1":"P25", "median":"P50", "mean":"Avrg", "q3":"P75", "max":"P95" } };
Plotly.register(locale);Plotly.setPlotConfig({ locale: 'internal' });


		function getMinMax(DataListGraph){
			var maxVal = -999999, minVal = 999999;

			for(var i=0;i<DataListGraph.length;i++) {
				var tempMinVal = d3.min(DataListGraph[i].values, function(d) { return d[1];} );
				var tempMaxVal = d3.max(DataListGraph[i].values, function(d) { return d[1];} );

				minVal = (minVal || tempMinVal);
				maxVal = (maxVal || tempMaxVal);

				minVal = (tempMinVal < minVal) ? tempMinVal : minVal;
				maxVal = (tempMaxVal > maxVal) ? tempMaxVal : maxVal;
			}
			return [minVal, maxVal];
		};
		function getChartLineArea(LabelListGraph, DataListGraph, chartName) { //Dados = [P10, P25, P50, P75, P90] 
		    $('#' + chartName).removeClass().addClass('default-chart linearea').html('');
			
			var colors = {
				cinzaClaro: "#e5e5e5",
				cinzaEscuro: "#d1d1d1",
				A: customColors[0], //Azul
				A2: customColors[1], //AzulClaro
				B: customColors[2], //Laranja
				C: customColors[4], //Verde
				aClaro: "#c9d6e4",  //AzulClaroTransparente
				bClaro: "#fbddbf"   //LaranjaClaroTransparente
			}

			var data_plotly = [];
			var xarr = convertToDate(LabelListGraph);
			
			//config 5
			if (DataListGraph.length == 5 || DataListGraph.length == 6) {
				data_plotly = [
					{	//P10
						opacity: 1,
						name: DataListGraph[0].key, showlegend: false, mode: "lines",
						"y": getValueArr(0, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.cinzaClaro, width: 1 },
						fill: "none", type: "scatter"
					},

					{   //P90
						opacity: 1,
						name: DataListGraph[4].key, showlegend: false, mode: "lines",
						"y": getValueArr(4, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.cinzaClaro, width: 1 },
						fill: "tonexty", type: "scatter"
					},

					{	//P25
						opacity: 1,
						name: DataListGraph[1].key, showlegend: false, mode: "lines",
						"y": getValueArr(1, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.cinzaEscuro, width: 1 },
						fill: "none", type: "scatter"
					},

					{	//P75
						opacity: 1,
						name: DataListGraph[3].key, showlegend: false, mode: "lines",
						"y": getValueArr(3, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.cinzaEscuro, width: 1 },
						fill: "tonexty", type: "scatter"
					},

					{	//P50
						name: DataListGraph[2].key, showlegend: true, mode: "lines",
						"y": getValueArr(2, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.A, width: 2 }, //"rgb( 31, 119, 180)"
						fill: "none", type: "scatter"
					},
				];
				
				if (DataListGraph.length == 6) {
					data_plotly.push({ //mean-2
						name: DataListGraph[5].key, showlegend: true, mode: "lines",
						"y": getValueArr(5, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.B, width: 2 },
						fill: "none", type: "scatter"
					});
				}
			}
			//config 4
			if (DataListGraph.length == 4) {
				data_plotly = [
					{	//P25
						opacity: 1,
						name: DataListGraph[1].key, showlegend: false, mode: "lines",
						"y": getValueArr(1, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.aClaro, width: 1 },
						fill: "none", type: "scatter"
					},
					{	//P75
						opacity: 1,
						name: DataListGraph[3].key, showlegend: false, mode: "lines",
						"y": getValueArr(3, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.aClaro, width: 1 },
						fill: "tonexty", type: "scatter"
					},
					{	//P50
						name: DataListGraph[2].key, showlegend: true, mode: "lines",
						"y": getValueArr(2, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.A, width: 2 },
						fill: "none", type: "scatter"
					},
					{	//Other
						name: DataListGraph[0].key, showlegend: true, mode: "lines",
						"y": getValueArr(0, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.A2, width: 2 },
						fill: "none", type: "scatter"
					},
				];			
			}
			//config 3
			if (DataListGraph.length == 3) {
				data_plotly = [
					{	//P25
						opacity: 1,
						name: DataListGraph[0].key, showlegend: false, mode: "lines",
						"y": getValueArr(0, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.cinzaEscuro, width: 1 },
						fill: "none", type: "scatter"
					},
					{	//P75
						opacity: 1,
						name: DataListGraph[2].key, showlegend: false, mode: "lines",
						"y": getValueArr(2, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.cinzaEscuro, width: 1 },
						fill: "tonexty", type: "scatter"
					},
					{	//P50
						name: DataListGraph[1].key, showlegend: true, mode: "lines",
						"y": getValueArr(1, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.C, width: 2 },
						fill: "none", type: "scatter"
					},
				];			
			}
			//config 6
			if (DataListGraph.length == 6 && false) { //disable comparative
				data_plotly = [
					{	//P10
						opacity: 1,
						name: DataListGraph[0].key, showlegend: false, mode: "lines",
						"y": getValueArr(0, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.aClaro, width: 1 },
						fill: "none", type: "scatter"
					},

					{   //P90
						opacity: 1,
						name: DataListGraph[2].key, showlegend: false, mode: "lines",
						"y": getValueArr(2, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.aClaro, width: 1 },
						fill: "tonexty", type: "scatter"
					},

					{	//P10-B
						opacity: 1,
						name: DataListGraph[3].key, showlegend: false, mode: "lines",
						"y": getValueArr(3, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.bClaro, width: 1 },//cde2ca
						fill: "none", type: "scatter"
					},

					{   //P90-B
						opacity: 1,
						name: DataListGraph[5].key, showlegend: false, mode: "lines",
						"y": getValueArr(5, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.bClaro, width: 1 },
						fill: "tonexty", type: "scatter"
					},


					{	//P50-A
						name: DataListGraph[1].key, showlegend: true, mode: "lines",
						"y": getValueArr(1, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.A, width: 2 },
						fill: "none", type: "scatter"
					},
					{	//P50-B
						name: DataListGraph[4].key, showlegend: true, mode: "lines",
						"y": getValueArr(4, DataListGraph),
						"x": xarr,
						line: { dash: "solid", color: colors.B, width: 2 },
						fill: "none", type: "scatter"
					},
					
				];
			}

		    var layout = {
		        xaxis: { showgrid: true },
		        margin: {l: 35, b: 40, r: 35, t: 30, pad: 0, autoexpand: true },
		        legend: {
		            x: 0,
		            y: 1.1,
		            bgcolor: '#fff',
		            bordercolor: 'rgba(0, 0, 0, 0)',
		            borderwidth: 1,
		            orientation: "h",
		            traceorder: 'normal',
		            xanchor: 'auto',
		            yanchor: 'auto'
		        }
		    };

		    Plotly.newPlot(chartName, data_plotly, layout, { displayModeBar: false, displaylogo: false, responsive: true });
			setTimeout(function() { window.dispatchEvent(new Event('resize')); }, 50);
		}
		function tryGetFormat(LabelListGraph) {
			LabelListGraph = LabelListGraph || [""];
			
			var formats = ["DD/MM/YYYY HH:mm", "YYYY-MM-DD HH:mm", "MM/DD/YYYY", "YYYY-MM-DD", "YYYY/MM", "YYYY-MM", "MM/YYYY", "YYYY/WW", "YYYY-WW", "WW/YYYY", "YYYY"];
			var format = "";
			
			if (LabelListGraph[0].trim().length < 4) return format;
			for (var i = 0; i < formats.length; i++) {
				var bValid = true; var format = formats[i];
				for (var j = 0; j < LabelListGraph.length; j++) {
					bValid = bValid && moment(LabelListGraph[j], format, true).isValid()
				}
				if (bValid) {
					return format;
				}
			}
			return "";
		
		}
		function convertToDate(LabelListGraph) {
			var format = tryGetFormat(LabelListGraph);
			if (format != "")
			{
				var arr = [];
				for (var i = 0; i < LabelListGraph.length; i++)
					arr.push(moment(LabelListGraph[i], format)._d);
				return arr;
			} else {
				return LabelListGraph; // sem formato identificado
			}
		}
		function getValueArr(x, DataListGraph) {
		    var arr = [];
		    var data = DataListGraph[x].values;
		    for (var i = 0; i < data.length; i++) {
		        arr.push(data[i][1]);
		    }
		    return arr;
		}
		function hexToRgbA(hex, opacity){
			var c; opacity = opacity || 1;
			if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
				c= hex.substring(1).split('');
				if(c.length== 3){
					c= [c[0], c[0], c[1], c[1], c[2], c[2]];
				}
				c= '0x'+c.join('');
				return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+','+opacity+')';
			}
			throw hex;
		}
		
		function getChartPlotly(LabelListGraph, DataListGraph, chartName, chartType, xLabel, yLabel, title) {
			$('#' + chartName).removeClass().addClass('default-chart').html('');
			chartType = chartType || "LINE"
		    var data_plotly = [];
			var xarr = convertToDate(LabelListGraph);
			if (chartType == "BOXPLOT") {
				var trace = {
					type: 'box',
					//marker: { color: hexToRgbA(customColors[0], 0.7) },
					marker: { color: customColors[0] },
					x: xarr
				};
				//config 3
				if (DataListGraph.length == 3) {
					  trace.q1 = getValueArr(0, DataListGraph);
					  trace.median = getValueArr(1, DataListGraph);
					  trace.q3 = getValueArr(2, DataListGraph);
				}
				//config 5
				if (DataListGraph.length == 5) {
					  trace.lowerfence = getValueArr(0, DataListGraph);
					  trace.q1 = getValueArr(1, DataListGraph);
					  trace.median = getValueArr(2, DataListGraph);
					  trace.q3 = getValueArr(3, DataListGraph);
					  trace.upperfence = getValueArr(4, DataListGraph);
				}
				//config 6
				if (DataListGraph.length == 6) {
					  trace.lowerfence = getValueArr(0, DataListGraph);
					  trace.q1 = getValueArr(1, DataListGraph);
					  trace.mean = getValueArr(2, DataListGraph);
					  trace.q3 = getValueArr(3, DataListGraph);
					  trace.upperfence = getValueArr(4, DataListGraph);
					  trace.median = getValueArr(5, DataListGraph);
				}
				data_plotly.push(trace);
			} else {
				for (i = 0; i < DataListGraph.length; i++) {
					var trace = {
						y: getValueArr(i, DataListGraph),
						x: xarr,
						name: DataListGraph[i].key,
						showlegend: true
					}
					switch (chartType) {
						case "LINE": 
							trace.type = 'scatter';
							trace.line = { dash: "solid", color: customColors[i], width: 1.5 };
							break;
						case "BAR": 
							trace.type = 'bar';
							trace.marker = { color: hexToRgbA(customColors[i], 0.7) };
							break;
						case "AREA":
							trace.hoverinfo = 'x+y';
							trace.mode = 'lines';
							trace.stackgroup = 'one';
							trace.line = { dash: "solid", color: hexToRgbA(customColors[i], 0.7), width: 0.5};
							break;
						case "AREA_COMPARATIVE":
							trace.hoverinfo = 'x+y';
							//trace.mode = 'lines';
							trace.type = 'scatter';
							trace.mode = 'lines+markers';
							trace.fill = (i%2 == 1) ? 'tozeroy' : 'tonexty';
							trace.line = { dash: "solid", color: hexToRgbA(customColors[i], 0.7), width: 0.5};
							break;
						default:
							//none
					}
					data_plotly.push(trace);
				}
			}
			var optionsList = {};
			optionsList.title = title || "";
			optionsList.xtitle = xLabel || null;
			optionsList.ytitle = yLabel || null;
			
			var minMax = getMinMax(DataListGraph);
			
			var yrange = [ Math.min( minMax[0], 0), Math.max( minMax[1], 1) ];
			if (yrange[0] != 0 || yrange[1] != 1) yrange = null; // free range if values are outside the [0,1]
			
			var nticks = xarr.length;
			var format = tryGetFormat(LabelListGraph);
			if (format == "") nticks = Math.min(8, nticks);
		    var layout = {
				title: { text: optionsList.title },
				xaxis: { showgrid: true,
				   nticks: nticks,
				   title: { text: optionsList.xtitle },
				   titlefont: { size: 12 },
				},
				yaxis: {
					title: { text: optionsList.ytitle },
					titlefont: { size: 12 },
					range: yrange
				},
		        margin: {l: 35, b: 35, r: 30, t: 30, pad: 0, autoexpand: true },
		    };
			if (optionsList.xtitle != "") layout.margin.b = 60;
			if (optionsList.ytitle != "") layout.margin.l = 60;
			if (optionsList.title != "") layout.margin.t = 40;
			
            Plotly.newPlot(chartName, data_plotly, layout, { showLink: false, displayModeBar: false, displaylogo: false, responsive: true, doubleClickDelay: 500 });
			setTimeout(function() { window.dispatchEvent(new Event('resize')); }, 50);
			$('#'+chartName + ' .nsdrag.drag.cursor-ns-resize').css({ cursor: 'grab' });
		}
	
	
		function getChartPlotlyXYZ(LabelX, LabelY, DataListGraph, chartName, chartType, optionsList, reflection, colorscale, reversescale) {
			$('#' + chartName).removeClass().addClass('default-chart').html('');
			chartType = chartType || 'heatmap'; //'surface'
			
			optionsList = optionsList || {};
			optionsList.colorscale = optionsList.colorscale || 'Portland'; //'RdBu'
			optionsList.reversescale = optionsList.reversescale || false;
			optionsList.reversescale = JSON.parse(optionsList.reversescale);
			optionsList.reflection = optionsList.reflection || false;
			optionsList.reflection = JSON.parse(optionsList.reflection);

			optionsList.title = optionsList.title || "";
			optionsList.xtitle = optionsList.xtitle || "";
			optionsList.ytitle = optionsList.ytitle || "";
			optionsList.ztitle = optionsList.ztitle || "";
			optionsList.xname = optionsList.xname || "X";
			optionsList.yname = optionsList.yname || "Y";
			optionsList.zname = optionsList.zname || "Value";
			
			reflection = reflection || false;
		    var data_plotly = [];
			var trace = {
				y: LabelY,
				x: convertToDate(LabelX),
				z: DataListGraph,
				showlegend: true,
				type: chartType,
				contours: {
					z: {
						show: reflection,
						usecolormap: true,
						project:{z: reflection}
					}
				}
			};
			//var minMax = getMinMax(DataListGraph);
			//var yrange = [ Math.min( minMax[0], 0), Math.max( minMax[1], 1) ];
			trace.zmin = 0;
			trace.zmax = 1;
			
			data_plotly.push(trace);

			var colors1 = [ "#64469A", "#4F93D4", "#61D52D", "#FBF209", "#F07B1D", "#D12951", "#690B5B", "#480548"];
			var colorscaleValue = [];
			for (var i = 0; i < colors1.length; i++) {
				var pice = 1 / (colors1.length-1);
				colorscaleValue.push([pice*i, colors1[i]]);
			}
			//trace.colorscale = colorscaleValue;
			trace.colorscale = colorscale || 'RdBu';
			trace.reversescale = reversescale || false;
			trace.hovertemplate=`${optionsList.xname}: %{x}<br>${optionsList.yname}: %{y}<br>${optionsList.zname}: %{z}<extra></extra>`;
		    var layout = {
		        title: { text: optionsList.title },
				autosize : true,
		        margin: {l: 35, b: 35, r: 30, t: 5, pad: 0, 
					autoexpand: true 
				},
				scene: {
					camera: {
						//up: { x: 0, y: 0, z: 1 },
						//center: { x: 0.0, y: 0.0, z: 0.0 },
						//eye: { x: 0.1, y: 0.1, z: 1 },
					},
					xaxis: {
					   tickformat: '%d %b',
					   title: { text: optionsList.xtitle },
					   titlefont: { size: 12 },
					},
					yaxis: {
						title: { text: optionsList.ytitle },
						titlefont: { size: 12 },
					},
					zaxis: {
						title: { text: optionsList.ztitle },
						titlefont: { size: 12 },
					}	
				},
				//dragmode: "turntable",
				xaxis: {
				   tickformat: '%d %b',
				   title: { text: optionsList.xtitle },
				   titlefont: { size: 12 },
				},
				yaxis: {
					title: { text: optionsList.ytitle },
					titlefont: { size: 12 },
				}
		    };
			if (optionsList.ytitle != "") layout.margin.l = 60;
			if (optionsList.xtitle != "") layout.margin.b = 60;
			if (optionsList.title != "") layout.margin.t = 40;
		    window.ploty = Plotly.newPlot(chartName, data_plotly, layout, { displayModeBar: false, displaylogo: false,
				scrollZoom: false
			});
			window.addEventListener("resize",function() { Plotly.update(chartName); } );
			setTimeout(function() { window.dispatchEvent(new Event('resize')); }, 50);
		}
	
		function getChartPie(LabelListGraph, DataListGraph, chartName) {
			$('#' + chartName).removeClass().addClass('default-chart').html('<svg></svg>');
			var color = d3.scale.ordinal().range(customColors);
            nv.addGraph(function() {
              var chartBar = nv.models.pieChart()
                  .x(function(d) { return d.label })
                  .y(function(d) { return d.data })
				  .growOnHover(false)
	              .labelType("percent")
	              .labelThreshold(.05)
                  .legendPosition("right")
                  .color(color.range())
                  .duration(250)
                  .showLabels(true);
                d3.select('#' + chartName + ' svg')
					.datum(DataListGraph)
                    .transition().duration(500)
                    .call(chartBar);
	            d3.selectAll('.nv-pieLabels text').style('fill', "white");
				chartBar.tooltip.contentGenerator(function (d) {
					var html = "";
					var elem = d.data;
					html += "<table>";
					html += "<tr><td class='legend-color-guide'><div style='background-color: "+d.color+";'></div></td>"
					+ "<td class='key'>"+elem.label+"</td>"
					+ "<td class='value'>"+d3.format('.2f')(elem.value)+"</td></tr>";
					html += "</table>";
					return html;
					});
                nv.utils.windowResize(chartBar.update);
				setChartTooltip(chartName, chartBar); //Armazena o último tooltip usado para posterior remoção
              return chartBar;
            });
        };
        function getChartDonut(LabelListGraph, DataListGraph, chartName) {
			$('#' + chartName).removeClass().addClass('default-chart').html('<svg></svg>');
			var color = d3.scale.ordinal().range(customColors);
            nv.addGraph(function() {
              var chartBar = nv.models.pieChart()
                  .x(function(d) { return d.label })
                  .y(function(d) { return d.data })
				  .growOnHover(false)
                  .legendPosition("right")
                  .color(color.range())
		          .donut(true)
                  .padAngle(.08)
                  .cornerRadius(5)
                  .duration(250)
                  .showLabels(true);
				chartBar.pie.labelsOutside(true).donut(true);
                d3.select('#' + chartName + ' svg')
					.datum(DataListGraph)
                    .transition().duration(500)
                    .call(chartBar);
				chartBar.tooltip.contentGenerator(function (d) {
					var html = "";
					var elem = d.data;
					html += "<table>";
					html += "<tr><td class='legend-color-guide'><div style='background-color: "+d.color+";'></div></td>"
					+ "<td class='key'>"+elem.label+"</td>"
					+ "<td class='value'>"+d3.format('.2f')(elem.value)+"</td></tr>";
					html += "</table>";
					return html;
					});
                nv.utils.windowResize(chartBar.update);
				setChartTooltip(chartName, chartBar); //Armazena o último tooltip usado para posterior remoção
              return chartBar;
            });
        };
        function getChartBar(LabelListGraph, DataListGraph, chartName, xLabel, yLabel) {
			$('#' + chartName).removeClass().addClass('default-chart').html('<svg></svg>');
			
			var color = d3.scale.ordinal().range(customColors);
            nv.addGraph(function () {
                var chartBar = nv.models.multiBarChart()
                        .x(function (d) { return d[0] })
                        .y(function (d) { return d[1] })
                         .staggerLabels(false)
                         .color(color.range())
                        .duration(250);
                chartBar.xAxis.tickFormat(function (d) {
                    return LabelListGraph[d];
                });
				var minMax = getMinMax(DataListGraph);
				chartBar.forceY([
									Math.min( minMax[0], 0), 
									Math.max( minMax[1], 1)
								  ]);
				chartBar.yAxis.axisLabel(yLabel);
				chartBar.xAxis.axisLabel(xLabel);
				chartBar.yAxis.tickFormat(d3.format('.0f'));
                d3.select('#' + chartName + ' svg')
                    .datum(DataListGraph)
					.transition().duration(500)
                    .call(chartBar);
					
				var domain = chartBar.yAxis.scale().domain();
				if (domain[1] - domain[0] < 10) chartBar.yAxis.tickFormat(d3.format('.1f'));
                nv.utils.windowResize(chartBar.update);
				setChartTooltip(chartName, chartBar); //Armazena o último tooltip usado para posterior remoção
                return chartBar;
            });
        };
        function getChartHorizontalBar(LabelListGraph, DataListGraph, chartName, xLabel, yLabel) {
			$('#' + chartName).removeClass().addClass('default-chart').html('<svg></svg>');
			
			var color = d3.scale.ordinal().range(customColors);
            nv.addGraph(function () {
                var chartBar = nv.models.multiBarHorizontalChart()
                        .x(function (d) { return d[0] })
                        .y(function (d) { return d[1] })
                         .color(color.range())
                        .duration(250);
                chartBar.xAxis.tickFormat(function (d) {
                    return LabelListGraph[d];
                });
				var minMax = getMinMax(DataListGraph);
				chartBar.forceY([
									Math.min( minMax[0], 0), 
									Math.max( minMax[1], 1)
								  ]);
				chartBar.yAxis.axisLabel(yLabel);
				chartBar.xAxis.axisLabel(xLabel);
				chartBar.yAxis.tickFormat(d3.format('.0f'));
                d3.select('#' + chartName + ' svg')
                    .datum(DataListGraph)
					.transition().duration(500)
                    .call(chartBar);
					
				var domain = chartBar.yAxis.scale().domain();
				if (domain[1] - domain[0] < 10) chartBar.yAxis.tickFormat(d3.format('.1f'));
                nv.utils.windowResize(chartBar.update);
				setChartTooltip(chartName, chartBar); //Armazena o último tooltip usado para posterior remoção
                return chartBar;
            });
        };
        function getChartLine(LabelListGraph, DataListGraph, chartName, xLabel, yLabel) {
			$('#' + chartName).removeClass().addClass('default-chart').html('<svg></svg>');
			
			var color = d3.scale.ordinal().range(customColors);
            nv.addGraph(function () {
                var chartLine = nv.models.lineWithFocusChart()
                        .x(function (d) { return d[0] })
                        .y(function (d) { return d[1] })
						.color(color.range())
                        .useInteractiveGuideline(true)
                        .duration(250);
                chartLine.xAxis.tickFormat(function (d) {
                    return LabelListGraph[d];
                });

				var minMax = getMinMax(DataListGraph);
				chartLine.forceY([
									Math.min( minMax[0], 0), 
									Math.max( minMax[1], 1)
								  ]);
                chartLine.yAxis.tickFormat(d3.format('.2f'));
 				chartLine.yAxis.axisLabel(yLabel);
				chartLine.xAxis.axisLabel(xLabel);
                d3.select('#' + chartName + ' svg')
                    .datum(DataListGraph)
					.transition().duration(500)
                    .call(chartLine);
				var domain = chartLine.yAxis.scale().domain();
				if (domain[1] - domain[0] < 10) chartLine.yAxis.tickFormat(d3.format('.1f'));
                nv.utils.windowResize(chartLine.update);
				
				setChartTooltip(chartName, chartLine); //Armazena o último tooltip usado para posterior remoção
				
                return chartLine;
            },
            function () {
                $(".background").attr("style", "cursor: crosshair; fill-opacity: 0.4! important; fill: lightblue;");
            }
            );
        };
        function getChartArea(LabelListGraph, DataListGraph, chartName, xLabel, yLabel) {
			$('#' + chartName).removeClass().addClass('default-chart').html('<svg></svg>');
			
			var color = d3.scale.ordinal().range(customColors);
            nv.addGraph(function () {
                var chartArea = nv.models.stackedAreaChart()
                    .color(color.range())
                    .x(function (d) { return d[0]; })
                    .y(function (d) { return d[1] });
                chartArea.xAxis.tickFormat(function (d) { return LabelListGraph[d]; });
                chartArea.yAxis.tickFormat(d3.format('.0f'));
                chartArea.useInteractiveGuideline(true);
				chartArea.yAxis.axisLabel(yLabel);
				chartArea.xAxis.axisLabel(xLabel);
                d3.select('#' + chartName + ' svg')
                    .datum(DataListGraph)
                    .transition().duration(500)
                    .call(chartArea);
				var domain = chartArea.yAxis.scale().domain();
				if (domain[1] - domain[0] < 10) chartArea.yAxis.tickFormat(d3.format('.1f'));
                nv.utils.windowResize(chartArea.update);

				setChartTooltip(chartName, chartArea); //Armazena o último tooltip usado para posterior remoção

				return chartArea;
            });
        };
		
		function setChartTooltip(chartName, chartObject) {
			var chart = chartObject || {};
			
			//1
			var interactiveLayer = chart.interactiveLayer || chart;
			var tooltip = interactiveLayer.tooltip || {};
			var idTooltip = null;
			if (tooltip.id) idTooltip =  tooltip.id();
			
			//2
			interactiveLayer = chart ||  chart.interactiveLayer;
			tooltip = interactiveLayer.tooltip || {};
			var idTooltip2 = null;
			if (tooltip.id) idTooltip2 =  tooltip.id();
			
			$('#' + chartName).data("chartTooltip", idTooltip);
			$('#' + chartName).data("chartTooltip2", idTooltip2);
		}
		
		function removeOldChartTooltip(chartName) {
			var idTooltip1 = $('#' + chartName).data("chartTooltip");
			if (idTooltip1) $('#' + idTooltip1).remove();
			$('#' + chartName).data("chartTooltip", null);
				
			var idTooltip2 = $('#' + chartName).data("chartTooltip2");
			if (idTooltip2) $('#' + idTooltip2).remove();
			$('#' + chartName).data("chartTooltip2", null);
		}
		
		
		function getExportFileName() {
			return "new_name";
		}
		
		function getTable(LabelListGraph, DataListGraph, chartName, titleChart, unitChart) {
			//converting data NVD3->DataTables
			var _columns = [{ title: 'Date' }];
			for (var i = 0; i < DataListGraph.length; i++)
				_columns.push({ title: DataListGraph[i].key, sClass: 'right' });
			
			var _dataSet = [];
			for (var j = 0; j < LabelListGraph.length; j++) { 
				var row = [];
				row.push(LabelListGraph[j]);
				for (var s = 0; s < DataListGraph.length; s++)
					row.push(formatNumberOneDigit(DataListGraph[s].values[j][1]));
				_dataSet.push(row);
			}
			getTableBase(_columns, _dataSet, chartName, titleChart, unitChart);
		}
		
		function getTableSingleValue(LabelListGraph, DataListGraph, chartName, titleChart, unitChart)
		{
			//converting data NVD3->DataTables
			var _columns = [{ title: titleChart}, { title: LabelListGraph[0], sClass: 'right' } ];
			var _dataSet = [];
			for (var s = 0; s < DataListGraph.length; s++) {
				var row = [];
				row.push(DataListGraph[s].label);
				row.push(formatNumberOneDigit(DataListGraph[s].value));
				_dataSet.push(row);
			}
			getTableBase(_columns, _dataSet, chartName, titleChart, unitChart);
		}
		
		function getTableSankey(LabelListGraph, DataListGraph, chartName, titleChart, unitChart)
		{
			//converting data Sankey->DataTables
			var _columns = [{ title: 'Input' }, { title: 'Output' }, { title: 'Value', sClass: 'right' }];
			
			adjustSankeyData(DataListGraph);
			
			var _dataSet = [];
			for (var j = 0; j < DataListGraph.links.length; j++) { 
				var row = []; var link = DataListGraph.links[j];
				row.push(link.source.name);
				row.push(link.target.name);
				row.push(formatNumberOneDigit(link.value));
				_dataSet.push(row);
			}
			
			function getColor(nodeName) {
				return $.map(DataListGraph.nodes, function(node) {
					if(node.name === nodeName)
						 return node.color;
				});
			}
			
			//var oTable = $('table.data-table').DataTable({ 
				var rowCallback = function(row, data, index){
					$(row).find('td:eq(0)').css('color', d3.rgb(getColor(data[0])).darker(1));
					$(row).find('td:eq(1)').css('color', d3.rgb(getColor(data[1])).darker(1));
					
					$(row).find('td:eq(0)').css('font-weight', 500);
					$(row).find('td:eq(1)').css('font-weight', 500);
			  }
			//});
			getTableBase(_columns, _dataSet, chartName, titleChart, unitChart, rowCallback);
		}
		
		function formatNumberOneDigit(x) {
			if (Number.parseFloat === undefined) return x; //sem suporte a parseFloat
			return Number.parseFloat(x).toFixed(1);
		}

		function getTableBase(_columns, _dataSet, chartName, titleChart, unitChart, rowCallback)
		{
			$('#' + chartName)
				.removeClass()
				.addClass('table-responsive')
				.html('<table class="table table-striped table-bordered table-hover display nowrap"></table>');
			var filename = titleChart + ' (' + unitChart + ')';
			var table = $('#' + chartName + ' table').DataTable( {
				data: _dataSet,
				columns: _columns,
				responsive: true,
				dom: 'T<"row"<"col-sm-3"l><"col-sm-3"f><"col-sm-6"B>>rtip',
				buttons: ['copy',
					{
						extend: 'excelHtml5',
						title: filename,
						filename: filename
					},
					'print'
				],
				colReorder: true,
				iDisplayLength: 5,
				lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
				language: {
				  lengthMenu: "Show _MENU_",
				},
				rowCallback: rowCallback
			});
			setTimeout(function(){
				table.columns.adjust().responsive.recalc();
			},100)
			
			$('.dt-buttons').addClass('pull-right');
			$('.dt-buttons a').addClass('btn-info btn-outline btn-sm').removeClass('btn-default ');
		}
		
		function dropdownChangeEvent(event) { 
			var _this = $(event.target);
			
			var typeSeleected = _this.val();
			var content = _this.data('content');
			var titleDiv = _this.data('title');
			var titleChart = $('#' + titleDiv).text();
			
			var unitChart = _this.data('unit');
			var labelList = _this.data('labelList');
			var dataList = _this.data('dataList');
			
			var flagLargeData = ( ((dataList.length * labelList.length) > 4000) || (labelList.length >= 672));
			removeOldChartTooltip(content);		//Removendo tooltip atual do nvd3
			
			$('.d3-tip').remove(); //Removendo tooltip de antigos sankeys
			$('#' + content).fadeOut(400, function() {
				switch (typeSeleected) {
					case "TSV": 
						getTableSingleValue(labelList, dataList, content, titleChart, unitChart);
						break;
					case "TS": 
						getTableSankey(labelList, dataList, content, titleChart, unitChart);
						break;
					case "T": 
						getTable(labelList, dataList, content, titleChart, unitChart);
						break;
					case "S":
						getChartSankey(labelList, dataList, content);
						break;
					case "6":
						getChartLineArea(labelList, dataList, content);
						break;
					case "5":
						getChartHorizontalBar(labelList, dataList, content);
						break;
					case "4":
						getChartDonut(labelList, dataList, content);
						break;
					case "3":
						getChartPie(labelList, dataList, content);
						break;
					case "2":
						if (!flagLargeData) {
							getChartLine(labelList, dataList, content);
						} else {
							getChartPlotly(labelList, dataList, content, "LINE");
						}
						break;
					case "1":
						if (!flagLargeData) {
							getChartBar(labelList, dataList, content);
						} else {
							getChartPlotly(labelList, dataList, content, "BAR");
						}
						break;
					case "0":
						if (!flagLargeData) {
							getChartArea(labelList, dataList, content);
						} else {
							getChartPlotly(labelList, dataList, content, "AREA");
						}
						break;
					default:
						//Not implemented
				}
				//$('html > .nvtooltip').remove(); //Removendo tooltip perdido do nvd3
				hideNVD3Tooltip();
				$('#' + content).fadeIn();
			});
		}
		
		function setValuesDiv(dropdownId)
		{
			$('#' + dropdownId).selectpicker({ iconBase: 'fa', tickIcon: 'fa-check'});
			$('#' + dropdownId).change(dropdownChangeEvent);
			//$('#' + dropdownId).change();
		}
		
		function setValuesDropdownUnit(dropdownUnitId, dataChart)
		{
			$.each(dataChart, function (i, item) {
				$('#' + dropdownUnitId).append($('<option>', { 
					value: i,
					text : item.unit 
				}));
			});
			
			var list = $('#' + dropdownUnitId + ' option').sort(function(a, b) {
				return a.textContent.localeCompare(b.textContent);
			});
			//Ordem inversa e seleciona o último
			//list = list.get().reverse();
			//$('#' + dropdownUnitId).val(list.length-1);
			$('#' + dropdownUnitId).append(list);
			$('#' + dropdownUnitId).val(0);

			$('#' + dropdownUnitId).selectpicker({ iconBase: 'fa', tickIcon: 'fa-check'});
			$('#' + dropdownUnitId).data('dataChart', dataChart);

			$('#' + dropdownUnitId).change(function(event) {
				var _this = $(event.target); 
				
				var typeSelected = _this.val();
				var contenttype = _this.data('contenttype');
				var dataChart = _this.data('dataChart');
				
				var selectedData = dataChart[typeSelected];
				$('#' + contenttype).data('labelList', selectedData.labelList);
				$('#' + contenttype).data('dataList', selectedData.dataList);
				$('#' + contenttype).data('unit', selectedData.unit);
				$('#' + contenttype).change();
			});
			
			$('#' + dropdownUnitId).change();			
		}
		
		var convertDataChartXY = function(dataChart) {
			var dtCht = [];
			$.each(dataChart, function (i, item) {
				var listX =  item.dataList.filter(function (iData){ return iData.key.trim().startsWith('x'); });
				var listY =  item.dataList.filter(function (iData){ return iData.key.trim().startsWith('y'); });
				$.each(item.labelList, function (i, itemLabel) {
					var objChart = {};
					objChart.unit = itemLabel
					objChart.labelList = [];
					var objItem = { key: 'ag1', values: [] };
					objChart.dataList = [objItem];
					for (var j = 0; j < item.dataList.length/2; j++) {
						objChart.labelList.push(listX[j].values[i][1]);
						objItem.values.push([j, listY[j].values[i][1]]);
					}
					dtCht.push(objChart);
				});
			});
			return dtCht;
		}
		var createDataPickerListValues = function (datetimepickerId, dropdownUnitId, dtCht) {
			var format = "YYYY-MM-DD HH:mm";
			var dateList = [];
			$.each(dtCht, function (i, item) { dateList.push(item.unit); });
			$('#' + datetimepickerId).datetimepicker({
				defaultDate: moment(dateList[0], format)._d,
				enabledDates: dateList.map(function(lb) { return moment(lb, format)._d }),
				minDate: moment(dateList[0], format)._d,
				maxDate: moment(dateList[dateList.length - 1], format)._d,
				format: 'YYYY-MM-DD HH',
				sideBySide: true,
			});
			var dt = $('#' + datetimepickerId);
			var dropdownUnitObj = $('#' + dropdownUnitId);
			dropdownUnitObj.parent().hide();
			dt.on("dp.change", function(d) { 
				var dateS = $(d.currentTarget).data("date") + ":00";
				dropdownUnitObj.val(dateList.indexOf(dateS)); 
				dropdownUnitObj.change(); 
				dt.data("DateTimePicker").hide(); 
			});
		}
		function dropdownMenuChange(event) { 
			var _this = $(event.target);
			
			var typeSelect = _this.data('typeSelect');
			var selectedValue = _this.data('value');
			$('#' + typeSelect).val(selectedValue);
			$('#' + typeSelect).change();

			var dropdownMenu = _this.data('dropdownMenu');
			$('#' + dropdownMenu + ' ul a.dropdown-item').removeClass('dropdownItemActive')
			_this.addClass('dropdownItemActive');
		}
		
		function setValuesDropdownMenu(dropdownId, typeSelect)
		{
			$('#' + dropdownId + ' ul a.dropdown-item').data('typeSelect', typeSelect);
			$('#' + dropdownId + ' ul a.dropdown-item').data('dropdownMenu', dropdownId);
			$('#' + dropdownId + ' ul a.dropdown-item').click(dropdownMenuChange);
		}
		
		//InitFunctions
		var hideNVD3Tooltip =  function (event) {
				$('.nvtooltip').css('opacity', 0);
			}
		$(function() {
			document.addEventListener('touchmove', hideNVD3Tooltip);
			document.addEventListener('scroll', hideNVD3Tooltip);
		});